const express = require("express");
const path = require("path");
const port = 8003;
const app = express();

app.use('/static', express.static(path.join(__dirname, '../static')));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../index.html'));
});

app.listen(port);
console.log("Your Express server has started at port: " + port);
