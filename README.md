# 1) Naming Convention

[NAMING_CONVENTION.md](./NAMING_CONVENTION.md)

# 2) Create ReactJs Project

```
npx create-react-app warehouse-web
npm install tree-changes
```

# 3) Install react-router-dom

```
npm install react-router-dom
```

# 4) Redux Persist

[REDUX_PERSIST.md](./REDUX_PERSIST.md)

# 5) React Redux, Redux Saga and axios

[REDUX_SAGA.md](./REDUX_SAGA.md)

# 6) connected-react-router

```
npm install connected-react-router
```

https://github.com/supasate/connected-react-router

- Step 1, In your root reducer file,
  \*\* Create a function that takes history as an argument and returns a root reducer.

# 7) Formik

```
npm install formik
npm install yup
```

# 8) Install ant-design

```
npm install antd
```

# 9) Install xlsx

```
npm install xlsx
```

# 10) Internationalization and Multi-language

[MULTI_LANGUAGE.md](./MULTI_LANGUAGE.md)
