import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { push } from 'connected-react-router';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import ItemGroup03ListActions from '../Stores/ItemGroup03List01/Actions';

const getAppStore = state => state.app;

export function* itemGroup03List01FetchItemGroup03List({ currentPage, sorts, filters, pageSize }) {
  try {
    yield put(ItemGroup03ListActions.itemGroup03List01FetchItemGroup03ListLoading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      pageSize
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `itemGroup03/index`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        ItemGroup03ListActions.itemGroup03List01FetchItemGroup03ListSuccess(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(ItemGroup03ListActions.itemGroup03List01FetchItemGroup03ListLoading(false));
  }
}

export function* itemGroup03List01GoToDocument({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/cartDetail/update/${hdrId}`));
}

export function* itemGroup03List01GoToAudit({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/auditResource/CART/${hdrId}/CartHdr`));
}

export function* itemGroup03List01UploadPhoto({ id, file }) {
  try {
    yield put(ItemGroup03ListActions.itemGroup03List01UploadLoading(true));

    const app = yield select(getAppStore);

    // eslint-disable-next-line no-undef
    const postData = new FormData();
    if (Math.sign(file.uid) !== -1) {
      postData.append('file', file.originFileObj, file.name);
    }

    const getData = {};

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `itemGroup03/uploadPhoto/${id}`,
      app.token,
      postData,
      getData,
      'multipart/form-data'
    );

    if (result.isSuccess === true) {
      yield put(ItemGroup03ListActions.itemGroup03List01ManagePhotoSuccess(result.data));
      yield put(ItemGroup03ListActions.itemGroup03List01ResetTimestamp());

      // result.data is total
      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(ItemGroup03ListActions.itemGroup03List01UploadLoading(false));
  }
}

export function* itemGroup03List01RemovePhoto({ id }) {
  try {
    yield put(ItemGroup03ListActions.itemGroup03List01UploadLoading(true));

    const app = yield select(getAppStore);

    // eslint-disable-next-line no-undef
    const postData = {};

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `itemGroup03/deletePhoto/${id}`,
      app.token,
      postData
    );

    if (result.isSuccess === true) {
      yield put(ItemGroup03ListActions.itemGroup03List01ManagePhotoSuccess(result.data));
      yield put(ItemGroup03ListActions.itemGroup03List01ResetTimestamp());

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(ItemGroup03ListActions.itemGroup03List01UploadLoading(false));
  }
}
