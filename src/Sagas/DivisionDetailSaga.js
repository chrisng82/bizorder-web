import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { replace, push } from 'connected-react-router';
import ApiService from '../Services/ApiService';
import UtilService from '../Services/UtilService';
import AppActions from '../Stores/App/Actions';
import DivisionDetailActions from '../Stores/DivisionDetail/Actions';

const getAppStore = state => state.app;

const getDivisionDetailStore = state => state.divisionDetail;

export function* divisionDetailInitHeader() {
  try {
    yield put(DivisionDetailActions.divisionDetailShowDocumentLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `division/initModel`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      yield put(DivisionDetailActions.divisionDetailShowHeaderSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(DivisionDetailActions.divisionDetailShowDocumentLoading(false));
  }
}

export function* divisionDetailShowHeader({ hdrId }) {
    try {
      yield put(DivisionDetailActions.divisionDetailShowDocumentLoading(true));
  
      const app = yield select(getAppStore);
      const getData = {};
  
      const result = yield call(
        ApiService.getApi, // function
        app.apiUrl,
        `division/showModel/${hdrId}`,
        app.token,
        getData,
        'multipart/form-data' // params
      );
  
      if (result.isSuccess === true) {
        yield put(DivisionDetailActions.divisionDetailShowHeaderSuccess(result.data));
      } else if (result.isTokenExpired === true) {
        yield put(AppActions.appTokenExpired(result.message));
      } else if (result.isPasswordExpired === true) {
        yield put(AppActions.appPasswordExpired(result.message));
      } else {
        yield call(notification.error, {
          message: result.message,
          duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
        });
      }
    } catch (error) {
      yield call(notification.error, {
        message: error.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } finally {
      yield put(DivisionDetailActions.divisionDetailShowDocumentLoading(false));
    }
  }

  export function* divisionDetailUpdateHeader({ formikBag, documentHeader }) {
    formikBag.setSubmitting(true);
    try {
      const app = yield select(getAppStore);
  
      const postData = {
        data: documentHeader
      };
  
      const result = yield call(
        ApiService.putApi, // function
        app.apiUrl,
        'division/updateModel',
        app.token,
        postData // params
      );
  
      if (result.isSuccess === true) {
        const divisionDetail = yield select(getDivisionDetailStore);
  
        const {
          documentHeader: oldDocumentHeader
        } = divisionDetail;
  
        const {
          model: retDocumentHeader
        } = result.data;

  
        const processed = UtilService.processHeaderDetails(
          oldDocumentHeader,
          [],
          retDocumentHeader,
          [], []
        );
  
        yield put(
          DivisionDetailActions.divisionDetailUpdateHeaderSuccess(retDocumentHeader)
        );
      } else if (result.isTokenExpired === true) {
        yield put(AppActions.appTokenExpired(result.message));
      } else if (result.isPasswordExpired === true) {
        yield put(AppActions.appPasswordExpired(result.message));
      } else {
        yield call(notification.error, {
          message: result.message,
          duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
        });
      }
    } catch (error) {
      yield call(notification.error, {
        message: error.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } finally {
      formikBag.setSubmitting(false);
    }
}

export function* divisionDetailCreateHeader({ formikBag, documentHeader }) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: documentHeader
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      'division/createModel',
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      yield put(replace(`${app.appPath}/divisionDetail/update/${result.data}`));
      yield put(DivisionDetailActions.divisionDetailSetHdrId(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* divisionDetailFetchCompanyOptions({ search }) {
  try {
    yield put(DivisionDetailActions.divisionDetailFetchCompanyOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: []
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `company/select2`,
      app.token,
      postData
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code} ${d.name_01}`
      }));

      yield put(DivisionDetailActions.divisionDetailFetchCompanyOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(DivisionDetailActions.divisionDetailFetchCompanyOptionLoading(false));
  }
}

export function* divisionDetailShowSyncSetting({ hdrId }) {
  try {
    yield put(DivisionDetailActions.divisionDetailShowSyncSettingLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `syncSetting/showDivisionSetting/SYNC_EFICHAIN_01/${hdrId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      yield put(DivisionDetailActions.divisionDetailShowSyncSettingSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(DivisionDetailActions.divisionDetailShowSyncSettingLoading(false));
  }
}

export function* divisionDetailUpdateSyncSetting({ formikBag, syncSetting }) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: syncSetting
    };
    
    const result = yield call(
      ApiService.putApi, // function
      app.apiUrl,
      'syncSetting/updateModel',
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      yield put(DivisionDetailActions.divisionDetailUpdateSyncSettingSuccess(result.data.model));
      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    formikBag.setSubmitting(false);
  }
}

