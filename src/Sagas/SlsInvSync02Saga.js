import { put, call, select } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { notification } from 'antd';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import SlsInvSync02Actions from '../Stores/SlsInvSync02/Actions';

const getAppStore = state => state.app;

export function* slsInvSync02FetchSlsOrdIndex({
  divisionId,
  currentPage,
  sorts,
  filters,
  criteria,
  pageSize
}) {
  try {
    yield put(SlsInvSync02Actions.slsInvSync02FetchSlsOrdIndexLoading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    var filterHasStatus = false;
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
      if (key == 'status') {
        filterHasStatus = true;
      }
    });
    if (!filterHasStatus) {
      processedFilters.push(`status:2,50,100`);
    }
    
    const objCriteria = [];
    if ('start_date' in criteria) {
      objCriteria.push(`start_date:${criteria.start_date}`);
    }
    if ('end_date' in criteria) {
      objCriteria.push(`end_date:${criteria.end_date}`);
    }

    const app = yield select(getAppStore);
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      criteria: objCriteria,
      pageSize
    };
    // console.log(getData);

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `slsOrd/indexProcess/SLS_ORD_01/${divisionId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        SlsInvSync02Actions.slsInvSync02FetchSlsOrdIndexSuccess(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(SlsInvSync02Actions.slsInvSync02FetchSlsOrdIndexLoading(false));
  }
}

export function* slsInvSync02GoToDocument({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/slsInvDetail/update/${hdrId}`));
}

export function* slsInvSync02FetchSlsOrdIndexSearch({ 
  formikBag, 
  criteria,
  divisionId,
  currentPage,
  sorts,
  filters,
  pageSize }) {
  try {
    formikBag.setSubmitting(true);
    yield put(SlsInvSync02Actions.slsInvSync02FetchSlsOrdIndexLoading(true));

    // const objCriteria = {};
    // if ('date' in criteria) {
    //   objCriteria.date = criteria.date;
    // }
    const objCriteria = [];
    if ('start_date' in criteria) {
      objCriteria.push(`start_date:${criteria.start_date}`);
    }
    if ('end_date' in criteria) {
      objCriteria.push(`end_date:${criteria.end_date}`);
    }

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    var filterHasStatus = false;
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
      if (key == 'status') {
        filterHasStatus = true;
      }
    });
    if (!filterHasStatus) {
      processedFilters.push(`status:2,50,100`);
    }

    const app = yield select(getAppStore);
    
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      criteria: objCriteria,
      pageSize
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `slsOrd/indexProcess/SLS_ORD_01/${divisionId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        SlsInvSync02Actions.slsInvSync02FetchSlsOrdIndexSearchSuccess(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page,
          criteria
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    formikBag.setSubmitting(false);
    yield put(SlsInvSync02Actions.slsInvSync02FetchSlsOrdIndexLoading(false));
  }
}

export function* slsInvSync02ShowBatchJobStatus() {
  try {
    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `batchJobStatus/showBatchJobStatus/SLS_ORD_SYNC_02`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      yield put(SlsInvSync02Actions.slsInvSync02ShowBatchJobStatusSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  }
}

export function* slsInvSync02SyncNow({ formikBag, divisionId, criteria }) {
  try {
    formikBag.setSubmitting(true);
    yield put(SlsInvSync02Actions.slsInvSync02SyncLoading(true));
    
    const app = yield select(getAppStore);

    const getData = {
      startDate : ('start_date' in criteria) ? criteria.start_date : '2020-01-01',
      endDate : ('end_date' in criteria) ? criteria.end_date : '2020-01-01',
    };
    // console.log(getData);

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `slsInv/syncProcess/SLS_INV_SYNC_02/${divisionId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    // console.log(result);

    if (result.isSuccess === true) {
      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
      yield put(SlsInvSync02Actions.slsInvSync02ResetTimestamp());
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    formikBag.setSubmitting(false);
    yield put(SlsInvSync02Actions.slsInvSync02SyncLoading(false));
  }
}
