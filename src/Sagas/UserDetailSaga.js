import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { replace, push } from 'connected-react-router';
import ApiService from '../Services/ApiService';
import UtilService from '../Services/UtilService';
import AppActions from '../Stores/App/Actions';
import UserDetailActions from '../Stores/UserDetail/Actions';

const getAppStore = state => state.app;

const getUserDetailStore = state => state.userDetail;

export function* userDetailInitHeader({ login_type }) {
  try {
    yield put(UserDetailActions.userDetailShowDocumentLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `user/initModel`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      result.data['login_type'] = login_type;
      yield put(UserDetailActions.userDetailShowHeaderSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserDetailActions.userDetailShowDocumentLoading(false));
  }
}

export function* userDetailShowHeader({ hdrId }) {
    try {
      yield put(UserDetailActions.userDetailShowDocumentLoading(true));
  
      const app = yield select(getAppStore);
      const getData = {};
  
      const result = yield call(
        ApiService.getApi, // function
        app.apiUrl,
        `user/showModel/${hdrId}`,
        app.token,
        getData,
        'multipart/form-data' // params
      );
  
      if (result.isSuccess === true) {
        yield put(UserDetailActions.userDetailShowHeaderSuccess(result.data));
      } else if (result.isTokenExpired === true) {
        yield put(AppActions.appTokenExpired(result.message));
      } else if (result.isPasswordExpired === true) {
        yield put(AppActions.appPasswordExpired(result.message));
      } else {
        yield call(notification.error, {
          message: result.message,
          duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
        });
      }
    } catch (error) {
      yield call(notification.error, {
        message: error.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } finally {
      yield put(UserDetailActions.userDetailShowDocumentLoading(false));
    }
  }

  export function* userDetailUpdateHeader({ formikBag, documentHeader }) {
    formikBag.setSubmitting(true);
    try {
      const app = yield select(getAppStore);
  
      const postData = {
        data: documentHeader
      };
  
      const result = yield call(
        ApiService.putApi, // function
        app.apiUrl,
        'user/updateModel',
        app.token,
        postData // params
      );
  
      if (result.isSuccess === true) {
        const userDetail = yield select(getUserDetailStore);
  
        const {
          documentHeader: oldDocumentHeader
        } = userDetail;
  
        const {
          model: retDocumentHeader
        } = result.data;
  
        const processed = UtilService.processHeaderDetails(
          oldDocumentHeader,
          [],
          retDocumentHeader,
          [], []
        );
  
        yield put(
          UserDetailActions.userDetailUpdateHeaderSuccess(retDocumentHeader)
        );
      } else if (result.isTokenExpired === true) {
        yield put(AppActions.appTokenExpired(result.message));
      } else if (result.isPasswordExpired === true) {
        yield put(AppActions.appPasswordExpired(result.message));
      } else {
        yield call(notification.error, {
          message: result.message,
          duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
        });
      }
    } catch (error) {
      yield call(notification.error, {
        message: error.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } finally {
      formikBag.setSubmitting(false);
    }
}

export function* userDetailCreateHeader({ formikBag, documentHeader }) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);

    const postData = {
      data: documentHeader
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      'user/createModel',
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      yield put(replace(`${app.appPath}/userDetail/update/${result.data}`));
      yield put(UserDetailActions.userDetailSetHdrId(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* userDetailGoToChangePassword({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/changePassword/${hdrId}`));
}

export function* userDetailChangePassword({ formikBag, hdrId, newPassword }) {
  formikBag.setSubmitting(true);
  try {
    const app = yield select(getAppStore);
    const postData = {
      newPassword
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `user/changePassword/${hdrId}`,
      app.token,
      postData // params
    );
    if (result.isSuccess === true) {
      yield put(AppActions.appChangePasswordSuccess(result.message));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    formikBag.setSubmitting(false);
  }
}

export function* userDetailFetchDebtorOptions({ search }) {
  try {
    yield put(UserDetailActions.userDetailFetchDebtorOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: []
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `debtor/select2`,
      app.token,
      postData
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({
        value: d.id,
        label: `${d.code} ${d.company_name_01}`
      }));

      yield put(UserDetailActions.userDetailFetchDebtorOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserDetailActions.userDetailFetchDebtorOptionLoading(false));
  }
}
