import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { push } from 'connected-react-router';
import ApiService from '../Services/ApiService';
import UtilService from '../Services/UtilService';
import AppActions from '../Stores/App/Actions';
import UserList02Actions from '../Stores/UserList02/Actions';

const getAppStore = state => state.app;

const getUserList02SelectedDocumentStore = state => state.userList02.selectedDocument;

export function* userList02FetchUserList02({ currentPage, sorts, filters, pageSize }) {
  try {
    yield put(UserList02Actions.userList02FetchUserList02Loading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      pageSize
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `user/indexProcess/USER_LIST_02`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        UserList02Actions.userList02FetchUserList02Success(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList02Actions.userList02FetchUserList02Loading(false));
  }
}

export function* userList02GoToDocument({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/userDetail/update/${hdrId}`));
}

export function* userList02GoToAudit({ userId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/auditUser/${userId}`));
}

export function* userList02NewDocument({ siteFlowId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/userDetail/create/v`));
}

export function* userList02RemoveDivision({ userId, divisionId }) {
  try {
    yield put(UserList02Actions.userList02ManageDivisionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      data: [{ division_id: divisionId }]
    };

    const result = yield call(
      ApiService.deleteApi, // function
      app.apiUrl,
      `user/deleteDivisions/${userId}`,
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      const userList02SelectedDocument = yield select(getUserList02SelectedDocumentStore);

      const { divisions: oldDivisions } = userList02SelectedDocument;
      
      const { divisions: retDivisions, deleted_divisions: retDeletedDivisions } = result.data;

      const processed = UtilService.processResources(oldDivisions, retDivisions, retDeletedDivisions);

      yield put(UserList02Actions.userList02ManageDivisionSuccess(processed.resources));

      yield put(UserList02Actions.userList02ResetTimestamp());

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList02Actions.userList02ManageDivisionLoading(false));
  }
}

export function* userList02FetchDivisionOptions({ userId, search }) {
  try {
    yield put(UserList02Actions.userList02FetchDivisionOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [
        {
          field: 'user_id',
          operator: 'NOT IN',
          value: userId
        }
      ]
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `division/select2`,
      app.token,
      postData
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({ value: d.id, label: `${d.code}` + ' - ' + `${d.name_01}` }));

      yield put(UserList02Actions.userList02FetchDivisionOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList02Actions.userList02FetchDivisionOptionLoading(false));
  }
}

export function* userList02AddDivision({ userId, divisionId }) {
  try {
    yield put(UserList02Actions.userList02ManageDivisionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      data: [{ division_id: divisionId }]
    };

    const result = yield call(
      ApiService.putApi, // function
      app.apiUrl,
      `user/createDivisions/${userId}`,
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      const userList02SelectedDocument = yield select(getUserList02SelectedDocumentStore);

      const { divisions: oldDivisions } = userList02SelectedDocument;

      const { divisions: retDivisions, deleted_divisions: retDeletedDivisions } = result.data;

      const processed = UtilService.processResources(oldDivisions, retDivisions, retDeletedDivisions);

      yield put(UserList02Actions.userList02ManageDivisionSuccess(processed.resources));

      yield put(UserList02Actions.userList02ResetTimestamp());

      // clear the select2 options and option
      yield put(UserList02Actions.userList02FetchDivisionOptionSuccess([]));
      yield put(UserList02Actions.userList02SetDivisionOption({ value: 0, label: '' }));

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList02Actions.userList02ManageDivisionLoading(false));
  }
}
