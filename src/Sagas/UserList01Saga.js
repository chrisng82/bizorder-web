import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { push } from 'connected-react-router';
import ApiService from '../Services/ApiService';
import UtilService from '../Services/UtilService';
import AppActions from '../Stores/App/Actions';
import UserList01Actions from '../Stores/UserList01/Actions';

const getAppStore = state => state.app;

const getUserList01SelectedDocumentStore = state => state.userList01.selectedDocument;

export function* userList01FetchUserList01({ currentPage, sorts, filters, pageSize }) {
  try {
    yield put(UserList01Actions.userList01FetchUserList01Loading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      pageSize
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `user/indexProcess/USER_LIST_01`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        UserList01Actions.userList01FetchUserList01Success(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01FetchUserList01Loading(false));
  }
}

export function* userList01GoToDocument({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/userDetail/update/${hdrId}`));
}

export function* userList01RemoveRole({ userId, roleId }) {
  try {
    yield put(UserList01Actions.userList01ManageRoleLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      data: [{ role_id: roleId }]
    };

    const result = yield call(
      ApiService.deleteApi, // function
      app.apiUrl,
      `user/deleteRoles/${userId}`,
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      const userList01SelectedDocument = yield select(getUserList01SelectedDocumentStore);

      const { roles: oldRoles } = userList01SelectedDocument;

      const { roles: retRoles, deleted_roles: retDeletedRoles } = result.data;

      const processed = UtilService.processResources(oldRoles, retRoles, retDeletedRoles);

      yield put(UserList01Actions.userList01ManageRoleSuccess(processed.resources));

      yield put(UserList01Actions.userList01ResetTimestamp());

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01ManageRoleLoading(false));
  }
}

export function* userList01FetchRoleOptions({ userId, search }) {
  try {
    yield put(UserList01Actions.userList01FetchRoleOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [
        {
          field: 'user_id',
          operator: 'NOT IN',
          value: userId
        }
      ]
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `role/select2`,
      app.token,
      postData
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({ value: d.id, label: `${d.desc_01}` }));

      yield put(UserList01Actions.userList01FetchRoleOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01FetchRoleOptionLoading(false));
  }
}

export function* userList01AddRole({ userId, roleId }) {
  try {
    yield put(UserList01Actions.userList01ManageRoleLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      data: [{ role_id: roleId }]
    };

    const result = yield call(
      ApiService.putApi, // function
      app.apiUrl,
      `user/updateRoles/${userId}`,
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      const userList01SelectedDocument = yield select(getUserList01SelectedDocumentStore);

      const { roles: oldRoles } = userList01SelectedDocument;

      const { roles: retRoles, deleted_roles: retDeletedRoles } = result.data;

      const processed = UtilService.processResources(oldRoles, retRoles, retDeletedRoles);

      yield put(UserList01Actions.userList01ManageRoleSuccess(processed.resources));

      yield put(UserList01Actions.userList01ResetTimestamp());

      // clear the select2 options and option
      yield put(UserList01Actions.userList01FetchRoleOptionSuccess([]));
      yield put(UserList01Actions.userList01SetRoleOption({ value: 0, label: '' }));

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01ManageRoleLoading(false));
  }
}

export function* userList01GoToAudit({ userId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/auditUser/${userId}`));
}

export function* userList01NewDocument({ siteFlowId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/userDetail/create/w`));
}

export function* userList01RemoveDivision({ userId, divisionId }) {
  try {
    yield put(UserList01Actions.userList01ManageDivisionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      data: [{ division_id: divisionId }]
    };

    const result = yield call(
      ApiService.deleteApi, // function
      app.apiUrl,
      `user/deleteDivisions/${userId}`,
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      const userList01SelectedDocument = yield select(getUserList01SelectedDocumentStore);

      const { divisions: oldDivisions } = userList01SelectedDocument;
      
      const { divisions: retDivisions, deleted_divisions: retDeletedDivisions } = result.data;

      const processed = UtilService.processResources(oldDivisions, retDivisions, retDeletedDivisions);

      yield put(UserList01Actions.userList01ManageDivisionSuccess(processed.resources));

      yield put(UserList01Actions.userList01ResetTimestamp());

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01ManageDivisionLoading(false));
  }
}

export function* userList01FetchDivisionOptions({ userId, search }) {
  try {
    yield put(UserList01Actions.userList01FetchDivisionOptionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      search,
      filters: [
        {
          field: 'user_id',
          operator: 'NOT IN',
          value: userId
        }
      ]
    };

    const result = yield call(
      ApiService.postApi, // function
      app.apiUrl,
      `division/select2`,
      app.token,
      postData
    );

    if (result.isSuccess === true) {
      const options = result.data.data.map(d => ({ value: d.id, label: `${d.code}` + ' - ' + `${d.name_01}` }));

      yield put(UserList01Actions.userList01FetchDivisionOptionSuccess(options));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01FetchDivisionOptionLoading(false));
  }
}

export function* userList01AddDivision({ userId, divisionId }) {
  try {
    yield put(UserList01Actions.userList01ManageDivisionLoading(true));

    const app = yield select(getAppStore);
    const postData = {
      data: [{ division_id: divisionId }]
    };

    const result = yield call(
      ApiService.putApi, // function
      app.apiUrl,
      `user/createDivisions/${userId}`,
      app.token,
      postData // params
    );

    if (result.isSuccess === true) {
      const userList01SelectedDocument = yield select(getUserList01SelectedDocumentStore);

      const { divisions: oldDivisions } = userList01SelectedDocument;

      const { divisions: retDivisions, deleted_divisions: retDeletedDivisions } = result.data;

      const processed = UtilService.processResources(oldDivisions, retDivisions, retDeletedDivisions);

      yield put(UserList01Actions.userList01ManageDivisionSuccess(processed.resources));

      yield put(UserList01Actions.userList01ResetTimestamp());

      // clear the select2 options and option
      yield put(UserList01Actions.userList01FetchDivisionOptionSuccess([]));
      yield put(UserList01Actions.userList01SetDivisionOption({ value: 0, label: '' }));

      yield call(notification.success, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(UserList01Actions.userList01ManageDivisionLoading(false));
  }
}