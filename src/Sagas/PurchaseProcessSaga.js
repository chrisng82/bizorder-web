import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import PurchaseProcessActions from '../Stores/PurchaseProcess/Actions';

const getAppStore = state => state.app;

// eslint-disable-next-line import/prefer-default-export
export function* purchaseProcessFetchPurchaseProcess({ divisionId }) {
  try {
    yield put(PurchaseProcessActions.purchaseProcessFetchPurchaseProcessLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `division/indexPurchaseFlow/${divisionId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );
    if (result.isSuccess === true) {
      yield put(PurchaseProcessActions.purchaseProcessFetchPurchaseProcessSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(PurchaseProcessActions.purchaseProcessFetchPurchaseProcessLoading(false));
  }
}
