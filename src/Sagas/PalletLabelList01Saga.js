import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import { push } from 'connected-react-router';
import FileSaver from 'file-saver';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import PalletLabelList01Actions from '../Stores/PalletLabelList01/Actions';

const getAppStore = state => state.app;

export function* palletLabelList01FetchPalletLabelList01({
  siteFlowId,
  currentPage,
  sorts,
  filters,
  pageSize
}) {
  try {
    yield put(PalletLabelList01Actions.palletLabelList01FetchPalletLabelList01Loading(true));

    const processedSorts = [];
    Object.entries(sorts).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      processedSorts.push(`${key}:${value === 'ascend' ? 'ASC' : 'DESC'}`);
    });

    const processedFilters = [];
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value) {
        processedFilters.push(`${key}:${value}`);
      }
    });

    const app = yield select(getAppStore);
    const getData = {
      page: currentPage,
      sorts: processedSorts,
      filters: processedFilters,
      pageSize
    };

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `handlingUnit/indexProcess/HANDLING_UNIT_LIST_01/${siteFlowId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );

    if (result.isSuccess === true) {
      // if nowCurrentPage is more than lastPage, then nowCurrentPage = lastPage
      let nowCurrentPage = result.data.current_page;
      if (nowCurrentPage > result.data.last_page) {
        nowCurrentPage = result.data.last_page;
      }

      yield put(
        PalletLabelList01Actions.palletLabelList01FetchPalletLabelList01Success(
          result.data.data,
          nowCurrentPage,
          result.data.last_page,
          result.data.total,
          result.data.per_page
        )
      );
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(PalletLabelList01Actions.palletLabelList01FetchPalletLabelList01Loading(false));
  }
}

export function* palletLabelList01GoToDocument({ hdrId }) {
  const app = yield select(getAppStore);
  yield put(push(`${app.appPath}/palletLabelDetail/update/${hdrId}`));
}

export function* palletLabelList01PrintPalletLabelList01({ siteFlowId, ids }) {
  try {
    yield put(PalletLabelList01Actions.palletLabelList01PrintPalletLabelList01Loading(true));

    const app = yield select(getAppStore);

    const getData = {
      ids
    };

    const result = yield call(
      ApiService.downloadGetApi, // function
      app.apiUrl,
      `handlingUnit/printProcess/HANDLING_UNIT_LIST_01/${siteFlowId}`,
      app.token,
      getData,
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    );

    if (result.isSuccess === true) {
      const selectedDocuments = ids.map(d => ({ id: d }));

      FileSaver.saveAs(result.data, 'PalletLabel.pdf');
      yield put(
        PalletLabelList01Actions.palletLabelList01RemoveSelectedDocuments(selectedDocuments)
      );
      // yield put(PalletLabelList01Actions.palletLabelList01ResetTimestamp());
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(PalletLabelList01Actions.palletLabelList01PrintPalletLabelList01Loading(false));
  }
}
