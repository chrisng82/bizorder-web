import { put, call, select } from 'redux-saga/effects';
import { notification } from 'antd';
import ApiService from '../Services/ApiService';
import AppActions from '../Stores/App/Actions';
import InvAuditProcessActions from '../Stores/InvAuditProcess/Actions';

const getAppStore = state => state.app;

// eslint-disable-next-line import/prefer-default-export
export function* invAuditProcessFetchInvAuditProcess({ siteFlowId }) {
  try {
    yield put(InvAuditProcessActions.invAuditProcessFetchInvAuditProcessLoading(true));

    const app = yield select(getAppStore);
    const getData = {};

    const result = yield call(
      ApiService.getApi, // function
      app.apiUrl,
      `site/indexInventoryAuditFlow/${siteFlowId}`,
      app.token,
      getData,
      'multipart/form-data' // params
    );
    if (result.isSuccess === true) {
      yield put(InvAuditProcessActions.invAuditProcessFetchInvAuditProcessSuccess(result.data));
    } else if (result.isTokenExpired === true) {
      yield put(AppActions.appTokenExpired(result.message));
    } else if (result.isPasswordExpired === true) {
      yield put(AppActions.appPasswordExpired(result.message));
    } else {
      yield call(notification.error, {
        message: result.message,
        duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
      });
    }
  } catch (error) {
    yield call(notification.error, {
      message: error.message,
      duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
    });
  } finally {
    yield put(InvAuditProcessActions.invAuditProcessFetchInvAuditProcessLoading(false));
  }
}
