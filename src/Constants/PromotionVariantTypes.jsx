const ITEM = 0;
const ITEM_GROUP_01 = 1;
const ITEM_GROUP_02 = 2;
const ITEM_GROUP_03 = 3;

export default {
  ITEM,
  ITEM_GROUP_01,
  ITEM_GROUP_02,
  ITEM_GROUP_03
};
