import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import treeChanges from 'tree-changes';

import { Card, Collapse } from 'antd';
import CriteriaForm from './CriteriaForm';
import ReportTable from './ReportTable';

import StockBalanceReportActions from '../../Stores/StockBalanceReport/Actions';

const { Panel } = Collapse;

class StockBalanceReportScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, initStockBalance } = this.props;

    if (curSiteFlowId > 0) {
      initStockBalance(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, initStockBalance } = this.props;
    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        initStockBalance(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'stock_balance_report' })}>
        <Collapse defaultActiveKey={['2']}>
          <Panel header={intl.formatMessage({ id: 'criteria' })} key="1">
            <CriteriaForm match={match} />
          </Panel>
          <Panel header={intl.formatMessage({ id: 'report' })} key="2">
            <ReportTable match={match} />
          </Panel>
        </Collapse>
      </Card>
    );
  }
}

StockBalanceReportScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,

  initStockBalance: PropTypes.func,
  curSiteFlowId: PropTypes.number
};

StockBalanceReportScreen.defaultProps = {
  intl: {},
  match: {},

  initStockBalance: {},
  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  initStockBalance: siteFlowId =>
    dispatch(StockBalanceReportActions.stockBalanceReportInitStockBalance(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(StockBalanceReportScreen));
