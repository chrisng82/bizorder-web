import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Row, Col, Table, Button, Badge } from 'antd';

import AntDesignTable from '../../Components/AntDesignTable';
import PickFaceStrategyList01Actions from '../../Stores/PickFaceStrategyList01/Actions';

class PickFaceStrategyList01Table extends React.PureComponent {
  constructor() {
    super();

    this.useOnTableChange = this.useOnTableChange.bind(this);
    this.useShowWorkspace = this.useShowWorkspace.bind(this);

    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDocumentColumns = this.getDocumentColumns.bind(this);
  }

  componentDidMount() {
    const { curSiteFlowId, setWorkspaceVisible, resetTimestamp } = this.props;

    if (curSiteFlowId > 0) {
      setWorkspaceVisible(false);
      resetTimestamp();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      curSiteFlowId,
      resetTimestamp,
      fetchPickFaceStrategyList01,
      currentPage,
      sorts,
      filters,
      pageSize
    } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      resetTimestamp();
    }

    if (changed('timestamp')) {
      if (curSiteFlowId > 0) {
        fetchPickFaceStrategyList01(curSiteFlowId, currentPage, sorts, filters, pageSize);
      }
    }
  }

  componentWillUnmount() {}

  getDocumentColumns() {
    const { intl, sorts, filters, goToDocument } = this.props;

    return [
      {
        width: 50,
        align: 'right',
        title: '',
        // sort field
        dataIndex: 'line_no',
        ...AntDesignTable.getColumnSortProps(sorts, 'line_no'),
        // filter field
        key: 'line_no',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          '#',
          'line_no',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.line_no}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'storage_bin' }),
        // sort field
        dataIndex: 'storage_bin_code',
        ...AntDesignTable.getColumnSortProps(sorts, 'storage_bin_code'),
        // filter field
        key: 'storage_bin_code',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'storage_bin' }),
          'storage_bin_code',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.storage_bin_code}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'item_code' }),
        // sort field
        dataIndex: 'item_code',
        ...AntDesignTable.getColumnSortProps(sorts, 'item_code'),
        // filter field
        key: 'item_code',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'item_code' }),
          'item_code',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.item_code}</>
      },
      {
        width: 150,
        align: 'left',
        title: intl.formatMessage({ id: 'item_desc_01' }),
        // sort field
        dataIndex: 'item_desc_01',
        ...AntDesignTable.getColumnSortProps(sorts, 'item_desc_01'),
        // filter field
        key: 'item_desc_01',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'item_desc_01' }),
          'item_desc_01',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.item_desc_01}</>
      },
      {
        width: 150,
        align: 'left',
        title: intl.formatMessage({ id: 'item_desc_02' }),
        // sort field
        dataIndex: 'item_desc_02',
        ...AntDesignTable.getColumnSortProps(sorts, 'item_desc_02'),
        // filter field
        key: 'item_desc_02',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'item_desc_02' }),
          'item_desc_02',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.item_desc_02}</>
      },
      {
        width: 50,
        // fixed: 'right',
        title: '',
        key: 'action',
        render: (text, record) => (
          <>
            <Button type="dashed" icon="edit" onClick={() => goToDocument(record.id)} />
          </>
        )
      }
    ];
  }

  useOnTableChange(pagination, filters, sorter) {
    const { resetTimestamp } = this.props;

    // process the filters
    const processedFilters = {};
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value && value[0]) {
        const filterVal = value.reduce((lastObj, data) => {
          if (lastObj.length === 0) {
            return data;
          }
          return `${lastObj},${data}`;
        }, '');
        processedFilters[key] = filterVal;
      }
    });

    // process the sorts, now just support 1 column
    const processedSorts = {};
    if (Object.entries(sorter).length > 0) {
      processedSorts[sorter.field] = sorter.order;
    }

    resetTimestamp(pagination.current, processedSorts, processedFilters, pagination.pageSize);
  }

  handleSearch(selectedKeys, confirm) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    confirm();
  }

  handleReset(clearFilters) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    clearFilters();
  }

  useShowWorkspace() {
    const { setWorkspaceVisible } = this.props;

    setWorkspaceVisible(true);
  }

  render() {
    const {
      resetTimestamp,
      currentPage,
      pageSize,
      total,
      documents,
      fetchIsLoading,
      selectedDocuments,
      addSelectedDocuments,
      removeSelectedDocuments
    } = this.props;

    return (
      <>
        <Table
          size="small"
          // rowSelection={rowSelection}
          rowKey="id"
          pagination={{
            current: currentPage,
            pageSize: parseInt(pageSize, 10),
            total,
            showTotal: () => `${total} items`
          }}
          columns={this.getDocumentColumns()}
          dataSource={documents}
          loading={fetchIsLoading}
          bordered
          rowSelection={{
            selectedRowKeys: selectedDocuments.map(value => value.id),
            onChange: () => {
              // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            onSelect: (record, selected) => {
              // console.log(`onSelect record: `, record, 'selected: ', selected, 'selectedRows: ', selectedRows);
              if (selected === true) {
                addSelectedDocuments([record]);
              } else {
                removeSelectedDocuments([record]);
              }
            },
            onSelectAll: (selected, selectedRows, changeRows) => {
              // console.log('onSelectAll selected: ', selected, 'selectedRows: ', selectedRows, 'changeRows: ', changeRows);
              if (selected === true) {
                addSelectedDocuments(changeRows);
              } else {
                removeSelectedDocuments(changeRows);
              }
            },
            onSelectInvert: selectedRows => {
              // console.log('onSelectInvert selectedRows: ', selectedRows);
              removeSelectedDocuments(selectedRows);
            },
            getCheckboxProps: () => ({
              // Column configuration not to be checked
              disabled: true
              // name: record.name,
            })
          }}
          onChange={this.useOnTableChange}
          title={() => (
            <>
              <Row type="flex" justify="space-between" gutter={[0, 16]}>
                <Col span={8}>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={fetchIsLoading}
                    onClick={() => resetTimestamp()}
                    icon="reload"
                  />
                </Col>
              </Row>
            </>
          )}
          // scroll={{ x: 950 }}
        />

        <div
          style={{
            position: 'fixed',
            bottom: '50px',
            right: '50px'
          }}
        >
          <Badge count={selectedDocuments.length}>
            <Button
              size="large"
              type="primary"
              shape="circle"
              icon="laptop"
              onClick={this.useShowWorkspace}
              loading={fetchIsLoading}
            />
          </Badge>
        </div>
      </>
    );
  }
}

PickFaceStrategyList01Table.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  resetTimestamp: PropTypes.func,
  goToDocument: PropTypes.func,

  setWorkspaceVisible: PropTypes.func,

  addSelectedDocuments: PropTypes.func,
  removeSelectedDocuments: PropTypes.func,
  documents: PropTypes.arrayOf(PropTypes.object),
  selectedDocuments: PropTypes.arrayOf(PropTypes.object),

  sorts: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  filters: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),

  currentPage: PropTypes.number,
  pageSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  total: PropTypes.number,
  fetchIsLoading: PropTypes.bool,

  curSiteFlowId: PropTypes.number,
  fetchPickFaceStrategyList01: PropTypes.func
};

PickFaceStrategyList01Table.defaultProps = {
  intl: {},
  resetTimestamp() {},
  goToDocument() {},

  setWorkspaceVisible() {},

  addSelectedDocuments() {},
  removeSelectedDocuments() {},
  documents: [],
  selectedDocuments: [],

  sorts: {},
  filters: {},

  currentPage: 1,
  pageSize: '20',
  total: 100,
  fetchIsLoading: false,

  curSiteFlowId: 0,
  fetchPickFaceStrategyList01() {}
};

const mapStateToProps = state => ({
  timestamp: state.pickFaceStrategyList01.timestamp,
  workspaceIsVisible: state.pickFaceStrategyList01.workspaceIsVisible,

  documents: state.pickFaceStrategyList01.documents,
  selectedDocuments: state.pickFaceStrategyList01.selectedDocuments,

  sorts: state.pickFaceStrategyList01.sorts,
  filters: state.pickFaceStrategyList01.filters,

  currentPage: state.pickFaceStrategyList01.currentPage,
  pageSize: state.pickFaceStrategyList01.pageSize,
  total: state.pickFaceStrategyList01.total,
  fetchIsLoading: state.pickFaceStrategyList01.fetchIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  resetTimestamp: (currentPage, sorts, filters, pageSize) =>
    dispatch(
      PickFaceStrategyList01Actions.pickFaceStrategyList01ResetTimestamp(
        currentPage,
        sorts,
        filters,
        pageSize
      )
    ),
  goToDocument: hdrId =>
    dispatch(PickFaceStrategyList01Actions.pickFaceStrategyList01GoToDocument(hdrId)),
  setWorkspaceVisible: boolean =>
    dispatch(PickFaceStrategyList01Actions.pickFaceStrategyList01SetWorkspaceVisible(boolean)),

  addSelectedDocuments: selectedDocuments =>
    dispatch(
      PickFaceStrategyList01Actions.pickFaceStrategyList01AddSelectedDocuments(selectedDocuments)
    ),
  removeSelectedDocuments: selectedDocuments =>
    dispatch(
      PickFaceStrategyList01Actions.pickFaceStrategyList01RemoveSelectedDocuments(selectedDocuments)
    ),

  fetchPickFaceStrategyList01: (siteFlowId, currentPage, sorts, filters, pageSize) =>
    dispatch(
      PickFaceStrategyList01Actions.pickFaceStrategyList01FetchPickFaceStrategyList01(
        siteFlowId,
        currentPage,
        sorts,
        filters,
        pageSize
      )
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(PickFaceStrategyList01Table));
