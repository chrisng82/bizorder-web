import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Card, Tabs, Tooltip } from 'antd';

import BinTrfProcessActions from '../../Stores/BinTrfProcess/Actions';

import BinTrf02Table from '../BinTrf02/BinTrf02Table';
import WhseJob1702Table from '../WhseJob1702/WhseJob1702Table';
import WhseJob170201Table from '../WhseJob170201/WhseJob170201Table';

const { TabPane } = Tabs;

class BinTrfProcessScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, fetchBinTrfProcess } = this.props;
    if (curSiteFlowId > 0) {
      fetchBinTrfProcess(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, fetchBinTrfProcess } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        fetchBinTrfProcess(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match, process, appPath } = this.props;

    return (
      <Card>
        <Tabs type="card" activeKey={match.params.proc_type}>
          {process.map(value => (
            <TabPane
              tab={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <Tooltip
                  placement="bottom"
                  title={intl.formatMessage({ id: `${value.proc_type.toLowerCase()}_desc` })}
                >
                  <Link
                    style={value.proc_type !== match.params.proc_type ? { color: 'black' } : {}}
                    to={`${appPath}/binTrfProcess/index/${value.proc_type}`}
                  >
                    <FormattedMessage id={`${value.proc_type.toLowerCase()}_label`} />
                  </Link>
                </Tooltip>
              }
              key={value.proc_type}
            />
          ))}
        </Tabs>

        {match.params.proc_type === 'BIN_TRF_02' && <BinTrf02Table match={match} />}
        {match.params.proc_type === 'WHSE_JOB_17_02' && <WhseJob1702Table match={match} />}
        {match.params.proc_type === 'WHSE_JOB_17_02_01' && <WhseJob170201Table match={match} />}
      </Card>
    );
  }
}

BinTrfProcessScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  appPath: PropTypes.string,
  process: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number,
  fetchBinTrfProcess: PropTypes.func
};

BinTrfProcessScreen.defaultProps = {
  intl: {},
  match: {},
  appPath: '',
  process: [],

  curSiteFlowId: 0,
  fetchBinTrfProcess() {}
};

const mapStateToProps = state => ({
  appPath: state.app.appPath,
  process: state.binTrfProcess.process,
  processIsLoading: state.binTrfProcess.processIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  fetchBinTrfProcess: siteFlowId =>
    dispatch(BinTrfProcessActions.binTrfProcessFetchBinTrfProcess(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(BinTrfProcessScreen));
