import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import { Typography, Select, Row, Col, Card, Modal, Breadcrumb } from 'antd';
import HeaderForm from './HeaderForm';
import DetailTable from './DetailTable';
import WhseJobType16Form from './WhseJobType16Form';
import WhseJobType17Form from './WhseJobType17Form';
import WhseJobType18Form from './WhseJobType18Form';
import WhseJobType19Form from './WhseJobType19Form';
import WhseJobType20Form from './WhseJobType20Form';
import WhseJob17DetailActions from '../../Stores/WhseJob17Detail/Actions';

const { Option } = Select;
const { Text } = Typography;

class WhseJob17DetailScreen extends React.PureComponent {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  componentDidMount() {
    const { match, setHdrId } = this.props;

    if (match.params.action === 'create') {
      setHdrId(0);
    } else if (match.params.action === 'update') {
      setHdrId(parseInt(match.params.id, 10));
    }
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  handleCancel() {
    const { setDetailVisible, documentIsLoading } = this.props;

    if (documentIsLoading === false) {
      setDetailVisible(false);
    }
  }

  handleSelectChange(value) {
    const { setWhseJobType } = this.props;

    setWhseJobType(value);
  }

  render() {
    const { intl, match, detailIsVisible, whseJobType, documentHeader, appPath } = this.props;

    const disabled = documentHeader && documentHeader.doc_status >= 50;

    return (
      <Card title={intl.formatMessage({ id: 'bin_transfer_warehouse_job' })}>
        <Breadcrumb>
          {documentHeader.doc_flows.map(docData => (
            <Breadcrumb.Item key={docData.doc_id}>
              {docData.is_current === true ? (
                docData.doc_code
              ) : (
                <Link replace to={`${appPath}/${docData.action}/update/${docData.doc_id}`}>
                  <span style={{ color: 'blue' }}>{docData.doc_code}</span>
                </Link>
              )}
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
        <HeaderForm match={match} />
        <DetailTable />
        <Modal
          visible={detailIsVisible}
          title={intl.formatMessage({ id: 'bin_transfer_warehouse_job_detail' })}
          // style={{top:20}}
          width="90%"
          centered
          forceRender
          // onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
          <Row type="flex" justify="center" gutter={[0, 8]}>
            <Col span={3}>
              <Text>{intl.formatMessage({ id: 'type' })}</Text>
            </Col>
            <Col span={1}>
              <Text>:</Text>
            </Col>
            <Col span={20}>
              <Select
                value={whseJobType}
                onChange={this.handleSelectChange}
                style={{ width: 350 }}
                disabled={disabled}
              >
                <Option value={0}>{intl.formatMessage({ id: 'please_select_type' })}</Option>
                <Option value={16}>
                  {intl.formatMessage({ id: 'transfer_full_pallet_to_bin' })}
                </Option>
                <Option value={17}>
                  {intl.formatMessage({ id: 'transfer_stock_from_pallet_a_to_pallet_b' })}
                </Option>
                <Option value={18}>
                  {intl.formatMessage({ id: 'transfer_stock_from_pallet_to_pick_face' })}
                </Option>
                <Option value={19}>
                  {intl.formatMessage({ id: 'transfer_stock_from_pick_face_a_to_pick_face_b' })}
                </Option>
                <Option value={20}>
                  {intl.formatMessage({ id: 'transfer_full_pallet_to_pick_face' })}
                </Option>
              </Select>
            </Col>
          </Row>
          {whseJobType === 16 && <WhseJobType16Form match={match} />}
          {whseJobType === 17 && <WhseJobType17Form match={match} />}
          {whseJobType === 18 && <WhseJobType18Form match={match} />}
          {whseJobType === 19 && <WhseJobType19Form match={match} />}
          {whseJobType === 20 && <WhseJobType20Form match={match} />}
        </Modal>
      </Card>
    );
  }
}

WhseJob17DetailScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  setHdrId: PropTypes.func,
  setDetailVisible: PropTypes.func,
  setWhseJobType: PropTypes.func,
  documentHeader: PropTypes.shape({
    doc_status: PropTypes.number,
    doc_flows: PropTypes.arrayOf(PropTypes.object)
  }),
  documentIsLoading: PropTypes.bool,
  detailIsVisible: PropTypes.bool,

  whseJobType: PropTypes.number,
  appPath: PropTypes.string
};

WhseJob17DetailScreen.defaultProps = {
  intl: {},
  match: {},
  setHdrId() {},
  setDetailVisible() {},
  setWhseJobType() {},
  documentHeader: { doc_status: 3, doc_flows: [] },
  documentIsLoading: false,
  detailIsVisible: false,

  whseJobType: 0,
  appPath: ''
};

const mapStateToProps = state => ({
  documentHeader: state.whseJob17Detail.documentHeader,
  documentIsLoading: state.whseJob17Detail.documentIsLoading,
  detailIsVisible: state.whseJob17Detail.detailIsVisible,

  whseJobType: state.whseJob17Detail.whseJobType,
  appPath: state.app.appPath
});

const mapDispatchToProps = dispatch => ({
  setHdrId: hdrId => dispatch(WhseJob17DetailActions.whseJob17DetailSetHdrId(hdrId)),
  setDetailVisible: boolean =>
    dispatch(WhseJob17DetailActions.whseJob17DetailSetDetailVisible(boolean)),

  setWhseJobType: whseJobType =>
    dispatch(WhseJob17DetailActions.whseJob17DetailSetWhseJobType(whseJobType))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(WhseJob17DetailScreen));
