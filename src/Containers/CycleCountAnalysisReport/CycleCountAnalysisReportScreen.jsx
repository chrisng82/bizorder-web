import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import treeChanges from 'tree-changes';

import { Card, Collapse } from 'antd';
import CriteriaForm from './CriteriaForm';
import ReportTable from './ReportTable';

import CycleCountAnalysisReportActions from '../../Stores/CycleCountAnalysisReport/Actions';

const { Panel } = Collapse;

class CycleCountAnalysisReportScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, initCycleCountAnalysis } = this.props;

    if (curSiteFlowId > 0) {
      initCycleCountAnalysis(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, initCycleCountAnalysis } = this.props;
    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        initCycleCountAnalysis(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'cycle_count_analysis_report' })}>
        <Collapse defaultActiveKey={['2']}>
          <Panel header={intl.formatMessage({ id: 'criteria' })} key="1">
            <CriteriaForm match={match} />
          </Panel>
          <Panel header={intl.formatMessage({ id: 'report' })} key="2">
            <ReportTable match={match} />
          </Panel>
        </Collapse>
      </Card>
    );
  }
}

CycleCountAnalysisReportScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,

  initCycleCountAnalysis: PropTypes.func,
  curSiteFlowId: PropTypes.number
};

CycleCountAnalysisReportScreen.defaultProps = {
  intl: {},
  match: {},

  initCycleCountAnalysis: {},
  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  initCycleCountAnalysis: siteFlowId =>
    dispatch(
      CycleCountAnalysisReportActions.cycleCountAnalysisReportInitCycleCountAnalysis(siteFlowId)
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(CycleCountAnalysisReportScreen));
