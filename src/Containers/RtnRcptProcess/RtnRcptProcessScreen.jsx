import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Card, Tabs, Tooltip } from 'antd';

import RtnRcptProcessActions from '../../Stores/RtnRcptProcess/Actions';

import GdsRcpt02Table from '../GdsRcpt02/GdsRcpt02Table';
import GdsRcpt0201Table from '../GdsRcpt0201/GdsRcpt0201Table';

const { TabPane } = Tabs;

class RtnRcptProcessScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, fetchRtnRcptProcess } = this.props;
    if (curSiteFlowId > 0) {
      fetchRtnRcptProcess(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, fetchRtnRcptProcess } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        fetchRtnRcptProcess(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match, process, appPath } = this.props;

    return (
      <Card>
        <Tabs type="card" activeKey={match.params.proc_type}>
          {process.map(value => (
            <TabPane
              tab={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <Tooltip
                  placement="bottom"
                  title={intl.formatMessage({ id: `${value.proc_type.toLowerCase()}_desc` })}
                >
                  <Link
                    style={value.proc_type !== match.params.proc_type ? { color: 'black' } : {}}
                    to={`${appPath}/rtnRcptProcess/index/${value.proc_type}`}
                  >
                    <FormattedMessage id={`${value.proc_type.toLowerCase()}_label`} />
                  </Link>
                </Tooltip>
              }
              key={value.proc_type}
            />
          ))}
        </Tabs>

        {match.params.proc_type === 'GDS_RCPT_02' && <GdsRcpt02Table match={match} />}
        {match.params.proc_type === 'GDS_RCPT_02_01' && <GdsRcpt0201Table match={match} />}
      </Card>
    );
  }
}

RtnRcptProcessScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  appPath: PropTypes.string,
  process: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number,
  fetchRtnRcptProcess: PropTypes.func
};

RtnRcptProcessScreen.defaultProps = {
  intl: {},
  match: {},
  appPath: '',
  process: [],

  curSiteFlowId: 0,
  fetchRtnRcptProcess() {}
};

const mapStateToProps = state => ({
  appPath: state.app.appPath,
  process: state.rtnRcptProcess.process,
  processIsLoading: state.rtnRcptProcess.processIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  fetchRtnRcptProcess: siteFlowId =>
    dispatch(RtnRcptProcessActions.rtnRcptProcessFetchRtnRcptProcess(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(RtnRcptProcessScreen));
