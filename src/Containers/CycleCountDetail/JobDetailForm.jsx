import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';

import { Button, Row, Col, Typography } from 'antd';
import { FormikSelect, FormikButton, FormikInput } from '../../Components/Formik';
import CycleCountDetailActions from '../../Stores/CycleCountDetail/Actions';

const { Text } = Typography;

class JobDetailForm extends React.PureComponent {
  constructor() {
    super();

    this.handleSearchStorageRow = this.handleSearchStorageRow.bind(this);
    this.handleSearchStorageBay = this.handleSearchStorageBay.bind(this);
  }

  componentDidMount() {}

  componentDidUpdate() {}

  componentWillUnmount() {}

  handleSearchStorageRow(form, value) {
    const { curSiteFlowId, fetchStorageRowOptions } = this.props;

    fetchStorageRowOptions(curSiteFlowId, value);
  }

  handleSearchStorageBay(form, value) {
    const { curSiteFlowId, fetchStorageBayOptions } = this.props;

    fetchStorageBayOptions(curSiteFlowId, value);
  }

  render() {
    const {
      intl,
      hdrId,
      documentHeader,
      createJobDetail,
      documentIsLoading,
      storageRowIsLoading,
      storageRowOptions,
      storageBayIsLoading,
      storageBayOptions
    } = this.props;

    const levelOptions = [
      {
        value: 1,
        label: 1
      },
      {
        value: 2,
        label: 2
      },
      {
        value: 3,
        label: 3
      },
      {
        value: 4,
        label: 4
      },
      {
        value: 5,
        label: 5
      },
      {
        value: 6,
        label: 6
      },
      {
        value: 7,
        label: 7
      },
      {
        value: 8,
        label: 8
      },
      {
        value: 9,
        label: 9
      },
      {
        value: 10,
        label: 10
      }
    ];

    const disabled = documentHeader && documentHeader.doc_status >= 50;
    return (
      <Formik
        enableReinitialize
        initialValues={{ desc: '', row_ids_select2: [], bay_ids_select2: [], levels_select2: [] }}
        onSubmit={(values, formikBag) => {
          const rowIds = values.row_ids_select2.map(d => d.value);
          const bayIds = values.bay_ids_select2.map(d => d.value);
          const levels = values.levels_select2.map(d => d.value);
          createJobDetail(formikBag, hdrId, values.desc, rowIds, bayIds, levels);
        }}
        validationSchema={Yup.object().shape({
          desc: Yup.string().required(intl.formatMessage({ id: 'description_is_required' })),
          row_ids_select2: Yup.array().min(1, intl.formatMessage({ id: 'storage_row_is_required' }))
        })}
      >
        {({
          values,
          // handleChange,
          // errors,
          // setFieldTouched,
          dirty,
          // touched,
          isSubmitting,
          // isValid,
          handleSubmit,
          handleReset
        }) => (
          <Form>
            <Row type="flex" justify="center" gutter={[8, 8]}>
              <Col span={3}>
                <Text>{intl.formatMessage({ id: 'description' })}</Text>
              </Col>
              <Col span={1}>
                <Text>:</Text>
              </Col>
              <Col span={20}>
                <Field name="desc" component={FormikInput} />
              </Col>
            </Row>

            <Row type="flex" justify="center" gutter={[8, 8]}>
              <Col span={3}>
                <Text>{intl.formatMessage({ id: 'storage_row' })}</Text>
              </Col>
              <Col span={1}>
                <Text>:</Text>
              </Col>
              <Col span={20}>
                <Field
                  name="row_ids_select2"
                  component={FormikSelect}
                  showArrow
                  filterOption={false}
                  onSearch={this.handleSearchStorageRow}
                  notFoundContent={null}
                  loading={storageRowIsLoading}
                  options={storageRowOptions}
                  disabled={disabled}
                  selectMode="multiple"
                />
              </Col>
            </Row>

            <Row type="flex" justify="center" gutter={[8, 8]}>
              <Col span={3}>
                <Text>{intl.formatMessage({ id: 'storage_bay' })}</Text>
              </Col>
              <Col span={1}>
                <Text>:</Text>
              </Col>
              <Col span={20}>
                <Field
                  name="bay_ids_select2"
                  component={FormikSelect}
                  showArrow
                  filterOption={false}
                  onSearch={this.handleSearchStorageBay}
                  notFoundContent={null}
                  loading={storageBayIsLoading}
                  options={storageBayOptions}
                  disabled={disabled}
                  selectMode="multiple"
                />
              </Col>
            </Row>

            <Row type="flex" justify="center" gutter={[8, 8]}>
              <Col span={3}>
                <Text>{intl.formatMessage({ id: 'level' })}</Text>
              </Col>
              <Col span={1}>
                <Text>:</Text>
              </Col>
              <Col span={20}>
                <Field
                  name="levels_select2"
                  component={FormikSelect}
                  showArrow
                  filterOption={false}
                  // onSearch={this.handleSearchStorageBay}
                  notFoundContent={null}
                  options={levelOptions}
                  disabled={disabled}
                  selectMode="multiple"
                />
              </Col>
            </Row>

            <Row type="flex" justify="end" gutter={[0, 16]}>
              <Col span={5}>
                <Button
                  type="primary"
                  disabled={!dirty}
                  loading={isSubmitting || documentIsLoading}
                  onClick={handleReset}
                  icon="undo"
                >
                  {intl.formatMessage({ id: 'reset' })}
                </Button>
                <Field
                  type="primary"
                  name="submit_action"
                  value="update"
                  component={FormikButton}
                  disabled={disabled}
                  loading={isSubmitting || documentIsLoading}
                  onClick={handleSubmit}
                  label={intl.formatMessage({
                    id: values.id > 0 ? 'update' : 'create'
                  })}
                  icon="save"
                />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    );
  }
}

JobDetailForm.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  createJobDetail: PropTypes.func,
  hdrId: PropTypes.number,
  documentHeader: PropTypes.shape({
    doc_status: PropTypes.number
  }),
  documentIsLoading: PropTypes.bool,

  fetchStorageRowOptions: PropTypes.func,
  storageRowIsLoading: PropTypes.bool,
  storageRowOptions: PropTypes.arrayOf(PropTypes.object),

  fetchStorageBayOptions: PropTypes.func,
  storageBayIsLoading: PropTypes.bool,
  storageBayOptions: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number
};

JobDetailForm.defaultProps = {
  intl: {},
  createJobDetail() {},
  hdrId: 0,
  documentHeader: { doc_status: 3 },
  documentIsLoading: false,

  fetchStorageRowOptions() {},
  storageRowIsLoading: false,
  storageRowOptions: [],

  fetchStorageBayOptions() {},
  storageBayIsLoading: false,
  storageBayOptions: [],

  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  hdrId: state.cycleCountDetail.hdrId,

  storageRowIsLoading: state.cycleCountDetail.storageRowIsLoading,
  storageRowOptions: state.cycleCountDetail.storageRowOptions,

  storageBayIsLoading: state.cycleCountDetail.storageBayIsLoading,
  storageBayOptions: state.cycleCountDetail.storageBayOptions,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  createJobDetail: (formikBag, hdrId, desc, rowIds, bayIds, levels) =>
    dispatch(
      CycleCountDetailActions.cycleCountDetailCreateJobDetail(
        formikBag,
        hdrId,
        desc,
        rowIds,
        bayIds,
        levels
      )
    ),

  fetchStorageRowOptions: (siteFlowId, search) =>
    dispatch(CycleCountDetailActions.cycleCountDetailFetchStorageRowOptions(siteFlowId, search)),
  fetchStorageBayOptions: (siteFlowId, search) =>
    dispatch(CycleCountDetailActions.cycleCountDetailFetchStorageBayOptions(siteFlowId, search))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(JobDetailForm));
