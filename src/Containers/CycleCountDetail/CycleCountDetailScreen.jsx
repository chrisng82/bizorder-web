import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import { Typography, Row, Col, Card, Modal, Select, Breadcrumb } from 'antd';
import HeaderForm from './HeaderForm';
import DetailTable from './DetailTable';
import WhseJobType14Form from './WhseJobType14Form';
import WhseJobType15Form from './WhseJobType15Form';
import JobDetailForm from './JobDetailForm';
import JobDetailTable from './JobDetailTable';
import CycleCountDetailActions from '../../Stores/CycleCountDetail/Actions';

const { Option } = Select;
const { Text } = Typography;

class CycleCountDetailScreen extends React.PureComponent {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
    this.jobHandleCancel = this.jobHandleCancel.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  componentDidMount() {
    const { match, setHdrId } = this.props;

    if (match.params.action === 'create') {
      setHdrId(0);
    } else if (match.params.action === 'update') {
      setHdrId(parseInt(match.params.id, 10));
    }
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  handleCancel() {
    const { setDetailVisible, documentIsLoading } = this.props;

    if (documentIsLoading === false) {
      setDetailVisible(false);
    }
  }

  jobHandleCancel() {
    const { setJobDetailVisible, documentIsLoading } = this.props;

    if (documentIsLoading === false) {
      setJobDetailVisible(false);
    }
  }

  handleSelectChange(value) {
    const { setWhseJobType } = this.props;

    setWhseJobType(value);
  }

  render() {
    const {
      intl,
      match,
      detailIsVisible,
      jobDetailIsVisible,
      whseJobType,
      documentHeader,
      appPath
    } = this.props;

    const disabled = documentHeader && documentHeader.doc_status >= 50;

    return (
      <Card title={intl.formatMessage({ id: 'cycle_count' })}>
        <Breadcrumb>
          {documentHeader.doc_flows.map(docData => (
            <Breadcrumb.Item key={docData.doc_id}>
              {docData.is_current === true ? (
                docData.doc_code
              ) : (
                <Link replace to={`${appPath}/${docData.action}/update/${docData.doc_id}`}>
                  <span style={{ color: 'blue' }}>{docData.doc_code}</span>
                </Link>
              )}
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
        <HeaderForm match={match} />
        <DetailTable />
        <Modal
          visible={detailIsVisible}
          title={intl.formatMessage({ id: 'cycle_count_detail' })}
          // style={{top:20}}
          width="90%"
          centered
          forceRender
          // onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
          <Row type="flex" justify="center" gutter={[0, 8]}>
            <Col span={3}>
              <Text>{intl.formatMessage({ id: 'type' })}</Text>
            </Col>
            <Col span={1}>
              <Text>:</Text>
            </Col>
            <Col span={20}>
              <Select
                value={whseJobType}
                onChange={this.handleSelectChange}
                style={{ width: 350 }}
                disabled={disabled}
              >
                <Option value={0}>{intl.formatMessage({ id: 'please_select_type' })}</Option>
                <Option value={14}>{intl.formatMessage({ id: 'cycle_count_pallet' })}</Option>
                <Option value={15}>{intl.formatMessage({ id: 'cycle_count_loose' })}</Option>
              </Select>
            </Col>
          </Row>
          {whseJobType === 14 && <WhseJobType14Form match={match} />}
          {whseJobType === 15 && <WhseJobType15Form match={match} />}
        </Modal>
        <Modal
          visible={jobDetailIsVisible}
          title={intl.formatMessage({ id: 'create_cycle_count_job' })}
          // style={{top:20}}
          width="90%"
          centered
          forceRender
          // onOk={this.handleOk}
          onCancel={this.jobHandleCancel}
          footer={null}
        >
          <JobDetailForm match={match} />
          <JobDetailTable match={match} />
        </Modal>
      </Card>
    );
  }
}

CycleCountDetailScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  setHdrId: PropTypes.func,
  setDetailVisible: PropTypes.func,
  setJobDetailVisible: PropTypes.func,
  setWhseJobType: PropTypes.func,
  documentHeader: PropTypes.shape({
    doc_status: PropTypes.number,
    doc_flows: PropTypes.arrayOf(PropTypes.object)
  }),
  documentIsLoading: PropTypes.bool,
  detailIsVisible: PropTypes.bool,
  jobDetailIsVisible: PropTypes.bool,

  whseJobType: PropTypes.number,
  appPath: PropTypes.string
};

CycleCountDetailScreen.defaultProps = {
  intl: {},
  match: {},
  setHdrId() {},
  setDetailVisible() {},
  setJobDetailVisible() {},
  setWhseJobType() {},
  documentHeader: { doc_status: 3, doc_flows: [] },
  documentIsLoading: false,
  detailIsVisible: false,
  jobDetailIsVisible: false,

  whseJobType: 0,
  appPath: ''
};

const mapStateToProps = state => ({
  documentHeader: state.cycleCountDetail.documentHeader,
  documentIsLoading: state.cycleCountDetail.documentIsLoading,
  detailIsVisible: state.cycleCountDetail.detailIsVisible,
  jobDetailIsVisible: state.cycleCountDetail.jobDetailIsVisible,

  whseJobType: state.cycleCountDetail.whseJobType,
  appPath: state.app.appPath
});

const mapDispatchToProps = dispatch => ({
  setHdrId: hdrId => dispatch(CycleCountDetailActions.cycleCountDetailSetHdrId(hdrId)),
  setDetailVisible: boolean =>
    dispatch(CycleCountDetailActions.cycleCountDetailSetDetailVisible(boolean)),
  setJobDetailVisible: boolean =>
    dispatch(CycleCountDetailActions.cycleCountDetailSetJobDetailVisible(boolean)),

  setWhseJobType: whseJobType =>
    dispatch(CycleCountDetailActions.cycleCountDetailSetWhseJobType(whseJobType))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(CycleCountDetailScreen));
