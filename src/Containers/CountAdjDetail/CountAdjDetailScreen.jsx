import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import { Card, Modal, Breadcrumb } from 'antd';
import HeaderForm from './HeaderForm';
import DetailTable from './DetailTable';
import DetailForm from './DetailForm';
import CountAdjDetailActions from '../../Stores/CountAdjDetail/Actions';

class CountAdjDetailScreen extends React.PureComponent {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
  }

  componentDidMount() {
    const { match, setHdrId } = this.props;

    if (match.params.action === 'create') {
      setHdrId(0);
    } else if (match.params.action === 'update') {
      setHdrId(parseInt(match.params.id, 10));
    }
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  handleCancel() {
    const { setDetailVisible, documentIsLoading } = this.props;

    if (documentIsLoading === false) {
      setDetailVisible(false);
    }
  }

  render() {
    const { intl, match, detailIsVisible, documentHeader, appPath } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'count_adjustment' })}>
        <Breadcrumb>
          {documentHeader.doc_flows.map(docData => (
            <Breadcrumb.Item key={docData.doc_id}>
              {docData.is_current === true ? (
                docData.doc_code
              ) : (
                <Link replace to={`${appPath}/${docData.action}/update/${docData.doc_id}`}>
                  <span style={{ color: 'blue' }}>{docData.doc_code}</span>
                </Link>
              )}
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
        <HeaderForm match={match} />
        <DetailTable />
        <Modal
          visible={detailIsVisible}
          title={intl.formatMessage({ id: 'count_adjustment_detail' })}
          // style={{top:20}}
          width="90%"
          centered
          forceRender
          // onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
          <DetailForm match={match} />
        </Modal>
      </Card>
    );
  }
}

CountAdjDetailScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  setHdrId: PropTypes.func,
  setDetailVisible: PropTypes.func,
  documentHeader: PropTypes.shape({
    doc_status: PropTypes.number,
    doc_flows: PropTypes.arrayOf(PropTypes.object)
  }),
  documentIsLoading: PropTypes.bool,
  detailIsVisible: PropTypes.bool,

  appPath: PropTypes.string
};

CountAdjDetailScreen.defaultProps = {
  intl: {},
  match: {},
  setHdrId() {},
  setDetailVisible() {},
  documentHeader: { doc_status: 3, doc_flows: [] },
  documentIsLoading: false,
  detailIsVisible: false,

  appPath: ''
};

const mapStateToProps = state => ({
  documentHeader: state.countAdjDetail.documentHeader,
  documentIsLoading: state.countAdjDetail.documentIsLoading,
  detailIsVisible: state.countAdjDetail.detailIsVisible,

  appPath: state.app.appPath
});

const mapDispatchToProps = dispatch => ({
  setHdrId: hdrId => dispatch(CountAdjDetailActions.countAdjDetailSetHdrId(hdrId)),
  setDetailVisible: boolean =>
    dispatch(CountAdjDetailActions.countAdjDetailSetDetailVisible(boolean))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(CountAdjDetailScreen));
