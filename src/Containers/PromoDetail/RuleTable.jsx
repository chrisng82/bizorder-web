import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Table, Popconfirm, Button } from 'antd';

import PromoDetailActions from '../../Stores/PromoDetail/Actions';

class RuleTable extends React.PureComponent {
  constructor() {
    super();

    this.getDocumentColumns = this.getDocumentColumns.bind(this);
    this.useOnSelectItem = this.useOnSelectItem.bind(this);
    this.useCreateRule = this.useCreateRule.bind(this);
    this.useOnDeleteItem = this.useOnDeleteItem.bind(this);
  }

  componentDidMount() {
    const { hdrId, showRules } = this.props;
    if (hdrId > 0) {
        showRules(hdrId);
    }
  }

  componentDidUpdate(prevProps) {
    const { hdrId, showRules } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('timestamp')) {
        showRules(hdrId);
    }
  }

  getDocumentColumns() {
    const { intl } = this.props;
    return [
        {
            width: 200,
            align: 'left',
            title: intl.formatMessage({ id: 'type' }),
            // sort field
            dataIndex: 'type',
            // filter field
            key: 'type',
            render: (text, record) => {
                if (record.type_str)
                    return record.type_str.toUpperCase();
                else
                    return '';
            }
        },
        {
            //width left empty for fluid auto set
            align: 'left',
            title: intl.formatMessage({ id: 'description' }),
            // sort field
            dataIndex: 'description',
            // filter field
            key: 'description',
            render: (text, record) => <><div dangerouslySetInnerHTML={{__html: record.description}} /></>
        },
        {
            width: 80,
            fixed: 'right',
            key: 'action',
            render: (text, record) => (
            <>
                <Button type="dashed" icon="edit" onClick={() => this.useOnSelectItem(record)} />
                <Popconfirm
                placement="left"
                title={intl.formatMessage({ id: 'are_you_sure_to_remove_this_line' })}
                onConfirm={() => this.useOnDeleteItem(record)}
                onCancel={() => {}}
                okText={intl.formatMessage({ id: 'yes' })}
                cancelText={intl.formatMessage({ id: 'cancel' })}
                >
                <Button type="dashed" icon="delete" />
                </Popconfirm>
            </>
            )
        }
    ];
  }

  useOnSelectItem(record) {
    const { setRule, setRuleVisible } = this.props;

    setRule(record);

    setRuleVisible(true);
  }

  useCreateRule() {
    const { initRule, setRule, setRuleVisible } = this.props;

    setRule(initRule);

    setRuleVisible(true);
  }

  useOnDeleteItem(record) {
    const { hdrId, deleteRule } = this.props;

    deleteRule(hdrId, record);
  }

  render() {
    const { intl, hdrId, itemId, rules, documentIsLoading } = this.props;
    return (
      <>
        <Table
          size="small"
          rowKey="id"
          pagination={{ pageSize: 20 }}
          columns={this.getDocumentColumns()}
          dataSource={rules}
          loading={documentIsLoading}
          bordered
          rowClassName={rowData => {
            if ('is_modified' in rowData && rowData.is_modified === true) {
              return 'success-row';
            }
            if (rowData.item_id === itemId) {
              return 'error-row';
            }
            return '';
          }}
          title={() => (
            <Button
              name="add_detail"
              type="primary"
              icon="plus"
              disabled={hdrId === 0}
              loading={documentIsLoading}
              onClick={this.useCreateRule}
            >
              {intl.formatMessage({ id: 'add_rule' })}
            </Button>
          )}
          scroll={{ x: 950 }}
        />
      </>
    );
  }
}

RuleTable.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  showRules: PropTypes.func,
  setRule: PropTypes.func,
  setRuleVisible: PropTypes.func,
  deleteRule: PropTypes.func,
  hdrId: PropTypes.number,
  documentIsLoading: PropTypes.bool,
  rules: PropTypes.arrayOf(PropTypes.object),
  initRule: PropTypes.shape({}),
  debtorId: PropTypes.number
};

RuleTable.defaultProps = {
  intl: {},
  showRules() {},
  setRule() {},
  setRuleVisible() {},
  deleteRule() {},
  hdrId: 0,
  rules: [],
  documentIsLoading: false,
  initRule: {},
  debtorId: 0
};

const mapStateToProps = state => ({
  timestamp: state.promoDetail.timestamp,
  hdrId: state.promoDetail.hdrId,
  rules: state.promoDetail.rules,
  initRule: state.promoDetail.initRule,
  documentIsLoading: state.promoDetail.documentIsLoading
});

const mapDispatchToProps = dispatch => ({
  showRules: hdrId => dispatch(PromoDetailActions.promoDetailShowRules(hdrId)),
  setRuleVisible: boolean => dispatch(PromoDetailActions.promoDetailSetRuleVisible(boolean)),
  setRule: rule =>
    dispatch(PromoDetailActions.promoDetailSetRule(rule)),
  deleteRule: (hdrId, rule) =>
    dispatch(PromoDetailActions.promoDetailDeleteRule(hdrId, rule))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(RuleTable));
