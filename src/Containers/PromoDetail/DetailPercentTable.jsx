import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Table, Popconfirm, Button } from 'antd';

import PromoDetailActions from '../../Stores/PromoDetail/Actions';

class DetailPercentTable extends React.PureComponent {
  constructor() {
    super();

    this.getDocumentColumns = this.getDocumentColumns.bind(this);
    this.useOnSelectItem = this.useOnSelectItem.bind(this);
    this.useCreateDetail = this.useCreateDetail.bind(this);
    this.useOnDeleteItem = this.useOnDeleteItem.bind(this);
  }

  componentDidMount() {
    const { hdrId, showDetails } = this.props;
    if (hdrId > 0) {
      showDetails(hdrId);
    }
  }

  componentDidUpdate(prevProps) {
    const { hdrId, showDetails } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('timestamp')) {
      showDetails(hdrId);
    }
  }

  getDocumentColumns() {
    const { intl } = this.props;
    return [
      {
        //width left empty for fluid auto set
        align: 'left',
        title: intl.formatMessage({ id: 'item' }),
        // sort field
        dataIndex: 'item',
        // filter field
        key: 'item',
        render: (text, record) => {
          let result = <>-</>;
          if (record.variant_type === 'item') {
            result = (
              <>
                {record.item_code}
                <br />
                {record.item_desc_01}
              </>
            );
          } else if (record.variant_type === 'item_group01') {
            result = <>{record.item_group_01_code}</>;
          } else if (record.variant_type === 'item_group02') {
            result = <>{record.item_group_02_code}</>;
          } else if (record.variant_type === 'item_group03') {
            result = <>{record.item_group_03_code}</>;
          }
          return result;
        }
      },
      {
        width: 150,
        align: 'left',
        title: intl.formatMessage({ id: 'disc' }),
        // sort field
        dataIndex: 'disc',
        // filter field
        key: 'disc',
        render: (text, record) => (
          <>
            {new Intl.NumberFormat([], {
              style: 'decimal',
              minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
              maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
            }).format(record.disc_perc_01)}
            %
          </>
        )
      },
      {
        width: 150,
        align: 'right',
        title: intl.formatMessage({ id: 'min_qty' }),
        // sort field
        dataIndex: 'min_qty',
        // filter field
        key: 'min_qty',
        render: (text, record) => (
          <>
            {new Intl.NumberFormat([], {
              style: 'decimal',
              minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
              maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
            }).format(record.min_qty)}
          </>
        )
      },
      {
        width: 150,
        align: 'right',
        title: intl.formatMessage({ id: 'max_qty' }),
        // sort field
        dataIndex: 'max_qty',
        // filter field
        key: 'max_qty',
        render: (text, record) => (
          <>
            {new Intl.NumberFormat([], {
              style: 'decimal',
              minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
              maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
            }).format(record.max_qty)}
          </>
        )
      },
      {
        width: 80,
        fixed: 'right',
        key: 'action',
        render: (text, record) => (
          <>
            <Button type="dashed" icon="edit" onClick={() => this.useOnSelectItem(record)} />
            <Popconfirm
              placement="left"
              title={intl.formatMessage({ id: 'are_you_sure_to_remove_this_line' })}
              onConfirm={() => this.useOnDeleteItem(record)}
              onCancel={() => {}}
              okText={intl.formatMessage({ id: 'yes' })}
              cancelText={intl.formatMessage({ id: 'cancel' })}
            >
              <Button type="dashed" icon="delete" />
            </Popconfirm>
          </>
        )
      }
    ];
  }

  useOnSelectItem(record) {
    const { setDocumentDetail, setDetailVisible } = this.props;

    setDocumentDetail(record);

    setDetailVisible(true);
  }

  useCreateDetail() {
    const { initDocumentDetail, setDocumentDetail, setDetailVisible } = this.props;

    setDocumentDetail(initDocumentDetail);

    setDetailVisible(true);
  }

  useOnDeleteItem(record) {
    const { hdrId, deleteDetail } = this.props;

    deleteDetail(hdrId, record);
  }

  render() {
    const { intl, hdrId, itemId, documentDetails, documentIsLoading } = this.props;
    return (
      <>
        <Table
          size="small"
          rowKey="id"
          pagination={{ pageSize: 20 }}
          columns={this.getDocumentColumns()}
          dataSource={documentDetails}
          loading={documentIsLoading}
          bordered
          rowClassName={rowData => {
            if ('is_modified' in rowData && rowData.is_modified === true) {
              return 'success-row';
            }
            if (rowData.item_id === itemId) {
              return 'error-row';
            }
            return '';
          }}
          title={() => (
            <Button
              name="add_detail"
              type="primary"
              icon="plus"
              disabled={hdrId === 0}
              loading={documentIsLoading}
              onClick={this.useCreateDetail}
            >
              {intl.formatMessage({ id: 'add_product_variant' })}
            </Button>
          )}
          scroll={{ x: 950 }}
        />
      </>
    );
  }
}

DetailPercentTable.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  showDetails: PropTypes.func,
  setDocumentDetail: PropTypes.func,
  setDetailVisible: PropTypes.func,
  deleteDetail: PropTypes.func,
  hdrId: PropTypes.number,
  documentIsLoading: PropTypes.bool,
  documentDetails: PropTypes.arrayOf(PropTypes.object),
  initDocumentDetail: PropTypes.shape({}),
  itemId: PropTypes.number
};

DetailPercentTable.defaultProps = {
  intl: {},
  showDetails() {},
  setDocumentDetail() {},
  setDetailVisible() {},
  deleteDetail() {},
  hdrId: 0,
  documentDetails: [],
  documentIsLoading: false,
  initDocumentDetail: {},
  itemId: 0
};

const mapStateToProps = state => ({
  timestamp: state.promoDetail.timestamp,
  hdrId: state.promoDetail.hdrId,
  documentDetails: state.promoDetail.documentDetails,
  initDocumentDetail: state.promoDetail.initDocumentDetail,
  documentIsLoading: state.promoDetail.documentIsLoading
});

const mapDispatchToProps = dispatch => ({
  showDetails: hdrId => dispatch(PromoDetailActions.promoDetailShowDetails(hdrId)),
  setDetailVisible: boolean => dispatch(PromoDetailActions.promoDetailSetDetailVisible(boolean)),
  setDocumentDetail: documentDetail =>
    dispatch(PromoDetailActions.promoDetailSetDocumentDetail(documentDetail)),
  deleteDetail: (hdrId, documentDetail) =>
    dispatch(PromoDetailActions.promoDetailDeleteDetail(hdrId, documentDetail))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(DetailPercentTable));
