import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Row, Col, Table, Button, Badge } from 'antd';

import AntDesignTable from '../../Components/AntDesignTable';
import PalletLabelList01Actions from '../../Stores/PalletLabelList01/Actions';

import PalletLabelList01Dialog from './PalletLabelList01Dialog';

class PalletLabelList01Table extends React.PureComponent {
  constructor() {
    super();

    this.useOnTableChange = this.useOnTableChange.bind(this);
    this.useShowWorkspace = this.useShowWorkspace.bind(this);

    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDocumentColumns = this.getDocumentColumns.bind(this);
  }

  componentDidMount() {
    const { curSiteFlowId, setWorkspaceVisible, resetTimestamp } = this.props;

    if (curSiteFlowId > 0) {
      setWorkspaceVisible(false);
      resetTimestamp();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      curSiteFlowId,
      resetTimestamp,
      fetchPalletLabelList01,
      currentPage,
      sorts,
      filters,
      pageSize
    } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      resetTimestamp();
    }

    if (changed('timestamp')) {
      if (curSiteFlowId > 0) {
        fetchPalletLabelList01(curSiteFlowId, currentPage, sorts, filters, pageSize);
      }
    }
  }

  componentWillUnmount() {}

  getDocumentColumns() {
    const { intl, sorts, filters, goToDocument } = this.props;

    return [
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'barcode' }),
        // sort field
        dataIndex: 'barcode',
        ...AntDesignTable.getColumnSortProps(sorts, 'barcode'),
        // filter field
        key: 'barcode',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'barcode' }),
          'barcode',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.barcode}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'ref_code' }),
        // sort field
        dataIndex: 'ref_code_01',
        ...AntDesignTable.getColumnSortProps(sorts, 'ref_code_01'),
        // filter field
        key: 'ref_code_01',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'ref_code' }),
          'ref_code_01',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.ref_code_01}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'created_at' }),
        // sort field
        dataIndex: 'created_at',
        ...AntDesignTable.getColumnSortProps(sorts, 'created_at'),
        // filter field
        key: 'created_at',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'created_at' }),
          'created_at',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.created_at}</>
      },
      {
        width: 80,
        align: 'left',
        title: intl.formatMessage({ id: 'print' }),
        // sort field
        dataIndex: 'print_count',
        ...AntDesignTable.getColumnSortProps(sorts, 'print_count'),
        // filter field
        key: 'print_count',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'print' }),
          'print_count',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.print_count}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'first_printed_at' }),
        // sort field
        dataIndex: 'first_printed_at',
        ...AntDesignTable.getColumnSortProps(sorts, 'first_printed_at'),
        // filter field
        key: 'first_printed_at',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'first_printed_at' }),
          'first_printed_at',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.first_printed_at}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'last_printed_at' }),
        // sort field
        dataIndex: 'last_printed_at',
        ...AntDesignTable.getColumnSortProps(sorts, 'last_printed_at'),
        // filter field
        key: 'last_printed_at',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'last_printed_at' }),
          'last_printed_at',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.last_printed_at}</>
      },
      {
        width: 50,
        // fixed: 'right',
        title: '',
        key: 'action',
        render: (text, record) => (
          <>
            <Button type="dashed" icon="edit" onClick={() => goToDocument(record.id)} />
          </>
        )
      }
    ];
  }

  useOnTableChange(pagination, filters, sorter) {
    const { resetTimestamp } = this.props;

    // process the filters
    const processedFilters = {};
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value && value[0]) {
        const filterVal = value.reduce((lastObj, data) => {
          if (lastObj.length === 0) {
            return data;
          }
          return `${lastObj},${data}`;
        }, '');
        processedFilters[key] = filterVal;
      }
    });

    // process the sorts, now just support 1 column
    const processedSorts = {};
    if (Object.entries(sorter).length > 0) {
      processedSorts[sorter.field] = sorter.order;
    }

    resetTimestamp(pagination.current, processedSorts, processedFilters, pagination.pageSize);
  }

  handleSearch(selectedKeys, confirm) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    confirm();
  }

  handleReset(clearFilters) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    clearFilters();
  }

  useShowWorkspace() {
    const { setWorkspaceVisible } = this.props;

    setWorkspaceVisible(true);
  }

  render() {
    const {
      resetTimestamp,
      currentPage,
      pageSize,
      total,
      documents,
      fetchIsLoading,
      selectedDocuments,
      addSelectedDocuments,
      removeSelectedDocuments
    } = this.props;

    return (
      <>
        <Table
          size="small"
          // rowSelection={rowSelection}
          rowKey="id"
          pagination={{
            current: currentPage,
            pageSize: parseInt(pageSize, 10),
            total,
            showTotal: () => `${total} items`
          }}
          columns={this.getDocumentColumns()}
          dataSource={documents}
          loading={fetchIsLoading}
          bordered
          rowSelection={{
            selectedRowKeys: selectedDocuments.map(value => value.id),
            onChange: () => {
              // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            onSelect: (record, selected) => {
              // console.log(`onSelect record: `, record, 'selected: ', selected, 'selectedRows: ', selectedRows);
              if (selected === true) {
                addSelectedDocuments([record]);
              } else {
                removeSelectedDocuments([record]);
              }
            },
            onSelectAll: (selected, selectedRows, changeRows) => {
              // console.log('onSelectAll selected: ', selected, 'selectedRows: ', selectedRows, 'changeRows: ', changeRows);
              if (selected === true) {
                addSelectedDocuments(changeRows);
              } else {
                removeSelectedDocuments(changeRows);
              }
            },
            onSelectInvert: selectedRows => {
              // console.log('onSelectInvert selectedRows: ', selectedRows);
              removeSelectedDocuments(selectedRows);
            },
            getCheckboxProps: () => ({
              // Column configuration not to be checked
              disabled: false
              // name: record.name,
            })
          }}
          onChange={this.useOnTableChange}
          title={() => (
            <>
              <Row type="flex" justify="space-between" gutter={[0, 16]}>
                <Col span={8}>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={fetchIsLoading}
                    onClick={() => resetTimestamp()}
                    icon="reload"
                  />
                </Col>
              </Row>
            </>
          )}
          // scroll={{ x: 950 }}
        />

        <div
          style={{
            position: 'fixed',
            bottom: '50px',
            right: '50px'
          }}
        >
          <Badge count={selectedDocuments.length}>
            <Button
              size="large"
              type="primary"
              shape="circle"
              icon="laptop"
              onClick={this.useShowWorkspace}
              loading={fetchIsLoading}
            />
          </Badge>
        </div>

        <PalletLabelList01Dialog />
      </>
    );
  }
}

PalletLabelList01Table.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  resetTimestamp: PropTypes.func,
  goToDocument: PropTypes.func,

  setWorkspaceVisible: PropTypes.func,

  addSelectedDocuments: PropTypes.func,
  removeSelectedDocuments: PropTypes.func,
  documents: PropTypes.arrayOf(PropTypes.object),
  selectedDocuments: PropTypes.arrayOf(PropTypes.object),

  sorts: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  filters: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),

  currentPage: PropTypes.number,
  pageSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  total: PropTypes.number,
  fetchIsLoading: PropTypes.bool,

  curSiteFlowId: PropTypes.number,
  fetchPalletLabelList01: PropTypes.func
};

PalletLabelList01Table.defaultProps = {
  intl: {},
  resetTimestamp() {},
  goToDocument() {},

  setWorkspaceVisible() {},

  addSelectedDocuments() {},
  removeSelectedDocuments() {},
  documents: [],
  selectedDocuments: [],

  sorts: {},
  filters: {},

  currentPage: 1,
  pageSize: '20',
  total: 100,
  fetchIsLoading: false,

  curSiteFlowId: 0,
  fetchPalletLabelList01() {}
};

const mapStateToProps = state => ({
  timestamp: state.palletLabelList01.timestamp,
  workspaceIsVisible: state.palletLabelList01.workspaceIsVisible,

  documents: state.palletLabelList01.documents,
  selectedDocuments: state.palletLabelList01.selectedDocuments,

  sorts: state.palletLabelList01.sorts,
  filters: state.palletLabelList01.filters,

  currentPage: state.palletLabelList01.currentPage,
  pageSize: state.palletLabelList01.pageSize,
  total: state.palletLabelList01.total,
  fetchIsLoading: state.palletLabelList01.fetchIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  resetTimestamp: (currentPage, sorts, filters, pageSize) =>
    dispatch(
      PalletLabelList01Actions.palletLabelList01ResetTimestamp(
        currentPage,
        sorts,
        filters,
        pageSize
      )
    ),
  goToDocument: hdrId => dispatch(PalletLabelList01Actions.palletLabelList01GoToDocument(hdrId)),
  setWorkspaceVisible: boolean =>
    dispatch(PalletLabelList01Actions.palletLabelList01SetWorkspaceVisible(boolean)),

  addSelectedDocuments: selectedDocuments =>
    dispatch(PalletLabelList01Actions.palletLabelList01AddSelectedDocuments(selectedDocuments)),
  removeSelectedDocuments: selectedDocuments =>
    dispatch(PalletLabelList01Actions.palletLabelList01RemoveSelectedDocuments(selectedDocuments)),

  fetchPalletLabelList01: (siteFlowId, currentPage, sorts, filters, pageSize) =>
    dispatch(
      PalletLabelList01Actions.palletLabelList01FetchPalletLabelList01(
        siteFlowId,
        currentPage,
        sorts,
        filters,
        pageSize
      )
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(PalletLabelList01Table));
