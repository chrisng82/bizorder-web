import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import treeChanges from 'tree-changes';

import { Card, Collapse } from 'antd';
import CriteriaForm from './CriteriaForm';
import ReportTable from './ReportTable';

import ReservedStockReportActions from '../../Stores/ReservedStockReport/Actions';

const { Panel } = Collapse;

class ReservedStockReportScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, initReservedStock } = this.props;

    if (curSiteFlowId > 0) {
      initReservedStock(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, initReservedStock } = this.props;
    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        initReservedStock(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'reserved_stock_report' })}>
        <Collapse defaultActiveKey={['2']}>
          <Panel header={intl.formatMessage({ id: 'criteria' })} key="1">
            <CriteriaForm match={match} />
          </Panel>
          <Panel header={intl.formatMessage({ id: 'report' })} key="2">
            <ReportTable match={match} />
          </Panel>
        </Collapse>
      </Card>
    );
  }
}

ReservedStockReportScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,

  initReservedStock: PropTypes.func,
  curSiteFlowId: PropTypes.number
};

ReservedStockReportScreen.defaultProps = {
  intl: {},
  match: {},

  initReservedStock: {},
  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  initReservedStock: siteFlowId =>
    dispatch(ReservedStockReportActions.reservedStockReportInitReservedStock(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(ReservedStockReportScreen));
