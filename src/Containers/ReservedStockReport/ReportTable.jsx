import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';

import { Table, Row, Col, Button } from 'antd';

import FileSaver from 'file-saver';
import XLSX from 'xlsx';

class ReportTable extends React.PureComponent {
  constructor() {
    super();

    this.useOnDownloadCsv = this.useOnDownloadCsv.bind(this);
  }

  componentDidMount() {}

  componentDidUpdate() {}

  componentWillUnmount() {}

  useOnDownloadCsv() {
    const { reportData, criteria } = this.props;

    const mappedDetails = reportData.map(dataEntry => {
      return criteria.columns.target.reduce((lastObj, columnEntry) => {
        const newObj = { ...lastObj };
        newObj[columnEntry.column] = dataEntry[columnEntry.column];
        return newObj;
      }, {});
    });

    const ws = XLSX.utils.json_to_sheet(mappedDetails);

    const fileType =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

    const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    // eslint-disable-next-line no-undef
    const data = new Blob([excelBuffer], { type: fileType });
    FileSaver.saveAs(data, `ReservedStock.xlsx`);
  }

  render() {
    const { intl, criteria, reportData, reportIsLoading } = this.props;

    const dynamicColumns = criteria.columns.target.map(entry => {
      if (
        entry.column === 'unit_qty' ||
        entry.column === 'case_qty' ||
        entry.column === 'case_uom_rate' ||
        entry.column === 'loose_qty' ||
        entry.column === 'loose_uom_rate' ||
        entry.column === 'picking_unit_qty'
      ) {
        return {
          width: 100,
          align: 'right',
          title: intl.formatMessage({ id: entry.column }),
          // sort field
          dataIndex: entry.column,
          sorter: (a, b) => a[entry.column] - b[entry.column],
          // filter field
          key: entry.column,
          render: text =>
            text > 0 ? (
              <>
                {new Intl.NumberFormat([], {
                  style: 'decimal',
                  minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
                  maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
                }).format(text)}
              </>
            ) : (
              ''
            )
        };
      }

      return {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: entry.column }),
        // sort field
        dataIndex: entry.column,
        sorter: (a, b) => `${a[entry.column]}`.localeCompare(b[entry.column]),
        // filter field
        key: entry.column,
        render: text => <>{text}</>
      };
    });

    return (
      <>
        <Table
          size="small"
          // rowSelection={rowSelection}
          rowKey="id"
          pagination={{
            pageSize: 20,
            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`
          }}
          columns={dynamicColumns}
          dataSource={reportData}
          loading={reportIsLoading}
          bordered
          title={() => (
            <>
              <Row type="flex" justify="space-between" gutter={[0, 16]}>
                <Col span={8}>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={reportIsLoading}
                    onClick={this.useOnDownloadCsv}
                    icon="cloud-download"
                  >
                    {intl.formatMessage({ id: 'excel' })}
                  </Button>
                </Col>
              </Row>
            </>
          )}
          scroll={{ x: 950 }}
        />
      </>
    );
  }
}

ReportTable.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,

  criteria: PropTypes.shape({
    columns: PropTypes.shape({
      source: PropTypes.arrayOf(PropTypes.object),
      target: PropTypes.arrayOf(PropTypes.object)
    })
  }),
  reportData: PropTypes.arrayOf(PropTypes.object),
  reportIsLoading: PropTypes.bool
};

ReportTable.defaultProps = {
  intl: {},

  criteria: {
    columns: {
      source: [],
      target: []
    }
  },
  reportData: [],
  reportIsLoading: false
};

const mapStateToProps = state => ({
  criteria: state.reservedStockReport.criteria,

  reportData: state.reservedStockReport.reportData,
  reportIsLoading: state.reservedStockReport.reportIsLoading
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(ReportTable));
