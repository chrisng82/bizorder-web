import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import treeChanges from 'tree-changes';

import { Card, Collapse } from 'antd';
import CriteriaForm from './CriteriaForm';
import ReportTable from './ReportTable';

import CountAdjAnalysisReportActions from '../../Stores/CountAdjAnalysisReport/Actions';

const { Panel } = Collapse;

class CountAdjAnalysisReportScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, initCountAdjAnalysis } = this.props;

    if (curSiteFlowId > 0) {
      initCountAdjAnalysis(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, initCountAdjAnalysis } = this.props;
    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        initCountAdjAnalysis(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'count_adjustment_analysis_report' })}>
        <Collapse defaultActiveKey={['2']}>
          <Panel header={intl.formatMessage({ id: 'criteria' })} key="1">
            <CriteriaForm match={match} />
          </Panel>
          <Panel header={intl.formatMessage({ id: 'report' })} key="2">
            <ReportTable match={match} />
          </Panel>
        </Collapse>
      </Card>
    );
  }
}

CountAdjAnalysisReportScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,

  initCountAdjAnalysis: PropTypes.func,
  curSiteFlowId: PropTypes.number
};

CountAdjAnalysisReportScreen.defaultProps = {
  intl: {},
  match: {},

  initCountAdjAnalysis: {},
  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  initCountAdjAnalysis: siteFlowId =>
    dispatch(CountAdjAnalysisReportActions.countAdjAnalysisReportInitCountAdjAnalysis(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(CountAdjAnalysisReportScreen));
