import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';
import moment from 'moment';

import { Row, Col, Table, Button, Badge } from 'antd';

import AntDesignTable from '../../Components/AntDesignTable';
import InvDoc01Actions from '../../Stores/InvDoc01/Actions';

import InvDoc01Dialog from './InvDoc01Dialog';

class InvDoc01Table extends React.PureComponent {
  constructor() {
    super();

    this.useOnTableChange = this.useOnTableChange.bind(this);
    this.useShowWorkspace = this.useShowWorkspace.bind(this);

    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDocumentColumns = this.getDocumentColumns.bind(this);
    this.getExpandedColumns = this.getExpandedColumns.bind(this);
    this.showExpandedRow = this.showExpandedRow.bind(this);
  }

  componentDidMount() {
    const { curSiteFlowId, setWorkspaceVisible, resetTimestamp } = this.props;

    if (curSiteFlowId > 0) {
      setWorkspaceVisible(false);
      resetTimestamp();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      curSiteFlowId,
      resetTimestamp,
      fetchInvDoc01,
      currentPage,
      sorts,
      filters,
      pageSize
    } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      resetTimestamp();
    }

    if (changed('timestamp')) {
      if (curSiteFlowId > 0) {
        fetchInvDoc01(curSiteFlowId, currentPage, sorts, filters, pageSize);
      }
    }
  }

  componentWillUnmount() {}

  getDocumentColumns() {
    const { intl, sorts, filters } = this.props;

    return [
      {
        align: 'left',
        title: intl.formatMessage({ id: 'doc_code' }),
        // sort field
        dataIndex: 'doc_code',
        ...AntDesignTable.getColumnSortProps(sorts, 'doc_code'),
        // filter field
        key: 'doc_code',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'doc_code' }),
          'doc_code',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.doc_code}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'doc_date' }),
        // sort field
        dataIndex: 'doc_date',
        ...AntDesignTable.getColumnSortProps(sorts, 'doc_date'),
        // filter field
        key: 'doc_date',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'doc_date' }),
          'doc_date',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{moment(record.doc_date).format('YYYY-MM-DD')}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'doc_status' }),
        // sort field
        dataIndex: 'str_doc_status',
        ...AntDesignTable.getColumnSortProps(sorts, 'doc_status'),
        // filter field
        key: 'doc_status',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'doc_status' }),
          'doc_status',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.str_doc_status}</>
      },

      {
        align: 'right',
        title: intl.formatMessage({ id: 'pick_qty' }),
        // sort field
        dataIndex: 'pick_unit_qty',
        ...AntDesignTable.getColumnSortProps(sorts, 'pick_unit_qty'),
        // filter field
        key: 'pick_unit_qty',
        render: (text, record) => (
          <>
            {new Intl.NumberFormat([], {
              style: 'decimal',
              minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
              maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
            }).format(record.pick_unit_qty)}
          </>
        )
      },
      {
        align: 'right',
        title: intl.formatMessage({ id: 'order_qty' }),
        // sort field
        dataIndex: 'ord_unit_qty',
        ...AntDesignTable.getColumnSortProps(sorts, 'ord_unit_qty'),
        // filter field
        key: 'ord_unit_qty',
        render: (text, record) => (
          <>
            {new Intl.NumberFormat([], {
              style: 'decimal',
              minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
              maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
            }).format(record.ord_unit_qty)}
          </>
        )
      }
    ];
  }

  getExpandedColumns() {
    const { intl, goToOrderDoc } = this.props;

    return [
      {
        align: 'left',
        title: intl.formatMessage({ id: 'code' }),
        // sort field
        dataIndex: 'doc_code',
        // filter field
        key: 'doc_code',
        render: (text, record) => <>{record.doc_code}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'description' }),
        // sort field
        dataIndex: 'delivery_point_company_name_01',
        // filter field
        key: 'delivery_point_company_name_01',
        render: (text, record) => <>{record.delivery_point_company_name_01}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'area' }),
        // sort field
        dataIndex: 'delivery_point_area_code',
        // filter field
        key: 'delivery_point_area_code',
        render: (text, record) => <>{record.delivery_point_area_code}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'salesman' }),
        // sort field
        dataIndex: 'salesman_username',
        // filter field
        key: 'salesman_username',
        render: (text, record) => <>{record.salesman_username}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'division' }),
        // sort field
        dataIndex: 'division_code',
        // filter field
        key: 'division_code',
        render: (text, record) => <>{record.division_code}</>
      },

      {
        align: 'right',
        title: intl.formatMessage({ id: 'pick_qty' }),
        // sort field
        dataIndex: 'pick_qty',
        // filter field
        key: 'pick_qty',
        render: (text, record) => (
          <>
            {record.pick_qty
              ? new Intl.NumberFormat([], {
                  style: 'decimal',
                  minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
                  maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
                }).format(record.pick_qty)
              : ''}
          </>
        )
      },
      {
        align: 'right',
        title: intl.formatMessage({ id: 'order_qty' }),
        // sort field
        dataIndex: 'unit_qty',
        // filter field
        key: 'unit_qty',
        render: (text, record) => (
          <>
            {new Intl.NumberFormat([], {
              style: 'decimal',
              minimumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE,
              maximumFractionDigits: process.env.REACT_APP_DECIMAL_SCALE
            }).format(record.unit_qty)}
          </>
        )
      },

      {
        key: 'action',
        render: (text, record) => (
          <>
            {record.pick_qty ? (
              ''
            ) : (
              <Button
                type="dashed"
                icon="edit"
                onClick={() => goToOrderDoc(record.hdr_id, record.item_id)}
              />
            )}
          </>
        )
      }
    ];
  }

  showExpandedRow(record) {
    const { fetchIsLoading } = this.props;
    const finalTreeDetails = record.details.reduce((treeDetails, detail) => {
      // check if treeDetails already have itemId
      let foundTreeDetail = treeDetails.reduce((acc, data) => {
        if (data.item_id === detail.item_id) {
          return data;
        }
        return acc;
      }, null);

      if (foundTreeDetail === null) {
        foundTreeDetail = {
          ...detail,
          id: `item${detail.item_id}`,
          doc_code: detail.item_code,
          delivery_point_company_name_01: detail.desc_01,
          delivery_point_area_code: '',
          salesman_username: '',
          division_code: '',
          pick_qty: detail.pick_item_ttl_unit_qty,
          unit_qty: detail.unit_qty,
          children: [detail]
        };
        treeDetails.push(foundTreeDetail);
      } else {
        // this item is already in the treeDetails, so just add to total unit_qty,
        // and add detail to children
        const index = treeDetails.indexOf(foundTreeDetail);
        foundTreeDetail = {
          ...foundTreeDetail,
          unit_qty: parseFloat(foundTreeDetail.unit_qty) + parseFloat(detail.unit_qty)
        };
        foundTreeDetail.children.push(detail);

        if (index !== -1) {
          // replace the original treeDetail with updated treeDetail
          treeDetails.splice(index, 1, foundTreeDetail);
        }
      }

      return treeDetails;
    }, []);

    return (
      <Table
        size="small"
        // rowSelection={rowSelection}
        rowKey="id"
        bordered={false}
        pagination={false}
        columns={this.getExpandedColumns()}
        dataSource={finalTreeDetails}
        loading={fetchIsLoading}
        defaultExpandAllRows
        rowClassName={rowData => {
          if (rowData.pick_qty && parseFloat(rowData.pick_qty) !== parseFloat(rowData.unit_qty)) {
            return 'error-row';
          }
          return '';
        }}
      />
    );
  }

  useOnTableChange(pagination, filters, sorter) {
    const { resetTimestamp } = this.props;

    // process the filters
    const processedFilters = {};
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value && value[0]) {
        const filterVal = value.reduce((lastObj, data) => {
          if (lastObj.length === 0) {
            return data;
          }
          return `${lastObj},${data}`;
        }, '');
        processedFilters[key] = filterVal;
      }
    });

    // process the sorts, now just support 1 column
    const processedSorts = {};
    if (Object.entries(sorter).length > 0) {
      processedSorts[sorter.field] = sorter.order;
    }

    resetTimestamp(pagination.current, processedSorts, processedFilters, pagination.pageSize);
  }

  handleSearch(selectedKeys, confirm) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    confirm();
  }

  handleReset(clearFilters) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    clearFilters();
  }

  useShowWorkspace() {
    const { setWorkspaceVisible } = this.props;

    setWorkspaceVisible(true);
  }

  render() {
    const {
      resetTimestamp,
      currentPage,
      pageSize,
      total,
      documents,
      fetchIsLoading,
      selectedDocuments,
      addSelectedDocuments,
      removeSelectedDocuments,
      expandedRows,
      setExpandedRows
    } = this.props;

    return (
      <>
        <Table
          size="small"
          // rowSelection={rowSelection}
          rowKey="id"
          pagination={{
            current: currentPage,
            pageSize: parseInt(pageSize, 10),
            total,
            showTotal: () => `${total} items`
          }}
          columns={this.getDocumentColumns()}
          dataSource={documents}
          loading={fetchIsLoading}
          bordered
          rowSelection={{
            selectedRowKeys: selectedDocuments.map(value => value.id),
            onChange: () => {
              // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            onSelect: (record, selected) => {
              // console.log(`onSelect record: `, record, 'selected: ', selected, 'selectedRows: ', selectedRows);
              if (selected === true) {
                addSelectedDocuments([record]);
              } else {
                removeSelectedDocuments([record]);
              }
            },
            onSelectAll: (selected, selectedRows, changeRows) => {
              // console.log('onSelectAll selected: ', selected, 'selectedRows: ', selectedRows, 'changeRows: ', changeRows);
              if (selected === true) {
                addSelectedDocuments(changeRows);
              } else {
                removeSelectedDocuments(changeRows);
              }
            },
            onSelectInvert: selectedRows => {
              // console.log('onSelectInvert selectedRows: ', selectedRows);
              removeSelectedDocuments(selectedRows);
            },
            getCheckboxProps: record => ({
              // Column configuration not to be checked
              disabled: record.str_doc_status !== 'COMPLETE'
              // name: record.name,
            })
          }}
          onChange={this.useOnTableChange}
          rowClassName={rowData => {
            if (parseFloat(rowData.pick_unit_qty) !== parseFloat(rowData.ord_unit_qty)) {
              return 'error-row';
            }
            return '';
          }}
          expandedRowRender={this.showExpandedRow}
          expandedRowKeys={expandedRows}
          onExpandedRowsChange={rowKeys => {
            setExpandedRows(rowKeys);
          }}
          title={() => (
            <>
              <Row type="flex" justify="space-between" gutter={[0, 16]}>
                <Col span={8}>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={fetchIsLoading}
                    onClick={() => resetTimestamp()}
                    icon="reload"
                  />
                </Col>
              </Row>
            </>
          )}
        />

        <div
          style={{
            position: 'fixed',
            bottom: '50px',
            right: '50px'
          }}
        >
          <Badge count={selectedDocuments.length}>
            <Button
              size="large"
              type="primary"
              shape="circle"
              icon="laptop"
              onClick={this.useShowWorkspace}
              loading={fetchIsLoading}
            />
          </Badge>
        </div>

        <InvDoc01Dialog />
      </>
    );
  }
}

InvDoc01Table.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  resetTimestamp: PropTypes.func,

  setWorkspaceVisible: PropTypes.func,

  addSelectedDocuments: PropTypes.func,
  removeSelectedDocuments: PropTypes.func,
  documents: PropTypes.arrayOf(PropTypes.object),
  selectedDocuments: PropTypes.arrayOf(PropTypes.object),

  sorts: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  filters: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),

  currentPage: PropTypes.number,
  pageSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  total: PropTypes.number,
  fetchIsLoading: PropTypes.bool,

  curSiteFlowId: PropTypes.number,
  fetchInvDoc01: PropTypes.func,
  goToOrderDoc: PropTypes.func,

  expandedRows: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  setExpandedRows: PropTypes.func
};

InvDoc01Table.defaultProps = {
  intl: {},
  resetTimestamp() {},

  setWorkspaceVisible() {},

  addSelectedDocuments() {},
  removeSelectedDocuments() {},
  documents: [],
  selectedDocuments: [],

  sorts: {},
  filters: {},

  currentPage: 1,
  pageSize: '20',
  total: 100,
  fetchIsLoading: false,

  curSiteFlowId: 0,
  fetchInvDoc01() {},
  goToOrderDoc() {},

  expandedRows: [],
  setExpandedRows() {}
};

const mapStateToProps = state => ({
  timestamp: state.invDoc01.timestamp,
  workspaceIsVisible: state.invDoc01.workspaceIsVisible,

  documents: state.invDoc01.documents,
  selectedDocuments: state.invDoc01.selectedDocuments,

  sorts: state.invDoc01.sorts,
  filters: state.invDoc01.filters,

  currentPage: state.invDoc01.currentPage,
  pageSize: state.invDoc01.pageSize,
  total: state.invDoc01.total,
  fetchIsLoading: state.invDoc01.fetchIsLoading,

  curSiteFlowId: state.app.curSiteFlowId,
  expandedRows: state.invDoc01.expandedRows
});

const mapDispatchToProps = dispatch => ({
  resetTimestamp: (currentPage, sorts, filters, pageSize) =>
    dispatch(InvDoc01Actions.invDoc01ResetTimestamp(currentPage, sorts, filters, pageSize)),
  setWorkspaceVisible: boolean => dispatch(InvDoc01Actions.invDoc01SetWorkspaceVisible(boolean)),

  addSelectedDocuments: selectedDocuments =>
    dispatch(InvDoc01Actions.invDoc01AddSelectedDocuments(selectedDocuments)),
  removeSelectedDocuments: selectedDocuments =>
    dispatch(InvDoc01Actions.invDoc01RemoveSelectedDocuments(selectedDocuments)),

  fetchInvDoc01: (divisionId, currentPage, sorts, filters, pageSize) =>
    dispatch(
      InvDoc01Actions.invDoc01FetchInvDoc01(divisionId, currentPage, sorts, filters, pageSize)
    ),

  setExpandedRows: expandedRows => dispatch(InvDoc01Actions.invDoc01SetExpandedRows(expandedRows)),

  goToOrderDoc: (hdrId, itemId) => dispatch(InvDoc01Actions.invDoc01GoToOrderDoc(hdrId, itemId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(InvDoc01Table));
