import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import treeChanges from 'tree-changes';

import { Card, Collapse } from 'antd';
import CriteriaForm from './CriteriaForm';
import ReportTable from './ReportTable';

import OutbOrdAnalysisReportActions from '../../Stores/OutbOrdAnalysisReport/Actions';

const { Panel } = Collapse;

class OutbOrdAnalysisReportScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, initOutbOrdAnalysis } = this.props;

    if (curSiteFlowId > 0) {
      initOutbOrdAnalysis(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, initOutbOrdAnalysis } = this.props;
    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        initOutbOrdAnalysis(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'outbound_order_analysis' })}>
        <Collapse defaultActiveKey={['2']}>
          <Panel header={intl.formatMessage({ id: 'criteria' })} key="1">
            <CriteriaForm match={match} />
          </Panel>
          <Panel header={intl.formatMessage({ id: 'report' })} key="2">
            <ReportTable match={match} />
          </Panel>
        </Collapse>
      </Card>
    );
  }
}

OutbOrdAnalysisReportScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,

  initOutbOrdAnalysis: PropTypes.func,
  curSiteFlowId: PropTypes.number
};

OutbOrdAnalysisReportScreen.defaultProps = {
  intl: {},
  match: {},

  initOutbOrdAnalysis: {},
  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  initOutbOrdAnalysis: siteFlowId =>
    dispatch(OutbOrdAnalysisReportActions.outbOrdAnalysisReportInitOutbOrdAnalysis(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(OutbOrdAnalysisReportScreen));
