import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import { Card, Modal, Breadcrumb } from 'antd';
import HeaderForm from './HeaderForm';
import DetailTable from './DetailTable';
import DetailForm from './DetailForm';
import AdvShipDetailActions from '../../Stores/AdvShipDetail/Actions';

class AdvShipDetailScreen extends React.PureComponent {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
  }

  componentDidMount() {
    const { match, setHdrId } = this.props;

    if (match.params.action === 'create') {
      setHdrId(0);
    } else if (match.params.action === 'update') {
      setHdrId(parseInt(match.params.id, 10));
    }
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  handleCancel() {
    const { setDetailVisible, documentIsLoading } = this.props;

    if (documentIsLoading === false) {
      setDetailVisible(false);
    }
  }

  render() {
    const { intl, match, detailIsVisible, documentHeader, appPath } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'adv_ship' })}>
        <Breadcrumb>
          {documentHeader.doc_flows.map(docData => (
            <Breadcrumb.Item key={docData.doc_id}>
              {docData.is_current === true ? (
                docData.doc_code
              ) : (
                <Link replace to={`${appPath}/${docData.action}/update/${docData.doc_id}`}>
                  <span style={{ color: 'blue' }}>{docData.doc_code}</span>
                </Link>
              )}
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
        <HeaderForm match={match} />
        <DetailTable />
        <Modal
          visible={detailIsVisible}
          title={intl.formatMessage({ id: 'adv_ship_detail' })}
          // style={{top:20}}
          width="90%"
          centered
          forceRender
          // onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
          <DetailForm match={match} />
        </Modal>
      </Card>
    );
  }
}

AdvShipDetailScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  setHdrId: PropTypes.func,
  setDetailVisible: PropTypes.func,
  documentIsLoading: PropTypes.bool,
  detailIsVisible: PropTypes.bool,
  documentHeader: PropTypes.shape({
    doc_flows: PropTypes.arrayOf(PropTypes.object)
  }),
  appPath: PropTypes.string
};

AdvShipDetailScreen.defaultProps = {
  intl: {},
  match: {},
  setHdrId() {},
  setDetailVisible() {},
  documentIsLoading: false,
  detailIsVisible: false,
  documentHeader: { doc_flows: [] },
  appPath: ''
};

const mapStateToProps = state => ({
  documentIsLoading: state.advShipDetail.documentIsLoading,
  detailIsVisible: state.advShipDetail.detailIsVisible,
  documentHeader: state.advShipDetail.documentHeader,
  appPath: state.app.appPath
});

const mapDispatchToProps = dispatch => ({
  setHdrId: hdrId => dispatch(AdvShipDetailActions.advShipDetailSetHdrId(hdrId)),
  setDetailVisible: boolean => dispatch(AdvShipDetailActions.advShipDetailSetDetailVisible(boolean))
});

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(AdvShipDetailScreen));
