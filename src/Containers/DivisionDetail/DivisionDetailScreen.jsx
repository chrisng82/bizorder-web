import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import { Card, Modal, Breadcrumb } from 'antd';
import HeaderForm from './HeaderForm';
import SyncSettingForm from './SyncSettingForm';
// import DetailTable from './DetailTable';
// import DetailForm from './DetailForm';
import DivisionDetailActions from '../../Stores/DivisionDetail/Actions';

class DivisionDetailScreen extends React.PureComponent {
  constructor() {
    super();

    this.handleCancel = this.handleCancel.bind(this);
  }

  componentDidMount() {
    const { match, setHdrId } = this.props;

    if (match.params.action === 'create') {
      setHdrId(0);
    } else if (match.params.action === 'update') {
      const hdrId = parseInt(match.params.id, 10);
      setHdrId(hdrId);
    }
  }

  componentDidUpdate() {}

  componentWillUnmount() {}

  handleCancel() {
    const { setDetailVisible, documentIsLoading } = this.props;

    if (documentIsLoading === false) {
      setDetailVisible(false);
    }
  }

  render() {
    const { intl, match, detailIsVisible, documentHeader, appPath } = this.props;

    return (
      <Card title={intl.formatMessage({ id: 'division' })}>
        <Breadcrumb>
        </Breadcrumb>
        <HeaderForm match={match} />
        
        {match.params.action === 'update' &&
        <SyncSettingForm match={match} />
        }
      </Card>
    );
  }
}

DivisionDetailScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  setHdrId: PropTypes.func,
  setDetailVisible: PropTypes.func,
  documentIsLoading: PropTypes.bool,
  detailIsVisible: PropTypes.bool,
  documentHeader: PropTypes.shape({
    doc_flows: PropTypes.arrayOf(PropTypes.object)
  }),
  appPath: PropTypes.string
};

DivisionDetailScreen.defaultProps = {
  intl: {},
  match: {},
  setHdrId() {},
  setDetailVisible() {},
  documentIsLoading: false,
  detailIsVisible: false,
  documentHeader: { doc_flows: [] },
  appPath: ''
};

const mapStateToProps = state => ({
  documentIsLoading: state.divisionDetail.documentIsLoading,
  detailIsVisible: state.divisionDetail.detailIsVisible,
  documentHeader: state.divisionDetail.documentHeader,
  appPath: state.app.appPath
});

const mapDispatchToProps = dispatch => ({
  setHdrId: (hdrId) => dispatch(DivisionDetailActions.divisionDetailSetHdrId(hdrId)),
  // setDetailVisible: boolean => dispatch(DivisionDetailActions.divisionDetailSetDetailVisible(boolean))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(DivisionDetailScreen));
