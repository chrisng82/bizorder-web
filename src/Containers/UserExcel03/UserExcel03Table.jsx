import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';

import { Card, Row, Col, Upload, Button, Progress, notification } from 'antd';

import UserExcel03Actions from '../../Stores/UserExcel03/Actions';

class UserExcel03Screen extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      fileList: []
    };

    this.useOnUpload = this.useOnUpload.bind(this);
    this.useOnDownload = this.useOnDownload.bind(this);
  }

  componentDidMount() {}

  componentDidUpdate() {
    const { uploadIsLoading, showBatchJobStatus } = this.props;

    if (uploadIsLoading) {
      showBatchJobStatus();
    }
  }

  componentWillUnmount() {}

  useOnUpload() {
    const { curSiteFlowId, curDivisionId, uploadExcel } = this.props;
    const { fileList } = this.state;

    if (fileList.length > 0) {
      uploadExcel(curSiteFlowId, curDivisionId, fileList[0]);
    }
  }

  useOnDownload() {
    const { curSiteFlowId, curDivisionId, downloadExcel } = this.props;

    downloadExcel(curSiteFlowId, curDivisionId);
  }

  render() {
    const { intl, uploadIsLoading, batchJobStatus } = this.props;
    const { fileList } = this.state;

    return (
      <Card>
        <Row type="flex" justify="start" gutter={[0, 8]}>
          <Col span={12}>
            <Upload
              disabled={uploadIsLoading}
              onRemove={() => {
                this.setState({
                  fileList: []
                });
              }}
              beforeUpload={file => {
                const validFileName = 'USER_EXCEL_03';
                if (file && file.name.includes(validFileName)) {
                  this.setState({
                    fileList: [file]
                  });
                } else {
                  notification.error({
                    message: `${intl.formatMessage({
                      id: 'invalid_file_name_must_contain'
                    })} ${validFileName}`,
                    duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
                  });
                }

                return false;
              }}
              fileList={fileList}
            >
              <Button type="primary" loading={uploadIsLoading}>
                {intl.formatMessage({ id: 'browse' })}
              </Button>
            </Upload>
          </Col>
          <Col span={12}>
            <Button
              type="primary"
              onClick={this.useOnUpload}
              disabled={fileList.length === 0}
              loading={uploadIsLoading}
            >
              {intl.formatMessage({ id: 'upload' })}
            </Button>
            <Button type="primary" onClick={this.useOnDownload} loading={uploadIsLoading}>
              {intl.formatMessage({ id: 'download' })}
            </Button>
          </Col>
        </Row>
        <Row type="flex" justify="center" gutter={[0, 8]}>
          <Col span={24}>
            <Progress percent={parseInt(batchJobStatus.status_number, 10)} />
          </Col>
        </Row>
      </Card>
    );
  }
}

UserExcel03Screen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,

  batchJobStatus: PropTypes.shape({
    status_number: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }),
  uploadIsLoading: PropTypes.bool,

  showBatchJobStatus: PropTypes.func,
  uploadExcel: PropTypes.func,
  downloadExcel: PropTypes.func,

  curSiteFlowId: PropTypes.number,
  curDivisionId: PropTypes.number,
};

UserExcel03Screen.defaultProps = {
  intl: {},

  batchJobStatus: {
    status_number: '0'
  },
  uploadIsLoading: false,

  showBatchJobStatus() {},
  uploadExcel() {},
  downloadExcel() {},

  curSiteFlowId: 0,
  curDivisionId: 0
};

const mapStateToProps = state => ({
  batchJobStatus: state.userExcel03.batchJobStatus,
  uploadIsLoading: state.userExcel03.uploadIsLoading,

  curSiteFlowId: state.app.curSiteFlowId,
  curDivisionId: state.app.curDivisionId
});

const mapDispatchToProps = dispatch => ({
  showBatchJobStatus: () => dispatch(UserExcel03Actions.userExcel03ShowBatchJobStatus()),
  uploadExcel: (siteFlowId, divisionId, file) =>
    dispatch(UserExcel03Actions.userExcel03UploadExcel(siteFlowId, divisionId, file)),
  downloadExcel: (siteFlowId, divisionId) => dispatch(UserExcel03Actions.userExcel03DownloadExcel(siteFlowId, divisionId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(UserExcel03Screen));
