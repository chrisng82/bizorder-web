import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { Card, Row, Col, Progress, Typography, Divider } from 'antd';
import { FormikPassword, FormikInput, FormikText, FormikButton, FormikSelect } from '../../Components/Formik';

import ItemSync01Actions from '../../Stores/ItemSync01/Actions';

const { Text } = Typography;

const ItemGroupOptions = [
  { value: '', label: 'N/A' },
  { value: 0, label: 'Item Group' },
  { value: 1, label: 'Item Group 01' },
  { value: 2, label: 'Item Group 02' },
];

class ItemSync01Form extends React.PureComponent {
  componentDidMount() {
    const { curDivisionId, showSyncSetting } = this.props;

    if (curDivisionId > 0) {
      showSyncSetting(curDivisionId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curDivisionId, syncIsLoading, showSyncSetting, showBatchJobStatus } = this.props;
    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curDivisionId')) {
      if (curDivisionId > 0) {
        showSyncSetting(curDivisionId);
      }
    }

    if (syncIsLoading) {
      showBatchJobStatus();
    }
  }

  componentWillUnmount() {}

  render() {
    const {
      intl,
      curDivisionId,
      fetchIsLoading,
      syncIsLoading,
      syncSetting,
      batchJobStatus,
      updateSyncSetting,
      syncNow,
      syncImage,
      resetImage
    } = this.props;

    return (
      <Formik
        enableReinitialize
        initialValues={syncSetting}
        onSubmit={(values, formikBag) => {
          if (values.submit_action === 'update') {
            updateSyncSetting(formikBag, values);
          } else if (values.submit_action === 'sync_now') {
            syncNow(formikBag, curDivisionId);
          } else if (values.submit_action === 'sync_image') {
            syncImage(formikBag, curDivisionId);
          } else if (values.submit_action === 'reset_image') {
            resetImage(formikBag, curDivisionId);
          }
        }}
        validationSchema={Yup.object().shape({
          currency_rate: Yup.number().moreThan(
            0,
            intl.formatMessage({ id: 'currency_rate_must_be_greater_than_0' })
          )
        })}
      >
        {({
          // values,
          // handleChange,
          // errors,
          // setFieldTouched,
          // dirty,
          // touched,
          isSubmitting,
          // isValid,
          handleSubmit
          // handleReset
        }) => (
          <Form>
            <Card>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'url' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="url" component={FormikInput} loading={fetchIsLoading} />
                </Col>
              </Row>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'page_size' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="page_size" component={FormikInput} loading={fetchIsLoading} />
                </Col>
              </Row>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'username' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="username" component={FormikInput} loading={fetchIsLoading} />
                </Col>
              </Row>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'password' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="password" component={FormikPassword} loading={fetchIsLoading} />
                </Col>
              </Row>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'last_synced_at' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="last_synced_at" component={FormikText} loading={fetchIsLoading} />
                </Col>
              </Row>
              
              <Divider>{intl.formatMessage({ id: 'field_mapping' })}</Divider>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={5}>
                  <Text>{intl.formatMessage({ id: 'item_brand_mapping' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={8}>
                  <Field
                    name="item_brand_mapping_select2"
                    component={FormikSelect}
                    showArrow
                    filterOption={false}
                    notFoundContent={null}
                    loading={fetchIsLoading}
                    options={ItemGroupOptions}
                  />
                </Col>
                <Col span={10}>
                </Col>
              </Row>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={5}>
                  <Text>{intl.formatMessage({ id: 'item_category_mapping' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={8}>
                  <Field
                    name="item_category_mapping_select2"
                    component={FormikSelect}
                    showArrow
                    filterOption={false}
                    notFoundContent={null}
                    loading={fetchIsLoading}
                    options={ItemGroupOptions}
                  />
                </Col>
                <Col span={10}>
                </Col>
              </Row>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={5}>
                  <Text>{intl.formatMessage({ id: 'item_manufacturer_mapping' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={8}>
                  <Field
                    name="item_manufacturer_mapping_select2"
                    component={FormikSelect}
                    showArrow
                    filterOption={false}
                    notFoundContent={null}
                    loading={fetchIsLoading}
                    options={ItemGroupOptions}
                  />
                </Col>
                <Col span={10}>
                </Col>
              </Row>
              <Divider></Divider>
              <Row type="flex" justify="start" gutter={[0, 16]}>
                {/*
                <Col span={3}>
                  <Field
                    type="primary"
                    name="submit_action"
                    value="update"
                    component={FormikButton}
                    // disabled={values.doc_status >= 50}
                    loading={isSubmitting || fetchIsLoading || syncIsLoading}
                    onClick={handleSubmit}
                    label={intl.formatMessage({ id: 'update' })}
                    icon="save"
                  />
                </Col>
                */}
                <Col span={3}>
                  <Field
                    type="primary"
                    name="submit_action"
                    value="sync_now"
                    component={FormikButton}
                    // disabled={values.doc_status >= 50}
                    loading={isSubmitting || fetchIsLoading || syncIsLoading}
                    onClick={handleSubmit}
                    label={intl.formatMessage({ id: 'sync_now' })}
                    icon="sync"
                  />
                </Col>
                <Col span={3}>
                  <Field
                    type="primary"
                    name="submit_action"
                    value="sync_image"
                    component={FormikButton}
                    // disabled={values.doc_status >= 50}
                    loading={isSubmitting || fetchIsLoading || syncIsLoading}
                    onClick={handleSubmit}
                    label={intl.formatMessage({ id: 'sync_image' })}
                    icon="sync"
                  />
                </Col>
                <Col span={3}>
                  <Field
                    type="primary"
                    name="submit_action"
                    value="reset_image"
                    component={FormikButton}
                    // disabled={values.doc_status >= 50}
                    loading={isSubmitting || fetchIsLoading || syncIsLoading}
                    onClick={handleSubmit}
                    label={intl.formatMessage({ id: 'reset_image' })}
                    icon="sync"
                  />
                </Col>
              </Row>

              <Row type="flex" justify="center" gutter={[0, 8]}>
                <Col span={24}>
                  <Progress percent={parseInt(batchJobStatus.status_number, 10)} />
                </Col>
              </Row>
            </Card>
          </Form>
        )}
      </Formik>
    );
  }
}

ItemSync01Form.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,

  syncSetting: PropTypes.shape({}),
  batchJobStatus: PropTypes.shape({
    status_number: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }),
  fetchIsLoading: PropTypes.bool,
  syncIsLoading: PropTypes.bool,

  showSyncSetting: PropTypes.func,
  updateSyncSetting: PropTypes.func,
  showBatchJobStatus: PropTypes.func,
  syncNow: PropTypes.func,
  syncImage: PropTypes.func,
  resetImage: PropTypes.func,

  curDivisionId: PropTypes.number
};

ItemSync01Form.defaultProps = {
  intl: {},

  syncSetting: {},
  batchJobStatus: {
    status_number: '0'
  },
  fetchIsLoading: false,
  syncIsLoading: false,

  showSyncSetting() {},
  updateSyncSetting() {},
  showBatchJobStatus() {},
  syncNow() {},
  syncImage() {},
  resetImage() {},

  curDivisionId: 0
};

const mapStateToProps = state => ({
  syncSetting: state.itemSync01.syncSetting,
  batchJobStatus: state.itemSync01.batchJobStatus,
  fetchIsLoading: state.itemSync01.fetchIsLoading,
  syncIsLoading: state.itemSync01.syncIsLoading,

  curDivisionId: state.app.curDivisionId
});

const mapDispatchToProps = dispatch => ({
  showSyncSetting: divisionId =>
    dispatch(ItemSync01Actions.itemSync01ShowSyncSetting(divisionId)),
  updateSyncSetting: (formikBag, syncSetting) =>
    dispatch(ItemSync01Actions.itemSync01UpdateSyncSetting(formikBag, syncSetting)),
  showBatchJobStatus: () => dispatch(ItemSync01Actions.itemSync01ShowBatchJobStatus()),
  syncNow: (formikBag, divisionId) =>
    dispatch(ItemSync01Actions.itemSync01SyncNow(formikBag, divisionId)),
  syncImage: (formikBag, divisionId) =>
    dispatch(ItemSync01Actions.itemSync01SyncImage(formikBag, divisionId)),
  resetImage: (formikBag, divisionId) =>
    dispatch(ItemSync01Actions.itemSync01ResetImage(formikBag, divisionId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(ItemSync01Form));
