import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';

import { Card, Button, Row, Col, Typography, Popconfirm, Radio } from 'antd';
import treeChanges from 'tree-changes';
import { FormikInput, FormikDatePicker, FormikButton, FormikRadio, FormikInputNumber } from '../../Components/Formik';
import PromoDetailActions from '../../Stores/PromoDetail/Actions';
import AppActions from '../../Stores/App/Actions';

const { Text } = Typography;

class BroadcastForm extends React.PureComponent {
  componentDidMount() {}

  componentDidUpdate(prevProps) {
    const { hdrId, showHeader } = this.props;

    const { changed } = treeChanges(prevProps, this.props);
    if (changed('timestamp')) {
      showHeader(hdrId);
    }
  }

  componentWillUnmount() {}

  render() {
    const {
      intl,
      documentHeader,
      createHeader,
      historyGoBack,
      documentIsLoading,
    } = this.props;

    const disabled = false;

    // const priorityOptions = [
    //   <Radio.Button key={100} value={100}>
    //     {intl.formatMessage({ id: 'active' })}
    //   </Radio.Button>,
    //   <Radio.Button key={2} value={2}>
    //     {intl.formatMessage({ id: 'inactive' })}
    //   </Radio.Button>
    // ];
    
    return (
      <Formik
        enableReinitialize
        initialValues={documentHeader}
        onSubmit={(values, formikBag) => {
          if (values.submit_action === 'update') {
            const processedValues = {
              id: values.id,
              title: values.title,
              content: values.content,
              priority: values.priority,
            };

            // createHeader(curDivisionId, formikBag, match.params.id, processedValues);
          }
        }}
        validationSchema={ 
          Yup.object().shape({
            title: Yup.string().required(intl.formatMessage({ id: 'title_is_required' })),
            content: Yup.string().required(intl.formatMessage({ id: 'content_is_required' })),
            priority: Yup.number().min(1, intl.formatMessage({ id: 'priority_is_required' })),
          })
        }
      >
        {({
          values,
          dirty,
          isSubmitting,
          handleSubmit,
          handleReset
        }) => (
          <Form>
            <Card type="inner" title={intl.formatMessage({ id: 'message_content' })}>
              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'title' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="title" component={FormikInput} disabled={disabled} />
                </Col>
              </Row>

              <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'content' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field name="content" component={FormikInput} disabled={disabled} />
                </Col>
              </Row>

              {/* <Row type="flex" justify="center" gutter={[8, 8]}>
                <Col span={3}>
                  <Text>{intl.formatMessage({ id: 'priority' })}</Text>
                </Col>
                <Col span={1}>
                  <Text>:</Text>
                </Col>
                <Col span={20}>
                  <Field
                    name="priority"
                    component={FormikRadio}
                    options={priorityOptions}
                    disabled={disabled}
                  />
                </Col>
              </Row> */}
            </Card>

            <Card>
              <Row type="flex" justify="space-between" gutter={[16, 16]}>
                <Col>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={isSubmitting}
                    onClick={historyGoBack}
                    icon="arrow-left"
                  >
                    {intl.formatMessage({
                      id: 'back'
                    })}
                  </Button>
                </Col>
                <Col>
                  <Button
                    type="primary"
                    disabled={!dirty}
                    loading={isSubmitting || documentIsLoading}
                    onClick={handleReset}
                    icon="undo"
                  >
                    {intl.formatMessage({ id: 'reset' })}
                  </Button>
                  <Field
                    type="primary"
                    name="submit_action"
                    value="create"
                    component={FormikButton}
                    loading={isSubmitting || documentIsLoading}
                    onClick={handleSubmit}
                    label={intl.formatMessage({ id: 'broadcast' })}
                    icon="notification"
                  />
                </Col>
              </Row>
            </Card>
          
          </Form>
        )}
      </Formik>
    );
  }
}

BroadcastForm.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  initHeader: PropTypes.func,
  showHeader: PropTypes.func,
  transitionToStatus: PropTypes.func,
  createHeader: PropTypes.func,
  updateHeader: PropTypes.func,
  historyGoBack: PropTypes.func,
  hdrId: PropTypes.number,
  documentHeader: PropTypes.shape({
    doc_status: PropTypes.number
  }),
  documentIsLoading: PropTypes.bool,
  curDivisionId: PropTypes.number,
};

BroadcastForm.defaultProps = {
  intl: {},
  match: {},
  initHeader() {},
  showHeader() {},
  transitionToStatus() {},
  createHeader() {},
  updateHeader() {},
  historyGoBack() {},
  hdrId: 0,
  documentHeader: { doc_status: 3 },
  documentIsLoading: false,
  curDivisionId: 0,
};

const mapStateToProps = state => ({
  apiUrl: state.app.apiUrl,
  timestamp: state.promoDetail.timestamp,
  hdrId: state.promoDetail.hdrId,
  documentHeader: state.promoDetail.documentHeader,
  documentIsLoading: state.promoDetail.documentIsLoading,
  curDivisionId: state.app.curDivisionId,
});

const mapDispatchToProps = dispatch => ({
  historyGoBack: () => dispatch(AppActions.appHistoryGoBack()),
  resetTimestamp: () => dispatch(PromoDetailActions.promoDetailResetTimestamp()),
  initHeader: (divisionId, promoType) => dispatch(PromoDetailActions.promoDetailInitHeader(divisionId, promoType)),
  showHeader: hdrId => dispatch(PromoDetailActions.promoDetailShowHeader(hdrId)),
  createHeader: (divisionId, formikBag, promoType, documentHeader) =>
    dispatch(PromoDetailActions.promoDetailCreateHeader(divisionId, formikBag, promoType, documentHeader)),
  updateHeader: (formikBag, documentHeader) =>
    dispatch(PromoDetailActions.promoDetailUpdateHeader(formikBag, documentHeader)),
  deleteHeader: (hdrId) => dispatch(PromoDetailActions.promoDetailDeleteHeader(hdrId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(BroadcastForm));
