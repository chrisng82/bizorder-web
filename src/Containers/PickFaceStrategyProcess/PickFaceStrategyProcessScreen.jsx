import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Card, Tabs, Tooltip } from 'antd';

import PickFaceStrategyProcessActions from '../../Stores/PickFaceStrategyProcess/Actions';

import PickFaceStrategyList01Table from '../PickFaceStrategyList01/PickFaceStrategyList01Table';
import PickFaceStrategyExcel01Table from '../PickFaceStrategyExcel01/PickFaceStrategyExcel01Table';

const { TabPane } = Tabs;

class PickFaceStrategyProcessScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, fetchPickFaceStrategyProcess } = this.props;
    if (curSiteFlowId > 0) {
      fetchPickFaceStrategyProcess(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, fetchPickFaceStrategyProcess } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        fetchPickFaceStrategyProcess(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match, process, appPath } = this.props;

    return (
      <Card>
        <Tabs type="card" activeKey={match.params.proc_type}>
          {process.map(value => (
            <TabPane
              tab={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <Tooltip
                  placement="bottom"
                  title={intl.formatMessage({ id: `${value.proc_type.toLowerCase()}_desc` })}
                >
                  <Link
                    style={value.proc_type !== match.params.proc_type ? { color: 'black' } : {}}
                    to={`${appPath}/pickFaceStrategyProcess/index/${value.proc_type}`}
                  >
                    <FormattedMessage id={`${value.proc_type.toLowerCase()}_label`} />
                  </Link>
                </Tooltip>
              }
              key={value.proc_type}
            />
          ))}
        </Tabs>

        {match.params.proc_type === 'PICK_FACE_STRATEGY_LIST_01' && (
          <PickFaceStrategyList01Table match={match} />
        )}
        {match.params.proc_type === 'PICK_FACE_STRATEGY_EXCEL_01' && (
          <PickFaceStrategyExcel01Table match={match} />
        )}
      </Card>
    );
  }
}

PickFaceStrategyProcessScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  appPath: PropTypes.string,
  process: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number,
  fetchPickFaceStrategyProcess: PropTypes.func
};

PickFaceStrategyProcessScreen.defaultProps = {
  intl: {},
  match: {},
  appPath: '',
  process: [],

  curSiteFlowId: 0,
  fetchPickFaceStrategyProcess() {}
};

const mapStateToProps = state => ({
  appPath: state.app.appPath,
  process: state.pickFaceStrategyProcess.process,
  processIsLoading: state.pickFaceStrategyProcess.processIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  fetchPickFaceStrategyProcess: siteFlowId =>
    dispatch(
      PickFaceStrategyProcessActions.pickFaceStrategyProcessFetchPickFaceStrategyProcess(siteFlowId)
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(PickFaceStrategyProcessScreen));
