import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Card, Tabs, Tooltip } from 'antd';

import PromoProcessActions from '../../Stores/PromoProcess/Actions';

import PromoSync01Form from '../PromoSync01/PromoSync01Form';

const { TabPane } = Tabs;

class PromoProcessScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, fetchPromoProcess } = this.props;
    if (curSiteFlowId > 0) {
      fetchPromoProcess(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, fetchPromoProcess } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        fetchPromoProcess(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match, process, appPath } = this.props;

    return (
      <Card>
        <Tabs type="card" activeKey={match.params.proc_type}>
          {process.map(value => (
            <TabPane
              tab={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <Tooltip
                  placement="bottom"
                  title={intl.formatMessage({ id: `${value.proc_type.toLowerCase()}_desc` })}
                >
                  <Link
                    style={value.proc_type !== match.params.proc_type ? { color: 'black' } : {}}
                    to={`${appPath}/promoProcess/index/${value.proc_type}`}
                  >
                    <FormattedMessage id={`${value.proc_type.toLowerCase()}_label`} />
                  </Link>
                </Tooltip>
              }
              key={value.proc_type}
            />
          ))}
        </Tabs>

        {/* {match.params.proc_type === 'PROMO_LIST_01' && <PromoList01Table match={match} />} */}
        {/* {match.params.proc_type === 'PROMO_EXCEL_01' && <PromoExcel01Table match={match} />} */}
        {match.params.proc_type === 'PROMO_SYNC_01' && <PromoSync01Form match={match} />}
      </Card>
    );
  }
}

PromoProcessScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  appPath: PropTypes.string,
  process: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number,
  fetchPromoProcess: PropTypes.func
};

PromoProcessScreen.defaultProps = {
  intl: {},
  match: {},
  appPath: '',
  process: [],

  curSiteFlowId: 0,
  fetchPromoProcess() {}
};

const mapStateToProps = state => ({
  appPath: state.app.appPath,
  process: state.promoProcess.process,
  processIsLoading: state.promoProcess.processIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  fetchPromoProcess: siteFlowId =>
    dispatch(PromoProcessActions.promoProcessFetchPromoProcess(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(PromoProcessScreen));
