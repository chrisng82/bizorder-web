import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Card, Tabs, Tooltip } from 'antd';

import PalletLabelProcessActions from '../../Stores/PalletLabelProcess/Actions';

import PalletLabelList01Table from '../PalletLabelList01/PalletLabelList01Table';

const { TabPane } = Tabs;

class PalletLabelProcessScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, fetchPalletLabelProcess } = this.props;
    if (curSiteFlowId > 0) {
      fetchPalletLabelProcess(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, fetchPalletLabelProcess } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        fetchPalletLabelProcess(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match, process, appPath } = this.props;

    return (
      <Card>
        <Tabs type="card" activeKey={match.params.proc_type}>
          {process.map(value => (
            <TabPane
              tab={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <Tooltip
                  placement="bottom"
                  title={intl.formatMessage({ id: `${value.proc_type.toLowerCase()}_desc` })}
                >
                  <Link
                    style={value.proc_type !== match.params.proc_type ? { color: 'black' } : {}}
                    to={`${appPath}/palletLabelProcess/index/${value.proc_type}`}
                  >
                    <FormattedMessage id={`${value.proc_type.toLowerCase()}_label`} />
                  </Link>
                </Tooltip>
              }
              key={value.proc_type}
            />
          ))}
        </Tabs>

        {match.params.proc_type === 'PALLET_LABEL_LIST_01' && (
          <PalletLabelList01Table match={match} />
        )}
      </Card>
    );
  }
}

PalletLabelProcessScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  appPath: PropTypes.string,
  process: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number,
  fetchPalletLabelProcess: PropTypes.func
};

PalletLabelProcessScreen.defaultProps = {
  intl: {},
  match: {},
  appPath: '',
  process: [],

  curSiteFlowId: 0,
  fetchPalletLabelProcess() {}
};

const mapStateToProps = state => ({
  appPath: state.app.appPath,
  process: state.palletLabelProcess.process,
  processIsLoading: state.palletLabelProcess.processIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  fetchPalletLabelProcess: siteFlowId =>
    dispatch(PalletLabelProcessActions.palletLabelProcessFetchPalletLabelProcess(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(PalletLabelProcessScreen));
