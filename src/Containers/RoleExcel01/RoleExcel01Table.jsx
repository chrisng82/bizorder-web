import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';

import { Card, Row, Col, Upload, Button, Progress, notification } from 'antd';

import RoleExcel01Actions from '../../Stores/RoleExcel01/Actions';

class RoleExcel01Screen extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      fileList: []
    };

    this.useOnUpload = this.useOnUpload.bind(this);
    this.useOnDownload = this.useOnDownload.bind(this);
  }

  componentDidMount() {}

  componentDidUpdate() {
    const { uploadIsLoading, showBatchJobStatus } = this.props;

    if (uploadIsLoading) {
      showBatchJobStatus();
    }
  }

  componentWillUnmount() {}

  useOnUpload() {
    const { curSiteFlowId, uploadExcel } = this.props;
    const { fileList } = this.state;

    if (fileList.length > 0) {
      uploadExcel(curSiteFlowId, fileList[0]);
    }
  }

  useOnDownload() {
    const { curSiteFlowId, downloadExcel } = this.props;

    downloadExcel(curSiteFlowId);
  }

  render() {
    const { intl, uploadIsLoading, batchJobStatus } = this.props;
    const { fileList } = this.state;

    return (
      <Card>
        <Row type="flex" justify="start" gutter={[0, 8]}>
          <Col span={12}>
            <Upload
              disabled={uploadIsLoading}
              onRemove={() => {
                this.setState({
                  fileList: []
                });
              }}
              beforeUpload={file => {
                const validFileName = 'ROLE_EXCEL_01';
                if (file && file.name.includes(validFileName)) {
                  this.setState({
                    fileList: [file]
                  });
                } else {
                  notification.error({
                    message: `${intl.formatMessage({
                      id: 'invalid_file_name_must_contain'
                    })} ${validFileName}`,
                    duration: parseInt(process.env.REACT_APP_MESSAGE_DURATION, 10)
                  });
                }

                return false;
              }}
              fileList={fileList}
            >
              <Button type="primary" loading={uploadIsLoading}>
                {intl.formatMessage({ id: 'browse' })}
              </Button>
            </Upload>
          </Col>
          <Col span={12}>
            <Button
              type="primary"
              onClick={this.useOnUpload}
              disabled={fileList.length === 0}
              loading={uploadIsLoading}
            >
              {intl.formatMessage({ id: 'upload' })}
            </Button>
            <Button type="primary" onClick={this.useOnDownload} loading={uploadIsLoading}>
              {intl.formatMessage({ id: 'download' })}
            </Button>
          </Col>
        </Row>
        <Row type="flex" justify="center" gutter={[0, 8]}>
          <Col span={24}>
            <Progress percent={parseInt(batchJobStatus.status_number, 10)} />
          </Col>
        </Row>
      </Card>
    );
  }
}

RoleExcel01Screen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,

  batchJobStatus: PropTypes.shape({
    status_number: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }),
  uploadIsLoading: PropTypes.bool,

  showBatchJobStatus: PropTypes.func,
  uploadExcel: PropTypes.func,
  downloadExcel: PropTypes.func,

  curSiteFlowId: PropTypes.number
};

RoleExcel01Screen.defaultProps = {
  intl: {},

  batchJobStatus: {
    status_number: '0'
  },
  uploadIsLoading: false,

  showBatchJobStatus() {},
  uploadExcel() {},
  downloadExcel() {},

  curSiteFlowId: 0
};

const mapStateToProps = state => ({
  batchJobStatus: state.roleExcel01.batchJobStatus,
  uploadIsLoading: state.roleExcel01.uploadIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  showBatchJobStatus: () => dispatch(RoleExcel01Actions.roleExcel01ShowBatchJobStatus()),
  uploadExcel: (siteFlowId, file) =>
    dispatch(RoleExcel01Actions.roleExcel01UploadExcel(siteFlowId, file)),
  downloadExcel: siteFlowId => dispatch(RoleExcel01Actions.roleExcel01DownloadExcel(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(RoleExcel01Screen));
