import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Row, Col, Table, Button, Card, Typography } from 'antd';

import AntDesignTable from '../../Components/AntDesignTable';
import AuditUser01Actions from '../../Stores/AuditUser01/Actions';
import AppActions from '../../Stores/App/Actions';

const { Text } = Typography;

class AuditUser01Table extends React.PureComponent {
  constructor() {
    super();

    this.useOnTableChange = this.useOnTableChange.bind(this);

    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getDocumentColumns = this.getDocumentColumns.bind(this);
    this.getExpandedColumns = this.getExpandedColumns.bind(this);
    this.showExpandedRow = this.showExpandedRow.bind(this);
  }

  componentDidMount() {
    const { resetTimestamp } = this.props;

    resetTimestamp();
  }

  componentDidUpdate(prevProps) {
    const {
      userId,
      resetTimestamp,
      fetchAuditUser01,
      currentPage,
      sorts,
      filters,
      pageSize
    } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('userId')) {
      resetTimestamp();
    }

    if (changed('timestamp')) {
      if (userId > 0) {
        // console.log('userId', userId, 'currentPage', currentPage, 'pageSize', pageSize);
        fetchAuditUser01(userId, currentPage, sorts, filters, pageSize);
      }
    }
  }

  componentWillUnmount() {}

  getDocumentColumns() {
    const { intl, sorts, filters } = this.props;

    return [
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'created_at' }),
        // sort field
        dataIndex: 'created_at',
        ...AntDesignTable.getColumnSortProps(sorts, 'created_at'),
        // filter field
        key: 'created_at',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'created_at' }),
          'created_at',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => record.created_at
      },
      {
        width: 80,
        align: 'left',
        title: intl.formatMessage({ id: 'event' }),
        // sort field
        dataIndex: 'event',
        ...AntDesignTable.getColumnSortProps(sorts, 'event'),
        // filter field
        key: 'event',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'event' }),
          'event',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.event}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'type' }),
        // sort field
        dataIndex: 'auditable_type',
        ...AntDesignTable.getColumnSortProps(sorts, 'type'),
        // filter field
        key: 'auditable_type',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'type' }),
          'auditable_type',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.auditable_type}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'ip_address' }),
        // sort field
        dataIndex: 'ip_address',
        ...AntDesignTable.getColumnSortProps(sorts, 'ip_address'),
        // filter field
        key: 'ip_address',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'ip_address' }),
          'ip_address',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.ip_address}</>
      },
      {
        width: 150,
        align: 'left',
        title: intl.formatMessage({ id: 'user_agent' }),
        // sort field
        dataIndex: 'user_agent',
        ...AntDesignTable.getColumnSortProps(sorts, 'user_agent'),
        // filter field
        key: 'user_agent',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'user_agent' }),
          'user_agent',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.user_agent}</>
      },
      {
        width: 100,
        align: 'left',
        title: intl.formatMessage({ id: 'tags' }),
        // sort field
        dataIndex: 'tags',
        ...AntDesignTable.getColumnSortProps(sorts, 'tags'),
        // filter field
        key: 'tags',
        ...AntDesignTable.getColumnSearchProps(
          filters,
          intl.formatMessage({ id: 'tags' }),
          'tags',
          this.handleSearch,
          this.handleReset
        ),
        render: (text, record) => <>{record.tags}</>
      }
    ];
  }

  getExpandedColumns() {
    const { intl } = this.props;

    return [
      {
        align: 'left',
        title: intl.formatMessage({ id: 'name' }),
        // sort field
        dataIndex: 'name',
        // filter field
        key: 'name',
        render: (text, record) => <>{record.name}</>
      },
      {
        align: 'left',
        title: intl.formatMessage({ id: 'value' }),
        // sort field
        dataIndex: 'value',
        // filter field
        key: 'value',
        render: (text, record) => <>{record.value}</>
      }
    ];
  }

  handleSearch(selectedKeys, confirm) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    confirm();
  }

  handleReset(clearFilters) {
    // eslint-disable-next-line no-unused-vars
    const { intl } = this.props;

    clearFilters();
  }

  showExpandedRow(record) {
    const { intl, fetchIsLoading } = this.props;

    return (
      <Card bordered={false}>
        <Row type="flex" justify="space-between" gutter={[0, 16]}>
          <Col span={12}>
            <Table
              size="small"
              // rowSelection={rowSelection}
              rowKey="name"
              bordered={false}
              pagination={false}
              columns={this.getExpandedColumns()}
              dataSource={record.old_values}
              loading={fetchIsLoading}
              defaultExpandAllRows
              title={() => (
                <>
                  <Row type="flex" justify="center" gutter={[0, 16]}>
                    <Col span={3}>
                      <Text strong>{intl.formatMessage({ id: 'old' })}</Text>
                    </Col>
                  </Row>
                </>
              )}
            />
          </Col>
          <Col span={12}>
            <Table
              size="small"
              // rowSelection={rowSelection}
              rowKey="name"
              bordered={false}
              pagination={false}
              columns={this.getExpandedColumns()}
              dataSource={record.new_values}
              loading={fetchIsLoading}
              defaultExpandAllRows
              title={() => (
                <>
                  <Row type="flex" justify="center" gutter={[0, 16]}>
                    <Col span={3}>
                      <Text strong>{intl.formatMessage({ id: 'new' })}</Text>
                    </Col>
                  </Row>
                </>
              )}
            />
          </Col>
        </Row>
      </Card>
    );
  }

  useOnTableChange(pagination, filters, sorter) {
    const { resetTimestamp } = this.props;

    // process the filters
    const processedFilters = {};
    Object.entries(filters).forEach(entry => {
      const key = entry[0];
      const value = entry[1];
      if (value && value[0]) {
        const filterVal = value.reduce((lastObj, data) => {
          if (lastObj.length === 0) {
            return data;
          }
          return `${lastObj},${data}`;
        }, '');
        processedFilters[key] = filterVal;
      }
    });

    // process the sorts, now just support 1 column
    const processedSorts = {};
    if (Object.entries(sorter).length > 0) {
      processedSorts[sorter.field] = sorter.order;
    }

    resetTimestamp(pagination.current, processedSorts, processedFilters, pagination.pageSize);
  }

  render() {
    const {
      intl,
      resetTimestamp,
      currentPage,
      pageSize,
      total,
      documents,
      fetchIsLoading,
      expandedRows,
      setExpandedRows,
      historyGoBack
    } = this.props;

    return (
      <>
        <Table
          size="small"
          // rowSelection={rowSelection}
          rowKey="id"
          pagination={{
            current: currentPage,
            pageSize: parseInt(pageSize, 10),
            total,
            showTotal: () => `${total} items`
          }}
          columns={this.getDocumentColumns()}
          dataSource={documents}
          loading={fetchIsLoading}
          bordered
          onChange={this.useOnTableChange}
          expandedRowRender={this.showExpandedRow}
          expandedRowKeys={expandedRows}
          onExpandedRowsChange={rowKeys => {
            setExpandedRows(rowKeys);
          }}
          title={() => (
            <>
              <Row type="flex" justify="space-between" gutter={[0, 16]}>
                <Col span={2}>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={fetchIsLoading}
                    onClick={() => resetTimestamp()}
                    icon="reload"
                  />
                </Col>
                <Col span={2}>
                  <Button
                    type="primary"
                    // disabled={!isValid}
                    loading={fetchIsLoading}
                    onClick={historyGoBack}
                    icon="arrow-left"
                  >
                    {intl.formatMessage({
                      id: 'back'
                    })}
                  </Button>
                </Col>
              </Row>
            </>
          )}
          scroll={{ x: 950 }}
        />
      </>
    );
  }
}

AuditUser01Table.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  resetTimestamp: PropTypes.func,

  documents: PropTypes.arrayOf(PropTypes.object),

  sorts: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  filters: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),

  currentPage: PropTypes.number,
  pageSize: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  total: PropTypes.number,
  fetchIsLoading: PropTypes.bool,

  fetchAuditUser01: PropTypes.func,

  userId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

  expandedRows: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  setExpandedRows: PropTypes.func,

  historyGoBack: PropTypes.func
};

AuditUser01Table.defaultProps = {
  intl: {},
  resetTimestamp() {},

  documents: [],

  sorts: {},
  filters: {},

  currentPage: 1,
  pageSize: '20',
  total: 100,
  fetchIsLoading: false,

  userId: 0,

  fetchAuditUser01() {},

  expandedRows: [],
  setExpandedRows() {},

  historyGoBack() {}
};

const mapStateToProps = state => ({
  timestamp: state.auditUser01.timestamp,

  documents: state.auditUser01.documents,

  sorts: state.auditUser01.sorts,
  filters: state.auditUser01.filters,

  currentPage: state.auditUser01.currentPage,
  pageSize: state.auditUser01.pageSize,
  total: state.auditUser01.total,
  fetchIsLoading: state.auditUser01.fetchIsLoading,

  userId: state.auditUser01.userId,

  expandedRows: state.auditUser01.expandedRows
});

const mapDispatchToProps = dispatch => ({
  resetTimestamp: (currentPage, sorts, filters, pageSize) =>
    dispatch(AuditUser01Actions.auditUser01ResetTimestamp(currentPage, sorts, filters, pageSize)),

  fetchAuditUser01: (userId, currentPage, sorts, filters, pageSize) =>
    dispatch(
      AuditUser01Actions.auditUser01FetchAuditUser01(userId, currentPage, sorts, filters, pageSize)
    ),

  setExpandedRows: expandedRows =>
    dispatch(AuditUser01Actions.auditUser01SetExpandedRows(expandedRows)),

  historyGoBack: () => dispatch(AppActions.appHistoryGoBack())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(AuditUser01Table));
