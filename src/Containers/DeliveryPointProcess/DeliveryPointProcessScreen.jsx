import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import treeChanges from 'tree-changes';

import { Card, Tabs, Tooltip } from 'antd';

import DeliveryPointProcessActions from '../../Stores/DeliveryPointProcess/Actions';

import DeliveryPointList01Table from '../DeliveryPointList01/DeliveryPointList01Table';

const { TabPane } = Tabs;

class DeliveryPointProcessScreen extends React.PureComponent {
  componentDidMount() {
    const { curSiteFlowId, fetchDeliveryPointProcess } = this.props;
    if (curSiteFlowId > 0) {
      fetchDeliveryPointProcess(curSiteFlowId);
    }
  }

  componentDidUpdate(prevProps) {
    const { curSiteFlowId, fetchDeliveryPointProcess } = this.props;

    const { changed } = treeChanges(prevProps, this.props);

    if (changed('curSiteFlowId')) {
      if (curSiteFlowId > 0) {
        fetchDeliveryPointProcess(curSiteFlowId);
      }
    }
  }

  componentWillUnmount() {}

  render() {
    const { intl, match, process, appPath } = this.props;

    return (
      <Card>
        <Tabs type="card" activeKey={match.params.proc_type}>
          {process.map(value => (
            <TabPane
              tab={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <Tooltip
                  placement="bottom"
                  title={intl.formatMessage({ id: `${value.proc_type.toLowerCase()}_desc` })}
                >
                  <Link
                    style={value.proc_type !== match.params.proc_type ? { color: 'black' } : {}}
                    to={`${appPath}/deliveryPointProcess/index/${value.proc_type}`}
                  >
                    <FormattedMessage id={`${value.proc_type.toLowerCase()}_label`} />
                  </Link>
                </Tooltip>
              }
              key={value.proc_type}
            />
          ))}
        </Tabs>

        {match.params.proc_type === 'DELIVERY_POINT_LIST_01' && (
          <DeliveryPointList01Table match={match} />
        )}
      </Card>
    );
  }
}

DeliveryPointProcessScreen.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  intl: PropTypes.object,
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object,
  appPath: PropTypes.string,
  process: PropTypes.arrayOf(PropTypes.object),

  curSiteFlowId: PropTypes.number,
  fetchDeliveryPointProcess: PropTypes.func
};

DeliveryPointProcessScreen.defaultProps = {
  intl: {},
  match: {},
  appPath: '',
  process: [],

  curSiteFlowId: 0,
  fetchDeliveryPointProcess() {}
};

const mapStateToProps = state => ({
  appPath: state.app.appPath,
  process: state.deliveryPointProcess.process,
  processIsLoading: state.deliveryPointProcess.processIsLoading,

  curSiteFlowId: state.app.curSiteFlowId
});

const mapDispatchToProps = dispatch => ({
  fetchDeliveryPointProcess: siteFlowId =>
    dispatch(DeliveryPointProcessActions.deliveryPointProcessFetchDeliveryPointProcess(siteFlowId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(DeliveryPointProcessScreen));
