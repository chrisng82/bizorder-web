import enUS from './enUS.json';

const languageObject = {
  'en-US': enUS,
  'zh-Hans-CN': {}
};
export default languageObject;
