/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { ItemExcel01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const itemExcel01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const itemExcel01UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemExcel01Types.ITEM_EXCEL01_SHOW_BATCH_JOB_STATUS_SUCCESS]: itemExcel01ShowBatchJobStatusSuccess,
  [ItemExcel01Types.ITEM_EXCEL01_UPLOAD_LOADING]: itemExcel01UploadLoading
});
