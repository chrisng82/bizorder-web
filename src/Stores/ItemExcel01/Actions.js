import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  itemExcel01ShowBatchJobStatus: null,
  itemExcel01ShowBatchJobStatusSuccess: ['batchJobStatus'],
  itemExcel01UploadExcel: ['siteFlowId', 'divisionId', 'file'],
  itemExcel01UploadLoading: ['boolean'],
  itemExcel01DownloadExcel: ['siteFlowId', 'divisionId']
});

export const ItemExcel01Types = Types;
export default Creators;
