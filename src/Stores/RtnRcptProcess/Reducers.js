/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { RtnRcptProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const rtnRcptProcessFetchRtnRcptProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const rtnRcptProcessFetchRtnRcptProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [RtnRcptProcessTypes.RTN_RCPT_PROCESS_FETCH_RTN_RCPT_PROCESS_LOADING]: rtnRcptProcessFetchRtnRcptProcessLoading,
  [RtnRcptProcessTypes.RTN_RCPT_PROCESS_FETCH_RTN_RCPT_PROCESS_SUCCESS]: rtnRcptProcessFetchRtnRcptProcessSuccess
});
