import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  rtnRcptProcessFetchRtnRcptProcess: ['siteFlowId'],
  rtnRcptProcessFetchRtnRcptProcessLoading: ['boolean'],
  rtnRcptProcessFetchRtnRcptProcessSuccess: ['process']
});

export const RtnRcptProcessTypes = Types;
export default Creators;
