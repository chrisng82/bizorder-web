/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { InbOrd01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const inbOrd01ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const inbOrd01FetchInbOrd01Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const inbOrd01FetchInbOrd01Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const inbOrd01AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const inbOrd01RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const inbOrd01SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const inbOrd01CreateInbOrd01Loading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const inbOrd01CreateInbOrd01Success = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [InbOrd01Types.INB_ORD01_RESET_TIMESTAMP]: inbOrd01ResetTimestamp,
  [InbOrd01Types.INB_ORD01_FETCH_INB_ORD01_LOADING]: inbOrd01FetchInbOrd01Loading,
  [InbOrd01Types.INB_ORD01_FETCH_INB_ORD01_SUCCESS]: inbOrd01FetchInbOrd01Success,
  [InbOrd01Types.INB_ORD01_ADD_SELECTED_DOCUMENTS]: inbOrd01AddSelectedDocuments,
  [InbOrd01Types.INB_ORD01_REMOVE_SELECTED_DOCUMENTS]: inbOrd01RemoveSelectedDocuments,
  [InbOrd01Types.INB_ORD01_SET_WORKSPACE_VISIBLE]: inbOrd01SetWorkspaceVisible,
  [InbOrd01Types.INB_ORD01_CREATE_INB_ORD01_LOADING]: inbOrd01CreateInbOrd01Loading,
  [InbOrd01Types.INB_ORD01_CREATE_INB_ORD01_SUCCESS]: inbOrd01CreateInbOrd01Success
});
