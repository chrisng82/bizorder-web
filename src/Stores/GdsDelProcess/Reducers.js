/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { GdsDelProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const gdsDelProcessFetchGdsDelProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const gdsDelProcessFetchGdsDelProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [GdsDelProcessTypes.GDS_DEL_PROCESS_FETCH_GDS_DEL_PROCESS_LOADING]: gdsDelProcessFetchGdsDelProcessLoading,
  [GdsDelProcessTypes.GDS_DEL_PROCESS_FETCH_GDS_DEL_PROCESS_SUCCESS]: gdsDelProcessFetchGdsDelProcessSuccess
});
