/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PurchaseProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const purchaseProcessFetchPurchaseProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const purchaseProcessFetchPurchaseProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PurchaseProcessTypes.PURCHASE_PROCESS_FETCH_PURCHASE_PROCESS_LOADING]: purchaseProcessFetchPurchaseProcessLoading,
  [PurchaseProcessTypes.PURCHASE_PROCESS_FETCH_PURCHASE_PROCESS_SUCCESS]: purchaseProcessFetchPurchaseProcessSuccess
});
