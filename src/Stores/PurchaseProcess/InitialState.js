/**
 * The initial values for the redux state.
 */
export default {
  process: [],
  processIsLoading: false
};
