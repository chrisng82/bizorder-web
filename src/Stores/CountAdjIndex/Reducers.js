/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { CountAdjIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const countAdjIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const countAdjIndexFetchCountAdjIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const countAdjIndexFetchCountAdjIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const countAdjIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const countAdjIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const countAdjIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CountAdjIndexTypes.COUNT_ADJ_INDEX_RESET_TIMESTAMP]: countAdjIndexResetTimestamp,
  [CountAdjIndexTypes.COUNT_ADJ_INDEX_FETCH_COUNT_ADJ_INDEX_LOADING]: countAdjIndexFetchCountAdjIndexLoading,
  [CountAdjIndexTypes.COUNT_ADJ_INDEX_FETCH_COUNT_ADJ_INDEX_SUCCESS]: countAdjIndexFetchCountAdjIndexSuccess,
  [CountAdjIndexTypes.COUNT_ADJ_INDEX_ADD_SELECTED_DOCUMENTS]: countAdjIndexAddSelectedDocuments,
  [CountAdjIndexTypes.COUNT_ADJ_INDEX_REMOVE_SELECTED_DOCUMENTS]: countAdjIndexRemoveSelectedDocuments,
  [CountAdjIndexTypes.COUNT_ADJ_INDEX_SET_WORKSPACE_VISIBLE]: countAdjIndexSetWorkspaceVisible
});
