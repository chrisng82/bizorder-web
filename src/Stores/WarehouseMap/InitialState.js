/**
 * The initial values for the redux state.
 */
export default {
  timestamp: 0,
  fetchIsLoading: false,
  sorts: {
    est_del_date: 'ascend'
  },
  filters: {},
  pageSize: '20',
  currentPage: 1,
  lastPage: 10,
  total: 100,
  rowLevels: [],

  selectedStorageBin: {},
  selectedQuantBals: []
};
