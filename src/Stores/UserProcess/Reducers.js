/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { UserProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const userProcessFetchUserProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const userProcessFetchUserProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [UserProcessTypes.USER_PROCESS_FETCH_USER_PROCESS_LOADING]: userProcessFetchUserProcessLoading,
  [UserProcessTypes.USER_PROCESS_FETCH_USER_PROCESS_SUCCESS]: userProcessFetchUserProcessSuccess
});
