import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  whseJob1702ResetTimestamp: ['currentPage', 'sorts', 'filters', 'pageSize'],
  whseJob1702GoToDocument: ['hdrId'],
  whseJob1702NewDocument: ['siteFlowId'],
  whseJob1702FetchWhseJob1702: ['siteFlowId', 'currentPage', 'sorts', 'filters', 'pageSize'],
  whseJob1702FetchWhseJob1702Loading: ['boolean'],
  whseJob1702FetchWhseJob1702Success: ['documents', 'currentPage', 'lastPage', 'total', 'pageSize'],
  whseJob1702AddSelectedDocuments: ['selectedDocuments'],
  whseJob1702RemoveSelectedDocuments: ['selectedDocuments'],
  whseJob1702SetWorkspaceVisible: ['boolean'],
  whseJob1702CreateWhseJob1702: ['siteFlowId', 'hdrIds'],
  whseJob1702CreateWhseJob1702Loading: ['boolean'],
  whseJob1702CreateWhseJob1702Success: ['newDocuments']
});

export const WhseJob1702Types = Types;
export default Creators;
