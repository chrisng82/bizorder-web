/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { SlsRtnIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const slsRtnIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const slsRtnIndexFetchSlsRtnIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const slsRtnIndexFetchSlsRtnIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const slsRtnIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const slsRtnIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const slsRtnIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [SlsRtnIndexTypes.SLS_RTN_INDEX_RESET_TIMESTAMP]: slsRtnIndexResetTimestamp,
  [SlsRtnIndexTypes.SLS_RTN_INDEX_FETCH_SLS_RTN_INDEX_LOADING]: slsRtnIndexFetchSlsRtnIndexLoading,
  [SlsRtnIndexTypes.SLS_RTN_INDEX_FETCH_SLS_RTN_INDEX_SUCCESS]: slsRtnIndexFetchSlsRtnIndexSuccess,
  [SlsRtnIndexTypes.SLS_RTN_INDEX_ADD_SELECTED_DOCUMENTS]: slsRtnIndexAddSelectedDocuments,
  [SlsRtnIndexTypes.SLS_RTN_INDEX_REMOVE_SELECTED_DOCUMENTS]: slsRtnIndexRemoveSelectedDocuments,
  [SlsRtnIndexTypes.SLS_RTN_INDEX_SET_WORKSPACE_VISIBLE]: slsRtnIndexSetWorkspaceVisible
});
