/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { InvDoc01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const invDoc01ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const invDoc01FetchInvDoc01Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const invDoc01FetchInvDoc01Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const invDoc01AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const invDoc01RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const invDoc01SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const invDoc01CreateInvDoc01Loading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const invDoc01CreateInvDoc01Success = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

export const invDoc01SetExpandedRows = (state, { expandedRows }) => ({
  ...state,
  expandedRows
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [InvDoc01Types.INV_DOC01_RESET_TIMESTAMP]: invDoc01ResetTimestamp,
  [InvDoc01Types.INV_DOC01_FETCH_INV_DOC01_LOADING]: invDoc01FetchInvDoc01Loading,
  [InvDoc01Types.INV_DOC01_FETCH_INV_DOC01_SUCCESS]: invDoc01FetchInvDoc01Success,
  [InvDoc01Types.INV_DOC01_ADD_SELECTED_DOCUMENTS]: invDoc01AddSelectedDocuments,
  [InvDoc01Types.INV_DOC01_REMOVE_SELECTED_DOCUMENTS]: invDoc01RemoveSelectedDocuments,
  [InvDoc01Types.INV_DOC01_SET_WORKSPACE_VISIBLE]: invDoc01SetWorkspaceVisible,
  [InvDoc01Types.INV_DOC01_CREATE_INV_DOC01_LOADING]: invDoc01CreateInvDoc01Loading,
  [InvDoc01Types.INV_DOC01_CREATE_INV_DOC01_SUCCESS]: invDoc01CreateInvDoc01Success,

  [InvDoc01Types.INV_DOC01_SET_EXPANDED_ROWS]: invDoc01SetExpandedRows
});
