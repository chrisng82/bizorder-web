import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  stockBalanceReportInitStockBalance: ['siteFlowId'],
  stockBalanceReportInitStockBalanceSuccess: ['criteria'],
  stockBalanceReportStockBalance: ['formikBag', 'siteFlowId', 'criteria'],
  stockBalanceReportStockBalanceSuccess: ['criteria', 'reportData'],
  stockBalanceReportReportLoading: ['boolean'],

  stockBalanceReportFetchItemOptions: ['search'],
  stockBalanceReportFetchItemOptionLoading: ['boolean'],
  stockBalanceReportFetchItemOptionSuccess: ['options'],

  stockBalanceReportFetchItemGroup01Options: ['search'],
  stockBalanceReportFetchItemGroup01OptionLoading: ['boolean'],
  stockBalanceReportFetchItemGroup01OptionSuccess: ['options'],

  stockBalanceReportFetchItemGroup02Options: ['search'],
  stockBalanceReportFetchItemGroup02OptionLoading: ['boolean'],
  stockBalanceReportFetchItemGroup02OptionSuccess: ['options'],

  stockBalanceReportFetchItemGroup03Options: ['search'],
  stockBalanceReportFetchItemGroup03OptionLoading: ['boolean'],
  stockBalanceReportFetchItemGroup03OptionSuccess: ['options'],

  stockBalanceReportFetchStorageBinOptions: ['siteFlowId', 'search'],
  stockBalanceReportFetchStorageBinOptionLoading: ['boolean'],
  stockBalanceReportFetchStorageBinOptionSuccess: ['options'],

  stockBalanceReportFetchStorageRowOptions: ['siteFlowId', 'search'],
  stockBalanceReportFetchStorageRowOptionLoading: ['boolean'],
  stockBalanceReportFetchStorageRowOptionSuccess: ['options'],

  stockBalanceReportFetchStorageBayOptions: ['siteFlowId', 'search'],
  stockBalanceReportFetchStorageBayOptionLoading: ['boolean'],
  stockBalanceReportFetchStorageBayOptionSuccess: ['options'],

  stockBalanceReportFetchLocationOptions: ['siteFlowId', 'search'],
  stockBalanceReportFetchLocationOptionLoading: ['boolean'],
  stockBalanceReportFetchLocationOptionSuccess: ['options']
});

export const StockBalanceReportTypes = Types;
export default Creators;
