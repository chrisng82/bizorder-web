/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { CartIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const cartIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const cartIndexFetchCartIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const cartIndexFetchCartIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const cartIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const cartIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const cartIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CartIndexTypes.CART_INDEX_RESET_TIMESTAMP]: cartIndexResetTimestamp,
  [CartIndexTypes.CART_INDEX_FETCH_CART_INDEX_LOADING]: cartIndexFetchCartIndexLoading,
  [CartIndexTypes.CART_INDEX_FETCH_CART_INDEX_SUCCESS]: cartIndexFetchCartIndexSuccess,
  [CartIndexTypes.CART_INDEX_ADD_SELECTED_DOCUMENTS]: cartIndexAddSelectedDocuments,
  [CartIndexTypes.CART_INDEX_REMOVE_SELECTED_DOCUMENTS]: cartIndexRemoveSelectedDocuments,
  [CartIndexTypes.CART_INDEX_SET_WORKSPACE_VISIBLE]: cartIndexSetWorkspaceVisible
});
