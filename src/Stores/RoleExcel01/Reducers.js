/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { RoleExcel01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const roleExcel01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const roleExcel01UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [RoleExcel01Types.ROLE_EXCEL01_SHOW_BATCH_JOB_STATUS_SUCCESS]: roleExcel01ShowBatchJobStatusSuccess,
  [RoleExcel01Types.ROLE_EXCEL01_UPLOAD_LOADING]: roleExcel01UploadLoading
});
