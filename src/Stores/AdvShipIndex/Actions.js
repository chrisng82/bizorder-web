import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  advShipIndexResetTimestamp: ['currentPage', 'sorts', 'filters', 'pageSize'],
  advShipIndexGoToDocument: ['hdrId'],
  advShipIndexGoToAudit: ['hdrId'],
  advShipIndexFetchAdvShipIndex: ['divisionId', 'currentPage', 'sorts', 'filters', 'pageSize'],
  advShipIndexFetchAdvShipIndexLoading: ['boolean'],
  advShipIndexFetchAdvShipIndexSuccess: [
    'documents',
    'currentPage',
    'lastPage',
    'total',
    'pageSize'
  ],
  advShipIndexAddSelectedDocuments: ['selectedDocuments'],
  advShipIndexRemoveSelectedDocuments: ['selectedDocuments'],
  advShipIndexSetWorkspaceVisible: ['boolean'],
  advShipIndexCreateAdvShipIndex: ['hdrIds'],
  advShipIndexCreateAdvShipIndexLoading: ['boolean'],
  advShipIndexCreateAdvShipIndexSuccess: ['newDocuments']
});

export const AdvShipIndexTypes = Types;
export default Creators;
