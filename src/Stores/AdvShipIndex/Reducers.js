/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { AdvShipIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const advShipIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const advShipIndexFetchAdvShipIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const advShipIndexFetchAdvShipIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const advShipIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const advShipIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const advShipIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const advShipIndexCreateAdvShipIndexLoading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const advShipIndexCreateAdvShipIndexSuccess = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AdvShipIndexTypes.ADV_SHIP_INDEX_RESET_TIMESTAMP]: advShipIndexResetTimestamp,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_FETCH_ADV_SHIP_INDEX_LOADING]: advShipIndexFetchAdvShipIndexLoading,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_FETCH_ADV_SHIP_INDEX_SUCCESS]: advShipIndexFetchAdvShipIndexSuccess,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_ADD_SELECTED_DOCUMENTS]: advShipIndexAddSelectedDocuments,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_REMOVE_SELECTED_DOCUMENTS]: advShipIndexRemoveSelectedDocuments,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_SET_WORKSPACE_VISIBLE]: advShipIndexSetWorkspaceVisible,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_CREATE_ADV_SHIP_INDEX_LOADING]: advShipIndexCreateAdvShipIndexLoading,
  [AdvShipIndexTypes.ADV_SHIP_INDEX_CREATE_ADV_SHIP_INDEX_SUCCESS]: advShipIndexCreateAdvShipIndexSuccess
});
