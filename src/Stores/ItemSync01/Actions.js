import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  itemSync01ShowSyncSetting: ['divisionId'],
  itemSync01ShowSyncSettingLoading: ['boolean'],
  itemSync01ShowSyncSettingSuccess: ['syncSetting'],
  itemSync01UpdateSyncSetting: ['formikBag', 'syncSetting'],
  itemSync01ShowBatchJobStatus: null,
  itemSync01ShowBatchJobStatusSuccess: ['batchJobStatus'],
  itemSync01SyncNow: ['formikBag', 'divisionId'],
  itemSync01SyncImage: ['formikBag', 'divisionId'],
  itemSync01ResetImage: ['formikBag', 'divisionId'],
  itemSync01SyncLoading: ['boolean']
});

export const ItemSync01Types = Types;
export default Creators;
