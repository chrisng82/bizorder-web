import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  countAdjAnalysisReportInitCountAdjAnalysis: ['siteFlowId'],
  countAdjAnalysisReportInitCountAdjAnalysisSuccess: ['criteria'],
  countAdjAnalysisReportCountAdjAnalysis: ['formikBag', 'siteFlowId', 'criteria'],
  countAdjAnalysisReportCountAdjAnalysisSuccess: ['criteria', 'reportData'],
  countAdjAnalysisReportReportLoading: ['boolean'],

  countAdjAnalysisReportFetchItemOptions: ['search'],
  countAdjAnalysisReportFetchItemOptionLoading: ['boolean'],
  countAdjAnalysisReportFetchItemOptionSuccess: ['options'],

  countAdjAnalysisReportFetchItemGroup01Options: ['search'],
  countAdjAnalysisReportFetchItemGroup01OptionLoading: ['boolean'],
  countAdjAnalysisReportFetchItemGroup01OptionSuccess: ['options'],

  countAdjAnalysisReportFetchItemGroup02Options: ['search'],
  countAdjAnalysisReportFetchItemGroup02OptionLoading: ['boolean'],
  countAdjAnalysisReportFetchItemGroup02OptionSuccess: ['options'],

  countAdjAnalysisReportFetchItemGroup03Options: ['search'],
  countAdjAnalysisReportFetchItemGroup03OptionLoading: ['boolean'],
  countAdjAnalysisReportFetchItemGroup03OptionSuccess: ['options'],

  countAdjAnalysisReportFetchStorageBinOptions: ['siteFlowId', 'search'],
  countAdjAnalysisReportFetchStorageBinOptionLoading: ['boolean'],
  countAdjAnalysisReportFetchStorageBinOptionSuccess: ['options'],

  countAdjAnalysisReportFetchStorageRowOptions: ['siteFlowId', 'search'],
  countAdjAnalysisReportFetchStorageRowOptionLoading: ['boolean'],
  countAdjAnalysisReportFetchStorageRowOptionSuccess: ['options'],

  countAdjAnalysisReportFetchStorageBayOptions: ['siteFlowId', 'search'],
  countAdjAnalysisReportFetchStorageBayOptionLoading: ['boolean'],
  countAdjAnalysisReportFetchStorageBayOptionSuccess: ['options'],

  countAdjAnalysisReportFetchLocationOptions: ['siteFlowId', 'search'],
  countAdjAnalysisReportFetchLocationOptionLoading: ['boolean'],
  countAdjAnalysisReportFetchLocationOptionSuccess: ['options']
});

export const CountAdjAnalysisReportTypes = Types;
export default Creators;
