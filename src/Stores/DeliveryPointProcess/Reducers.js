/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { DeliveryPointProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const deliveryPointProcessFetchDeliveryPointProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const deliveryPointProcessFetchDeliveryPointProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DeliveryPointProcessTypes.DELIVERY_POINT_PROCESS_FETCH_DELIVERY_POINT_PROCESS_LOADING]: deliveryPointProcessFetchDeliveryPointProcessLoading,
  [DeliveryPointProcessTypes.DELIVERY_POINT_PROCESS_FETCH_DELIVERY_POINT_PROCESS_SUCCESS]: deliveryPointProcessFetchDeliveryPointProcessSuccess
});
