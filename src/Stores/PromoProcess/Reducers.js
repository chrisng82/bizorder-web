/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PromoProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const promoProcessFetchPromoProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const promoProcessFetchPromoProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PromoProcessTypes.PROMO_PROCESS_FETCH_PROMO_PROCESS_LOADING]: promoProcessFetchPromoProcessLoading,
  [PromoProcessTypes.PROMO_PROCESS_FETCH_PROMO_PROCESS_SUCCESS]: promoProcessFetchPromoProcessSuccess
});
