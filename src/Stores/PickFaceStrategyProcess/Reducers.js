/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PickFaceStrategyProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const pickFaceStrategyProcessFetchPickFaceStrategyProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const pickFaceStrategyProcessFetchPickFaceStrategyProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PickFaceStrategyProcessTypes.PICK_FACE_STRATEGY_PROCESS_FETCH_PICK_FACE_STRATEGY_PROCESS_LOADING]: pickFaceStrategyProcessFetchPickFaceStrategyProcessLoading,
  [PickFaceStrategyProcessTypes.PICK_FACE_STRATEGY_PROCESS_FETCH_PICK_FACE_STRATEGY_PROCESS_SUCCESS]: pickFaceStrategyProcessFetchPickFaceStrategyProcessSuccess
});
