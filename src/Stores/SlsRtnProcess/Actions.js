import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  slsRtnProcessFetchSlsRtnProcess: ['divisionId'],
  slsRtnProcessFetchSlsRtnProcessLoading: ['boolean'],
  slsRtnProcessFetchSlsRtnProcessSuccess: ['process']
});

export const SlsRtnProcessTypes = Types;
export default Creators;
