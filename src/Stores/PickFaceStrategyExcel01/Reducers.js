/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PickFaceStrategyExcel01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const pickFaceStrategyExcel01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const pickFaceStrategyExcel01UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PickFaceStrategyExcel01Types.PICK_FACE_STRATEGY_EXCEL01_SHOW_BATCH_JOB_STATUS_SUCCESS]: pickFaceStrategyExcel01ShowBatchJobStatusSuccess,
  [PickFaceStrategyExcel01Types.PICK_FACE_STRATEGY_EXCEL01_UPLOAD_LOADING]: pickFaceStrategyExcel01UploadLoading
});
