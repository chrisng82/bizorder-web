/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { UserExcel03Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const userExcel03ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const userExcel03UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [UserExcel03Types.USER_EXCEL03_SHOW_BATCH_JOB_STATUS_SUCCESS]: userExcel03ShowBatchJobStatusSuccess,
  [UserExcel03Types.USER_EXCEL03_UPLOAD_LOADING]: userExcel03UploadLoading
});
