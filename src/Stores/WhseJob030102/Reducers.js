/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { WhseJob030102Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const whseJob030102ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  fetchIsLoading: false,
  printIsLoading: false
});

export const whseJob030102FetchWhseJob030102Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const whseJob030102FetchWhseJob030102Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const whseJob030102AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const whseJob030102RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const whseJob030102SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const whseJob030102PrintWhseJob030102Loading = (state, { boolean }) => ({
  ...state,
  printIsLoading: boolean
});

export const whseJob030102SetExpandedRows = (state, { expandedRows }) => ({
  ...state,
  expandedRows
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [WhseJob030102Types.WHSE_JOB030102_RESET_TIMESTAMP]: whseJob030102ResetTimestamp,
  [WhseJob030102Types.WHSE_JOB030102_FETCH_WHSE_JOB030102_LOADING]: whseJob030102FetchWhseJob030102Loading,
  [WhseJob030102Types.WHSE_JOB030102_FETCH_WHSE_JOB030102_SUCCESS]: whseJob030102FetchWhseJob030102Success,
  [WhseJob030102Types.WHSE_JOB030102_ADD_SELECTED_DOCUMENTS]: whseJob030102AddSelectedDocuments,
  [WhseJob030102Types.WHSE_JOB030102_REMOVE_SELECTED_DOCUMENTS]: whseJob030102RemoveSelectedDocuments,
  [WhseJob030102Types.WHSE_JOB030102_SET_WORKSPACE_VISIBLE]: whseJob030102SetWorkspaceVisible,
  [WhseJob030102Types.WHSE_JOB030102_PRINT_WHSE_JOB030102_LOADING]: whseJob030102PrintWhseJob030102Loading,

  [WhseJob030102Types.WHSE_JOB030102_SET_EXPANDED_ROWS]: whseJob030102SetExpandedRows
});
