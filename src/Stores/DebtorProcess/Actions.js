import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  debtorProcessFetchDebtorProcess: ['siteFlowId'],
  debtorProcessFetchDebtorProcessLoading: ['boolean'],
  debtorProcessFetchDebtorProcessSuccess: ['process']
});

export const DebtorProcessTypes = Types;
export default Creators;
 