/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { DebtorProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const debtorProcessFetchDebtorProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const debtorProcessFetchDebtorProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DebtorProcessTypes.DEBTOR_PROCESS_FETCH_DEBTOR_PROCESS_LOADING]: debtorProcessFetchDebtorProcessLoading,
  [DebtorProcessTypes.DEBTOR_PROCESS_FETCH_DEBTOR_PROCESS_SUCCESS]: debtorProcessFetchDebtorProcessSuccess
});
 