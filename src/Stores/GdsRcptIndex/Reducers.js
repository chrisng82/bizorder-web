/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { GdsRcptIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const gdsRcptIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  toStorageBinOptions: INITIAL_STATE.toStorageBinOptions,
  toStorageBinOption: INITIAL_STATE.toStorageBinOption
});

export const gdsRcptIndexFetchGdsRcptIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const gdsRcptIndexFetchGdsRcptIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const gdsRcptIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const gdsRcptIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const gdsRcptIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [GdsRcptIndexTypes.GDS_RCPT_INDEX_RESET_TIMESTAMP]: gdsRcptIndexResetTimestamp,
  [GdsRcptIndexTypes.GDS_RCPT_INDEX_FETCH_GDS_RCPT_INDEX_LOADING]: gdsRcptIndexFetchGdsRcptIndexLoading,
  [GdsRcptIndexTypes.GDS_RCPT_INDEX_FETCH_GDS_RCPT_INDEX_SUCCESS]: gdsRcptIndexFetchGdsRcptIndexSuccess,
  [GdsRcptIndexTypes.GDS_RCPT_INDEX_ADD_SELECTED_DOCUMENTS]: gdsRcptIndexAddSelectedDocuments,
  [GdsRcptIndexTypes.GDS_RCPT_INDEX_REMOVE_SELECTED_DOCUMENTS]: gdsRcptIndexRemoveSelectedDocuments,
  [GdsRcptIndexTypes.GDS_RCPT_INDEX_SET_WORKSPACE_VISIBLE]: gdsRcptIndexSetWorkspaceVisible
});
