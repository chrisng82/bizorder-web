/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { ItemGroup03List01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const itemGroup03List01ResetTimestamp = (
  state,
  { currentPage, sorts, filters, pageSize }
) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const itemGroup03List01FetchItemGroup03ListLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const itemGroup03List01FetchItemGroup03ListSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const itemGroup03List01AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const itemGroup03List01RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const itemGroup03List01SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const itemGroup03List01SetManagePhotoVisible = (state, { document, boolean }) => ({
  ...state,
  selectedDocument: document,
  managePhotoIsVisible: boolean
});

export const itemGroup03List01ManagePhotoLoading = (state, { boolean }) => ({
  ...state,
  photoIsLoading: boolean
});

export const itemGroup03List01ManagePhotoSuccess = (state, { document }) => ({
  ...state,
  selectedDocument: document
});

export const itemGroup03List01UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_RESET_TIMESTAMP]: itemGroup03List01ResetTimestamp,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_FETCH_ITEM_GROUP03_LIST_LOADING]: itemGroup03List01FetchItemGroup03ListLoading,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_FETCH_ITEM_GROUP03_LIST_SUCCESS]: itemGroup03List01FetchItemGroup03ListSuccess,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_ADD_SELECTED_DOCUMENTS]: itemGroup03List01AddSelectedDocuments,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_REMOVE_SELECTED_DOCUMENTS]: itemGroup03List01RemoveSelectedDocuments,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_SET_WORKSPACE_VISIBLE]: itemGroup03List01SetWorkspaceVisible,

  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_SET_MANAGE_PHOTO_VISIBLE]: itemGroup03List01SetManagePhotoVisible,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_MANAGE_PHOTO_LOADING]: itemGroup03List01ManagePhotoLoading,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_MANAGE_PHOTO_SUCCESS]: itemGroup03List01ManagePhotoSuccess,
  [ItemGroup03List01Types.ITEM_GROUP03_LIST01_UPLOAD_LOADING]: itemGroup03List01UploadLoading
});
