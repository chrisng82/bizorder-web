/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { GdsRcpt0201Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const gdsRcpt0201ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  fetchIsLoading: false,
  printIsLoading: false
});

export const gdsRcpt0201FetchGdsRcpt0201Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const gdsRcpt0201FetchGdsRcpt0201Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const gdsRcpt0201AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const gdsRcpt0201RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const gdsRcpt0201SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const gdsRcpt0201PrintGdsRcpt0201Loading = (state, { boolean }) => ({
  ...state,
  printIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [GdsRcpt0201Types.GDS_RCPT0201_RESET_TIMESTAMP]: gdsRcpt0201ResetTimestamp,
  [GdsRcpt0201Types.GDS_RCPT0201_FETCH_GDS_RCPT0201_LOADING]: gdsRcpt0201FetchGdsRcpt0201Loading,
  [GdsRcpt0201Types.GDS_RCPT0201_FETCH_GDS_RCPT0201_SUCCESS]: gdsRcpt0201FetchGdsRcpt0201Success,
  [GdsRcpt0201Types.GDS_RCPT0201_ADD_SELECTED_DOCUMENTS]: gdsRcpt0201AddSelectedDocuments,
  [GdsRcpt0201Types.GDS_RCPT0201_REMOVE_SELECTED_DOCUMENTS]: gdsRcpt0201RemoveSelectedDocuments,
  [GdsRcpt0201Types.GDS_RCPT0201_SET_WORKSPACE_VISIBLE]: gdsRcpt0201SetWorkspaceVisible,
  [GdsRcpt0201Types.GDS_RCPT0201_PRINT_GDS_RCPT0201_LOADING]: gdsRcpt0201PrintGdsRcpt0201Loading
});
