import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  userExcel01ShowBatchJobStatus: null,
  userExcel01ShowBatchJobStatusSuccess: ['batchJobStatus'],
  userExcel01UploadExcel: ['siteFlowId', 'file'],
  userExcel01UploadLoading: ['boolean'],
  userExcel01DownloadExcel: ['siteFlowId']
});

export const UserExcel01Types = Types;
export default Creators;
