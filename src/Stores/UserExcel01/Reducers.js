/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { UserExcel01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const userExcel01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const userExcel01UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [UserExcel01Types.USER_EXCEL01_SHOW_BATCH_JOB_STATUS_SUCCESS]: userExcel01ShowBatchJobStatusSuccess,
  [UserExcel01Types.USER_EXCEL01_UPLOAD_LOADING]: userExcel01UploadLoading
});
