import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  slsOrdSync01ShowSyncSetting: ['divisionId'],
  slsOrdSync01ShowSyncSettingLoading: ['boolean'],
  slsOrdSync01ShowSyncSettingSuccess: ['syncSetting'],
  slsOrdSync01UpdateSyncSetting: ['formikBag', 'syncSetting'],
  slsOrdSync01ShowBatchJobStatus: null,
  slsOrdSync01ShowBatchJobStatusSuccess: ['batchJobStatus'],
  slsOrdSync01SyncNow: ['formikBag', 'divisionId'],
  slsOrdSync01SyncLoading: ['boolean']
});

export const SlsOrdSync01Types = Types;
export default Creators;
