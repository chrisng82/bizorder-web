/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { InbOrdIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const inbOrdIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const inbOrdIndexFetchInbOrdIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const inbOrdIndexFetchInbOrdIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const inbOrdIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const inbOrdIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const inbOrdIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [InbOrdIndexTypes.INB_ORD_INDEX_RESET_TIMESTAMP]: inbOrdIndexResetTimestamp,
  [InbOrdIndexTypes.INB_ORD_INDEX_FETCH_INB_ORD_INDEX_LOADING]: inbOrdIndexFetchInbOrdIndexLoading,
  [InbOrdIndexTypes.INB_ORD_INDEX_FETCH_INB_ORD_INDEX_SUCCESS]: inbOrdIndexFetchInbOrdIndexSuccess,
  [InbOrdIndexTypes.INB_ORD_INDEX_ADD_SELECTED_DOCUMENTS]: inbOrdIndexAddSelectedDocuments,
  [InbOrdIndexTypes.INB_ORD_INDEX_REMOVE_SELECTED_DOCUMENTS]: inbOrdIndexRemoveSelectedDocuments,
  [InbOrdIndexTypes.INB_ORD_INDEX_SET_WORKSPACE_VISIBLE]: inbOrdIndexSetWorkspaceVisible
});
