/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { WhseJob160102Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const whseJob160102ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  fetchIsLoading: false,
  printIsLoading: false
});

export const whseJob160102FetchWhseJob160102Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const whseJob160102FetchWhseJob160102Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const whseJob160102AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const whseJob160102RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const whseJob160102SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const whseJob160102PrintWhseJob160102Loading = (state, { boolean }) => ({
  ...state,
  printIsLoading: boolean
});

export const whseJob160102SetExpandedRows = (state, { expandedRows }) => ({
  ...state,
  expandedRows
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [WhseJob160102Types.WHSE_JOB160102_RESET_TIMESTAMP]: whseJob160102ResetTimestamp,
  [WhseJob160102Types.WHSE_JOB160102_FETCH_WHSE_JOB160102_LOADING]: whseJob160102FetchWhseJob160102Loading,
  [WhseJob160102Types.WHSE_JOB160102_FETCH_WHSE_JOB160102_SUCCESS]: whseJob160102FetchWhseJob160102Success,
  [WhseJob160102Types.WHSE_JOB160102_ADD_SELECTED_DOCUMENTS]: whseJob160102AddSelectedDocuments,
  [WhseJob160102Types.WHSE_JOB160102_REMOVE_SELECTED_DOCUMENTS]: whseJob160102RemoveSelectedDocuments,
  [WhseJob160102Types.WHSE_JOB160102_SET_WORKSPACE_VISIBLE]: whseJob160102SetWorkspaceVisible,
  [WhseJob160102Types.WHSE_JOB160102_PRINT_WHSE_JOB160102_LOADING]: whseJob160102PrintWhseJob160102Loading,

  [WhseJob160102Types.WHSE_JOB160102_SET_EXPANDED_ROWS]: whseJob160102SetExpandedRows
});
