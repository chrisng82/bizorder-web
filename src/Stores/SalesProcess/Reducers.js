/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { SalesProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const salesProcessFetchSalesProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const salesProcessFetchSalesProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [SalesProcessTypes.SALES_PROCESS_FETCH_SALES_PROCESS_LOADING]: salesProcessFetchSalesProcessLoading,
  [SalesProcessTypes.SALES_PROCESS_FETCH_SALES_PROCESS_SUCCESS]: salesProcessFetchSalesProcessSuccess
});
