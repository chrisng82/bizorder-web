/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { CompanyListTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const companyListResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const companyListFetchCompanyListLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const companyListFetchCompanyListSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const companyListAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const companyListRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CompanyListTypes.COMPANY_LIST_RESET_TIMESTAMP]: companyListResetTimestamp,
  [CompanyListTypes.COMPANY_LIST_FETCH_COMPANY_LIST_LOADING]: companyListFetchCompanyListLoading,
  [CompanyListTypes.COMPANY_LIST_FETCH_COMPANY_LIST_SUCCESS]: companyListFetchCompanyListSuccess,
  [CompanyListTypes.COMPANY_LIST_ADD_SELECTED_DOCUMENTS]: companyListAddSelectedDocuments,
  [CompanyListTypes.COMPANY_LIST_REMOVE_SELECTED_DOCUMENTS]: companyListRemoveSelectedDocuments
});
