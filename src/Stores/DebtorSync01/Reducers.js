/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { DebtorSync01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const debtorSync01ShowSyncSettingLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const debtorSync01ShowSyncSettingSuccess = (state, { syncSetting }) => ({
  ...state,
  syncSetting
});

export const debtorSync01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const debtorSync01SyncLoading = (state, { boolean }) => ({
  ...state,
  syncIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DebtorSync01Types.DEBTOR_SYNC01_SHOW_SYNC_SETTING_LOADING]: debtorSync01ShowSyncSettingLoading,
  [DebtorSync01Types.DEBTOR_SYNC01_SHOW_SYNC_SETTING_SUCCESS]: debtorSync01ShowSyncSettingSuccess,
  [DebtorSync01Types.DEBTOR_SYNC01_SHOW_BATCH_JOB_STATUS_SUCCESS]: debtorSync01ShowBatchJobStatusSuccess,
  [DebtorSync01Types.DEBTOR_SYNC01_SYNC_LOADING]: debtorSync01SyncLoading
});
