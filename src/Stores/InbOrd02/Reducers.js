/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { InbOrd02Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const inbOrd02ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const inbOrd02FetchInbOrd02Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const inbOrd02FetchInbOrd02Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const inbOrd02AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const inbOrd02RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const inbOrd02SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const inbOrd02CreateInbOrd02Loading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const inbOrd02CreateInbOrd02Success = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [InbOrd02Types.INB_ORD02_RESET_TIMESTAMP]: inbOrd02ResetTimestamp,
  [InbOrd02Types.INB_ORD02_FETCH_INB_ORD02_LOADING]: inbOrd02FetchInbOrd02Loading,
  [InbOrd02Types.INB_ORD02_FETCH_INB_ORD02_SUCCESS]: inbOrd02FetchInbOrd02Success,
  [InbOrd02Types.INB_ORD02_ADD_SELECTED_DOCUMENTS]: inbOrd02AddSelectedDocuments,
  [InbOrd02Types.INB_ORD02_REMOVE_SELECTED_DOCUMENTS]: inbOrd02RemoveSelectedDocuments,
  [InbOrd02Types.INB_ORD02_SET_WORKSPACE_VISIBLE]: inbOrd02SetWorkspaceVisible,
  [InbOrd02Types.INB_ORD02_CREATE_INB_ORD02_LOADING]: inbOrd02CreateInbOrd02Loading,
  [InbOrd02Types.INB_ORD02_CREATE_INB_ORD02_SUCCESS]: inbOrd02CreateInbOrd02Success
});
