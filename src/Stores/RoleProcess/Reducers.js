/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { RoleProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const roleProcessFetchRoleProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const roleProcessFetchRoleProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [RoleProcessTypes.ROLE_PROCESS_FETCH_ROLE_PROCESS_LOADING]: roleProcessFetchRoleProcessLoading,
  [RoleProcessTypes.ROLE_PROCESS_FETCH_ROLE_PROCESS_SUCCESS]: roleProcessFetchRoleProcessSuccess
});
