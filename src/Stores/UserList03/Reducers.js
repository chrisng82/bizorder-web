/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { UserList03Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const userList03ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const userList03FetchUserList03Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const userList03FetchUserList03Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const userList03AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const userList03RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [UserList03Types.USER_LIST03_RESET_TIMESTAMP]: userList03ResetTimestamp,
  [UserList03Types.USER_LIST03_FETCH_USER_LIST03_LOADING]: userList03FetchUserList03Loading,
  [UserList03Types.USER_LIST03_FETCH_USER_LIST03_SUCCESS]: userList03FetchUserList03Success,
  [UserList03Types.USER_LIST03_ADD_SELECTED_DOCUMENTS]: userList03AddSelectedDocuments,
  [UserList03Types.USER_LIST03_REMOVE_SELECTED_DOCUMENTS]: userList03RemoveSelectedDocuments,
});
