/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PromoSync01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const promoSync01ShowSyncSettingLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const promoSync01ShowSyncSettingSuccess = (state, { syncSetting }) => ({
  ...state,
  syncSetting
});

export const promoSync01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const promoSync01SyncLoading = (state, { boolean }) => ({
  ...state,
  syncIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PromoSync01Types.PROMO_SYNC01_SHOW_SYNC_SETTING_LOADING]: promoSync01ShowSyncSettingLoading,
  [PromoSync01Types.PROMO_SYNC01_SHOW_SYNC_SETTING_SUCCESS]: promoSync01ShowSyncSettingSuccess,
  [PromoSync01Types.PROMO_SYNC01_SHOW_BATCH_JOB_STATUS_SUCCESS]: promoSync01ShowBatchJobStatusSuccess,
  [PromoSync01Types.PROMO_SYNC01_SYNC_LOADING]: promoSync01SyncLoading
});
