/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { createReducer } from 'reduxsauce';
import { SlsOrdIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const slsOrdIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: []
});

export const slsOrdIndexFetchSlsOrdIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const slsOrdIndexFetchSlsOrdIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const slsOrdIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const slsOrdIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const slsOrdIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [SlsOrdIndexTypes.SLS_ORD_INDEX_RESET_TIMESTAMP]: slsOrdIndexResetTimestamp,
  [SlsOrdIndexTypes.SLS_ORD_INDEX_FETCH_SLS_ORD_INDEX_LOADING]: slsOrdIndexFetchSlsOrdIndexLoading,
  [SlsOrdIndexTypes.SLS_ORD_INDEX_FETCH_SLS_ORD_INDEX_SUCCESS]: slsOrdIndexFetchSlsOrdIndexSuccess,
  [SlsOrdIndexTypes.SLS_ORD_INDEX_ADD_SELECTED_DOCUMENTS]: slsOrdIndexAddSelectedDocuments,
  [SlsOrdIndexTypes.SLS_ORD_INDEX_REMOVE_SELECTED_DOCUMENTS]: slsOrdIndexRemoveSelectedDocuments,
  [SlsOrdIndexTypes.SLS_ORD_INDEX_SET_WORKSPACE_VISIBLE]: slsOrdIndexSetWorkspaceVisible
});
