/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { WhseJob1601Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const whseJob1601ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  toStorageBinOptions: INITIAL_STATE.toStorageBinOptions,
  toStorageBinOption: INITIAL_STATE.toStorageBinOption
});

export const whseJob1601FetchWhseJob1601Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const whseJob1601FetchWhseJob1601Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const whseJob1601AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const whseJob1601RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const whseJob1601SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const whseJob1601CreateWhseJob1601Loading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const whseJob1601CreateWhseJob1601Success = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [WhseJob1601Types.WHSE_JOB1601_RESET_TIMESTAMP]: whseJob1601ResetTimestamp,
  [WhseJob1601Types.WHSE_JOB1601_FETCH_WHSE_JOB1601_LOADING]: whseJob1601FetchWhseJob1601Loading,
  [WhseJob1601Types.WHSE_JOB1601_FETCH_WHSE_JOB1601_SUCCESS]: whseJob1601FetchWhseJob1601Success,
  [WhseJob1601Types.WHSE_JOB1601_ADD_SELECTED_DOCUMENTS]: whseJob1601AddSelectedDocuments,
  [WhseJob1601Types.WHSE_JOB1601_REMOVE_SELECTED_DOCUMENTS]: whseJob1601RemoveSelectedDocuments,
  [WhseJob1601Types.WHSE_JOB1601_SET_WORKSPACE_VISIBLE]: whseJob1601SetWorkspaceVisible,
  [WhseJob1601Types.WHSE_JOB1601_CREATE_WHSE_JOB1601_LOADING]: whseJob1601CreateWhseJob1601Loading,
  [WhseJob1601Types.WHSE_JOB1601_CREATE_WHSE_JOB1601_SUCCESS]: whseJob1601CreateWhseJob1601Success
});
