/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { StorageBinExcel01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const storageBinExcel01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const storageBinExcel01UploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [StorageBinExcel01Types.STORAGE_BIN_EXCEL01_SHOW_BATCH_JOB_STATUS_SUCCESS]: storageBinExcel01ShowBatchJobStatusSuccess,
  [StorageBinExcel01Types.STORAGE_BIN_EXCEL01_UPLOAD_LOADING]: storageBinExcel01UploadLoading
});
