/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { ItemProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const itemProcessFetchItemProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const itemProcessFetchItemProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ItemProcessTypes.ITEM_PROCESS_FETCH_ITEM_PROCESS_LOADING]: itemProcessFetchItemProcessLoading,
  [ItemProcessTypes.ITEM_PROCESS_FETCH_ITEM_PROCESS_SUCCESS]: itemProcessFetchItemProcessSuccess
});
