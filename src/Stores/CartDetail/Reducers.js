/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { CartDetailTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const cartDetailResetTimestamp = state => ({
  ...state,
  timestamp: Date.now()
});

export const cartDetailSetHdrId = (state, { hdrId, itemId }) => ({
  ...state,
  timestamp: Date.now(),
  documentIsLoading: false,
  hdrId,
  itemId,
  detailIsVisible: false,
  salesmanOptions: [],
  deliveryPointOptions: [],
  creditTermOptions: [],
  currencyOptions: []
});

export const cartDetailShowDocumentLoading = (state, { boolean }) => ({
  ...state,
  documentIsLoading: boolean
});

export const cartDetailShowHeaderSuccess = (state, { documentHeader }) => ({
  ...state,
  documentHeader
});

export const cartDetailUpdateDocumentSuccess = (state, { documentHeader, documentDetails }) => ({
  ...state,
  documentHeader,
  documentDetails
});

export const cartDetailShowDetailsSuccess = (state, { documentDetails }) => ({
  ...state,
  documentDetails
});

export const cartDetailSetDetailVisible = (state, { boolean }) => ({
  ...state,
  detailIsVisible: boolean
});

export const cartDetailSetDocumentDetail = (state, { documentDetail }) => ({
  ...state,
  documentDetail,
  itemOptions: [],
  uomOptions: []
});

export const cartDetailFetchSalesmanOptionLoading = (state, { boolean }) => ({
  ...state,
  salesmanIsLoading: boolean
});

export const cartDetailFetchSalesmanOptionSuccess = (state, { options }) => ({
  ...state,
  salesmanOptions: options
});

export const cartDetailFetchDeliveryPointOptionLoading = (state, { boolean }) => ({
  ...state,
  deliveryPointIsLoading: boolean
});

export const cartDetailFetchDeliveryPointOptionSuccess = (state, { options }) => ({
  ...state,
  deliveryPointOptions: options
});

export const cartDetailFetchCreditTermOptionLoading = (state, { boolean }) => ({
  ...state,
  creditTermIsLoading: boolean
});

export const cartDetailFetchCreditTermOptionSuccess = (state, { options }) => ({
  ...state,
  creditTermOptions: options
});

export const cartDetailFetchCurrencyOptionLoading = (state, { boolean }) => ({
  ...state,
  currencyIsLoading: boolean
});

export const cartDetailFetchCurrencyOptionSuccess = (state, { options }) => ({
  ...state,
  currencyOptions: options
});

export const cartDetailFetchItemOptionLoading = (state, { boolean }) => ({
  ...state,
  itemIsLoading: boolean
});

export const cartDetailFetchItemOptionSuccess = (state, { options }) => ({
  ...state,
  itemOptions: options
});

export const cartDetailFetchUomOptionLoading = (state, { boolean }) => ({
  ...state,
  uomIsLoading: boolean
});

export const cartDetailFetchUomOptionSuccess = (state, { options }) => ({
  ...state,
  uomOptions: options
});

// force refresh the formik form
export const cartDetailUpdateDetails = (state, { documentDetails }) => ({
  ...state,
  documentDetail: documentDetails[0]
});

// force refresh the formik form
export const cartDetailCreateDetail = (state, { documentDetail }) => ({
  ...state,
  documentDetail
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [CartDetailTypes.CART_DETAIL_RESET_TIMESTAMP]: cartDetailResetTimestamp,
  [CartDetailTypes.CART_DETAIL_SET_HDR_ID]: cartDetailSetHdrId,

  [CartDetailTypes.CART_DETAIL_SHOW_HEADER_SUCCESS]: cartDetailShowHeaderSuccess,

  [CartDetailTypes.CART_DETAIL_UPDATE_DOCUMENT_SUCCESS]: cartDetailUpdateDocumentSuccess,

  [CartDetailTypes.CART_DETAIL_SHOW_DOCUMENT_LOADING]: cartDetailShowDocumentLoading,

  [CartDetailTypes.CART_DETAIL_SHOW_DETAILS_SUCCESS]: cartDetailShowDetailsSuccess,

  [CartDetailTypes.CART_DETAIL_SET_DETAIL_VISIBLE]: cartDetailSetDetailVisible,
  [CartDetailTypes.CART_DETAIL_SET_DOCUMENT_DETAIL]: cartDetailSetDocumentDetail,

  [CartDetailTypes.CART_DETAIL_UPDATE_DETAILS]: cartDetailUpdateDetails,
  [CartDetailTypes.CART_DETAIL_CREATE_DETAIL]: cartDetailCreateDetail,

  [CartDetailTypes.CART_DETAIL_FETCH_SALESMAN_OPTION_LOADING]: cartDetailFetchSalesmanOptionLoading,
  [CartDetailTypes.CART_DETAIL_FETCH_SALESMAN_OPTION_SUCCESS]: cartDetailFetchSalesmanOptionSuccess,

  [CartDetailTypes.CART_DETAIL_FETCH_DELIVERY_POINT_OPTION_LOADING]: cartDetailFetchDeliveryPointOptionLoading,
  [CartDetailTypes.CART_DETAIL_FETCH_DELIVERY_POINT_OPTION_SUCCESS]: cartDetailFetchDeliveryPointOptionSuccess,

  [CartDetailTypes.CART_DETAIL_FETCH_CREDIT_TERM_OPTION_LOADING]: cartDetailFetchCreditTermOptionLoading,
  [CartDetailTypes.CART_DETAIL_FETCH_CREDIT_TERM_OPTION_SUCCESS]: cartDetailFetchCreditTermOptionSuccess,

  [CartDetailTypes.CART_DETAIL_FETCH_CURRENCY_OPTION_LOADING]: cartDetailFetchCurrencyOptionLoading,
  [CartDetailTypes.CART_DETAIL_FETCH_CURRENCY_OPTION_SUCCESS]: cartDetailFetchCurrencyOptionSuccess,

  [CartDetailTypes.CART_DETAIL_FETCH_ITEM_OPTION_LOADING]: cartDetailFetchItemOptionLoading,
  [CartDetailTypes.CART_DETAIL_FETCH_ITEM_OPTION_SUCCESS]: cartDetailFetchItemOptionSuccess,

  [CartDetailTypes.CART_DETAIL_FETCH_UOM_OPTION_LOADING]: cartDetailFetchUomOptionLoading,
  [CartDetailTypes.CART_DETAIL_FETCH_UOM_OPTION_SUCCESS]: cartDetailFetchUomOptionSuccess
});
