/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { RtnRcptSync01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const rtnRcptSync01ShowSyncSettingLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const rtnRcptSync01ShowSyncSettingSuccess = (state, { syncSetting }) => ({
  ...state,
  syncSetting
});

export const rtnRcptSync01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const rtnRcptSync01SyncLoading = (state, { boolean }) => ({
  ...state,
  syncIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [RtnRcptSync01Types.RTN_RCPT_SYNC01_SHOW_SYNC_SETTING_LOADING]: rtnRcptSync01ShowSyncSettingLoading,
  [RtnRcptSync01Types.RTN_RCPT_SYNC01_SHOW_SYNC_SETTING_SUCCESS]: rtnRcptSync01ShowSyncSettingSuccess,
  [RtnRcptSync01Types.RTN_RCPT_SYNC01_SHOW_BATCH_JOB_STATUS_SUCCESS]: rtnRcptSync01ShowBatchJobStatusSuccess,
  [RtnRcptSync01Types.RTN_RCPT_SYNC01_SYNC_LOADING]: rtnRcptSync01SyncLoading
});
