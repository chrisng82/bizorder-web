import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  rtnRcptSync01ShowSyncSetting: ['divisionId'],
  rtnRcptSync01ShowSyncSettingLoading: ['boolean'],
  rtnRcptSync01ShowSyncSettingSuccess: ['syncSetting'],
  rtnRcptSync01UpdateSyncSetting: ['formikBag', 'syncSetting'],
  rtnRcptSync01ShowBatchJobStatus: null,
  rtnRcptSync01ShowBatchJobStatusSuccess: ['batchJobStatus'],
  rtnRcptSync01SyncNow: ['formikBag', 'divisionId'],
  rtnRcptSync01SyncLoading: ['boolean']
});

export const RtnRcptSync01Types = Types;
export default Creators;
