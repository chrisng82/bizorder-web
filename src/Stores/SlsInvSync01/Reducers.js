/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { SlsInvSync01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const slsInvSync01ShowSyncSettingLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const slsInvSync01ShowSyncSettingSuccess = (state, { syncSetting }) => ({
  ...state,
  syncSetting
});

export const slsInvSync01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const slsInvSync01SyncLoading = (state, { boolean }) => ({
  ...state,
  syncIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [SlsInvSync01Types.SLS_INV_SYNC01_SHOW_SYNC_SETTING_LOADING]: slsInvSync01ShowSyncSettingLoading,
  [SlsInvSync01Types.SLS_INV_SYNC01_SHOW_SYNC_SETTING_SUCCESS]: slsInvSync01ShowSyncSettingSuccess,
  [SlsInvSync01Types.SLS_INV_SYNC01_SHOW_BATCH_JOB_STATUS_SUCCESS]: slsInvSync01ShowBatchJobStatusSuccess,
  [SlsInvSync01Types.SLS_INV_SYNC01_SYNC_LOADING]: slsInvSync01SyncLoading
});
