/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { AdvShipSync01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const advShipSync01ShowSyncSettingLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const advShipSync01ShowSyncSettingSuccess = (state, { syncSetting }) => ({
  ...state,
  syncSetting
});

export const advShipSync01ShowBatchJobStatusSuccess = (state, { batchJobStatus }) => ({
  ...state,
  batchJobStatus
});

export const advShipSync01SyncLoading = (state, { boolean }) => ({
  ...state,
  syncIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AdvShipSync01Types.ADV_SHIP_SYNC01_SHOW_SYNC_SETTING_LOADING]: advShipSync01ShowSyncSettingLoading,
  [AdvShipSync01Types.ADV_SHIP_SYNC01_SHOW_SYNC_SETTING_SUCCESS]: advShipSync01ShowSyncSettingSuccess,
  [AdvShipSync01Types.ADV_SHIP_SYNC01_SHOW_BATCH_JOB_STATUS_SUCCESS]: advShipSync01ShowBatchJobStatusSuccess,
  [AdvShipSync01Types.ADV_SHIP_SYNC01_SYNC_LOADING]: advShipSync01SyncLoading
});
