/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PickListIndexTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const pickListIndexResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  toStorageBinOptions: INITIAL_STATE.toStorageBinOptions,
  toStorageBinOption: INITIAL_STATE.toStorageBinOption
});

export const pickListIndexFetchPickListIndexLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const pickListIndexFetchPickListIndexSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const pickListIndexAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const pickListIndexRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const pickListIndexSetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PickListIndexTypes.PICK_LIST_INDEX_RESET_TIMESTAMP]: pickListIndexResetTimestamp,
  [PickListIndexTypes.PICK_LIST_INDEX_FETCH_PICK_LIST_INDEX_LOADING]: pickListIndexFetchPickListIndexLoading,
  [PickListIndexTypes.PICK_LIST_INDEX_FETCH_PICK_LIST_INDEX_SUCCESS]: pickListIndexFetchPickListIndexSuccess,
  [PickListIndexTypes.PICK_LIST_INDEX_ADD_SELECTED_DOCUMENTS]: pickListIndexAddSelectedDocuments,
  [PickListIndexTypes.PICK_LIST_INDEX_REMOVE_SELECTED_DOCUMENTS]: pickListIndexRemoveSelectedDocuments,
  [PickListIndexTypes.PICK_LIST_INDEX_SET_WORKSPACE_VISIBLE]: pickListIndexSetWorkspaceVisible
});
