import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  pickListIndexResetTimestamp: ['currentPage', 'sorts', 'filters', 'pageSize'],
  pickListIndexGoToDocument: ['hdrId'],
  pickListIndexGoToAudit: ['hdrId'],
  pickListIndexFetchPickListIndex: ['siteFlowId', 'currentPage', 'sorts', 'filters', 'pageSize'],
  pickListIndexFetchPickListIndexLoading: ['boolean'],
  pickListIndexFetchPickListIndexSuccess: [
    'documents',
    'currentPage',
    'lastPage',
    'total',
    'pageSize'
  ],
  pickListIndexAddSelectedDocuments: ['selectedDocuments'],
  pickListIndexRemoveSelectedDocuments: ['selectedDocuments'],
  pickListIndexSetWorkspaceVisible: ['boolean']
});

export const PickListIndexTypes = Types;
export default Creators;
