/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { DivisionListTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const divisionListResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const divisionListFetchDivisionListLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const divisionListFetchDivisionListSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const divisionListAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const divisionListRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DivisionListTypes.DIVISION_LIST_RESET_TIMESTAMP]: divisionListResetTimestamp,
  [DivisionListTypes.DIVISION_LIST_FETCH_DIVISION_LIST_LOADING]: divisionListFetchDivisionListLoading,
  [DivisionListTypes.DIVISION_LIST_FETCH_DIVISION_LIST_SUCCESS]: divisionListFetchDivisionListSuccess,
  [DivisionListTypes.DIVISION_LIST_ADD_SELECTED_DOCUMENTS]: divisionListAddSelectedDocuments,
  [DivisionListTypes.DIVISION_LIST_REMOVE_SELECTED_DOCUMENTS]: divisionListRemoveSelectedDocuments
});
