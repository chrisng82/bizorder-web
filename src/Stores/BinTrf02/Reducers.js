/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { BinTrf02Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const binTrf02ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const binTrf02NewDocument = state => ({
  ...state,
  timestamp: Date.now()
});

export const binTrf02FetchBinTrf02Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const binTrf02FetchBinTrf02Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const binTrf02AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const binTrf02RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const binTrf02SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const binTrf02PrintBinTrf02Loading = (state, { boolean }) => ({
  ...state,
  printIsLoading: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [BinTrf02Types.BIN_TRF02_RESET_TIMESTAMP]: binTrf02ResetTimestamp,
  [BinTrf02Types.BIN_TRF02_FETCH_BIN_TRF02_LOADING]: binTrf02FetchBinTrf02Loading,
  [BinTrf02Types.BIN_TRF02_FETCH_BIN_TRF02_SUCCESS]: binTrf02FetchBinTrf02Success,
  [BinTrf02Types.BIN_TRF02_ADD_SELECTED_DOCUMENTS]: binTrf02AddSelectedDocuments,
  [BinTrf02Types.BIN_TRF02_REMOVE_SELECTED_DOCUMENTS]: binTrf02RemoveSelectedDocuments,
  [BinTrf02Types.BIN_TRF02_SET_WORKSPACE_VISIBLE]: binTrf02SetWorkspaceVisible,
  [BinTrf02Types.BIN_TRF02_PRINT_BIN_TRF02_LOADING]: binTrf02PrintBinTrf02Loading
});
