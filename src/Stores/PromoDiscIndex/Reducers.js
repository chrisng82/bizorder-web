/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PromoDiscListTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const promoDiscListResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const promoDiscListFetchPromoListLoading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const promoDiscListFetchPromoListSuccess = (
  state,
  { documents, currentPage, lastPage, total, pageSize, filterStr }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize,
  filterStr
});

export const promoDiscListAddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const promoDiscListRemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const promoDiscListSetManagePhotoVisible = (state, { document, boolean }) => ({
  ...state,
  selectedDocument: document,
  managePhotoIsVisible: boolean
});

export const promoDiscListManagePhotoLoading = (state, { boolean }) => ({
  ...state,
  photoIsLoading: boolean
});

export const promoDiscListManagePhotoSuccess = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocument: selectedDocuments
});

export const promoDiscListUploadLoading = (state, { boolean }) => ({
  ...state,
  uploadIsLoading: boolean
});

export const promoDiscListUploadPhotoHold = (state, { boolean, fileList}) => ({
  ...state,
  uploadIsHold: boolean,
  fileList: fileList
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PromoDiscListTypes.PROMO_DISC_LIST_RESET_TIMESTAMP]: promoDiscListResetTimestamp,
  [PromoDiscListTypes.PROMO_DISC_LIST_FETCH_PROMO_LIST_LOADING]: promoDiscListFetchPromoListLoading,
  [PromoDiscListTypes.PROMO_DISC_LIST_FETCH_PROMO_LIST_SUCCESS]: promoDiscListFetchPromoListSuccess,
  [PromoDiscListTypes.PROMO_DISC_LIST_ADD_SELECTED_DOCUMENTS]: promoDiscListAddSelectedDocuments,
  [PromoDiscListTypes.PROMO_DISC_LIST_REMOVE_SELECTED_DOCUMENTS]: promoDiscListRemoveSelectedDocuments,
  [PromoDiscListTypes.PROMO_DISC_LIST_SET_MANAGE_PHOTO_VISIBLE]: promoDiscListSetManagePhotoVisible,
  [PromoDiscListTypes.PROMO_DISC_LIST_MANAGE_PHOTO_LOADING]: promoDiscListManagePhotoLoading,
  [PromoDiscListTypes.PROMO_DISC_LIST_MANAGE_PHOTO_SUCCESS]: promoDiscListManagePhotoSuccess,
  [PromoDiscListTypes.PROMO_DISC_LIST_UPLOAD_LOADING]: promoDiscListUploadLoading,
  [PromoDiscListTypes.PROMO_DISC_LIST_UPLOAD_PHOTO_HOLD]: promoDiscListUploadPhotoHold
});
