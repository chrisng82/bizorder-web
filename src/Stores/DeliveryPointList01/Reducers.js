/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { DeliveryPointList01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const deliveryPointList01ResetTimestamp = (
  state,
  { currentPage, sorts, filters, pageSize }
) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  toStorageBinOptions: INITIAL_STATE.toStorageBinOptions,
  toStorageBinOption: INITIAL_STATE.toStorageBinOption
});

export const deliveryPointList01FetchDeliveryPointList01Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const deliveryPointList01FetchDeliveryPointList01Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const deliveryPointList01AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const deliveryPointList01RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const deliveryPointList01SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [DeliveryPointList01Types.DELIVERY_POINT_LIST01_RESET_TIMESTAMP]: deliveryPointList01ResetTimestamp,
  [DeliveryPointList01Types.DELIVERY_POINT_LIST01_FETCH_DELIVERY_POINT_LIST01_LOADING]: deliveryPointList01FetchDeliveryPointList01Loading,
  [DeliveryPointList01Types.DELIVERY_POINT_LIST01_FETCH_DELIVERY_POINT_LIST01_SUCCESS]: deliveryPointList01FetchDeliveryPointList01Success,
  [DeliveryPointList01Types.DELIVERY_POINT_LIST01_ADD_SELECTED_DOCUMENTS]: deliveryPointList01AddSelectedDocuments,
  [DeliveryPointList01Types.DELIVERY_POINT_LIST01_REMOVE_SELECTED_DOCUMENTS]: deliveryPointList01RemoveSelectedDocuments,
  [DeliveryPointList01Types.DELIVERY_POINT_LIST01_SET_WORKSPACE_VISIBLE]: deliveryPointList01SetWorkspaceVisible
});
