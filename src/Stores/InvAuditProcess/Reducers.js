/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { InvAuditProcessTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const invAuditProcessFetchInvAuditProcessLoading = (state, { boolean }) => ({
  ...state,
  processIsLoading: boolean
});

export const invAuditProcessFetchInvAuditProcessSuccess = (state, { process }) => ({
  ...state,
  process
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [InvAuditProcessTypes.INV_AUDIT_PROCESS_FETCH_INV_AUDIT_PROCESS_LOADING]: invAuditProcessFetchInvAuditProcessLoading,
  [InvAuditProcessTypes.INV_AUDIT_PROCESS_FETCH_INV_AUDIT_PROCESS_SUCCESS]: invAuditProcessFetchInvAuditProcessSuccess
});
