/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { CompanyDetailTypes } from './Actions';
import INITIAL_STATE from './InitialState';

export const companyDetailResetTimestamp = state => ({
  ...state,
  timestamp: Date.now()
});

export const companyDetailSetHdrId = (state, { hdrId }) => ({
  ...state,
  timestamp: Date.now(),
  documentIsLoading: false,
  hdrId,
  detailIsVisible: false,
});

export const companyDetailShowDocumentLoading = (state, { boolean }) => ({
    ...state,
    documentIsLoading: boolean
  });
  
export const companyDetailShowHeaderSuccess = (state, { documentHeader }) => ({
    ...state,
    documentHeader
});

export const companyDetailUpdateHeaderSuccess = (state, { documentHeader }) => ({
  ...state,
  documentHeader
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
    [CompanyDetailTypes.COMPANY_DETAIL_RESET_TIMESTAMP]: companyDetailResetTimestamp,
    [CompanyDetailTypes.COMPANY_DETAIL_SET_HDR_ID]: companyDetailSetHdrId,
    [CompanyDetailTypes.COMPANY_DETAIL_SHOW_HEADER_SUCCESS]: companyDetailShowHeaderSuccess,
    [CompanyDetailTypes.COMPANY_DETAIL_SHOW_DOCUMENT_LOADING]: companyDetailShowDocumentLoading,
    [CompanyDetailTypes.COMPANY_DETAIL_UPDATE_HEADER_SUCCESS]: companyDetailUpdateHeaderSuccess,

});