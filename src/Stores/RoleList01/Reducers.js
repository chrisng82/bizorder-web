/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { RoleList01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const roleList01ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const roleList01FetchRoleList01Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const roleList01FetchRoleList01Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const roleList01AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const roleList01RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const roleList01SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [RoleList01Types.ROLE_LIST01_RESET_TIMESTAMP]: roleList01ResetTimestamp,
  [RoleList01Types.ROLE_LIST01_FETCH_ROLE_LIST01_LOADING]: roleList01FetchRoleList01Loading,
  [RoleList01Types.ROLE_LIST01_FETCH_ROLE_LIST01_SUCCESS]: roleList01FetchRoleList01Success,
  [RoleList01Types.ROLE_LIST01_ADD_SELECTED_DOCUMENTS]: roleList01AddSelectedDocuments,
  [RoleList01Types.ROLE_LIST01_REMOVE_SELECTED_DOCUMENTS]: roleList01RemoveSelectedDocuments,
  [RoleList01Types.ROLE_LIST01_SET_WORKSPACE_VISIBLE]: roleList01SetWorkspaceVisible
});
