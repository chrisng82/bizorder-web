/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { PutAway01Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const putAway01ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments
});

export const putAway01FetchPutAway01Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const putAway01FetchPutAway01Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const putAway01AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const putAway01RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const putAway01SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const putAway01CreatePutAway01Loading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const putAway01CreatePutAway01Success = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [PutAway01Types.PUT_AWAY01_RESET_TIMESTAMP]: putAway01ResetTimestamp,
  [PutAway01Types.PUT_AWAY01_FETCH_PUT_AWAY01_LOADING]: putAway01FetchPutAway01Loading,
  [PutAway01Types.PUT_AWAY01_FETCH_PUT_AWAY01_SUCCESS]: putAway01FetchPutAway01Success,
  [PutAway01Types.PUT_AWAY01_ADD_SELECTED_DOCUMENTS]: putAway01AddSelectedDocuments,
  [PutAway01Types.PUT_AWAY01_REMOVE_SELECTED_DOCUMENTS]: putAway01RemoveSelectedDocuments,
  [PutAway01Types.PUT_AWAY01_SET_WORKSPACE_VISIBLE]: putAway01SetWorkspaceVisible,
  [PutAway01Types.PUT_AWAY01_CREATE_PUT_AWAY01_LOADING]: putAway01CreatePutAway01Loading,
  [PutAway01Types.PUT_AWAY01_CREATE_PUT_AWAY01_SUCCESS]: putAway01CreatePutAway01Success
});
