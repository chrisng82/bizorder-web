/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { WhseJob1501Types } from './Actions';
import INITIAL_STATE from './InitialState';

export const whseJob1501ResetTimestamp = (state, { currentPage, sorts, filters, pageSize }) => ({
  ...state,
  currentPage: currentPage || state.currentPage,
  sorts: sorts || state.sorts,
  filters: filters || state.filters,
  pageSize: pageSize || state.pageSize,
  timestamp: Date.now(),
  selectedDocuments: INITIAL_STATE.selectedDocuments,
  toStorageBinOptions: INITIAL_STATE.toStorageBinOptions,
  toStorageBinOption: INITIAL_STATE.toStorageBinOption
});

export const whseJob1501FetchWhseJob1501Loading = (state, { boolean }) => ({
  ...state,
  fetchIsLoading: boolean
});

export const whseJob1501FetchWhseJob1501Success = (
  state,
  { documents, currentPage, lastPage, total, pageSize }
) => ({
  ...state,
  documents,
  currentPage,
  lastPage,
  total,
  pageSize
});

export const whseJob1501AddSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: [
    ...state.selectedDocuments,
    ...selectedDocuments.filter(data => {
      const selectIds = state.selectedDocuments.map(value => value.id);
      return !selectIds.includes(data.id);
    })
  ]
});

export const whseJob1501RemoveSelectedDocuments = (state, { selectedDocuments }) => ({
  ...state,
  selectedDocuments: state.selectedDocuments.filter(data => {
    const selectIds = selectedDocuments.map(value => value.id);
    return !selectIds.includes(data.id);
  })
});

export const whseJob1501SetWorkspaceVisible = (state, { boolean }) => ({
  ...state,
  workspaceIsVisible: boolean
});

export const whseJob1501CreateWhseJob1501Loading = (state, { boolean }) => ({
  ...state,
  createIsLoading: boolean
});

export const whseJob1501CreateWhseJob1501Success = (state, { newDocuments }) => ({
  ...state,
  timestamp: Date.now(),
  selectedDocuments: [],
  newDocuments
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [WhseJob1501Types.WHSE_JOB1501_RESET_TIMESTAMP]: whseJob1501ResetTimestamp,
  [WhseJob1501Types.WHSE_JOB1501_FETCH_WHSE_JOB1501_LOADING]: whseJob1501FetchWhseJob1501Loading,
  [WhseJob1501Types.WHSE_JOB1501_FETCH_WHSE_JOB1501_SUCCESS]: whseJob1501FetchWhseJob1501Success,
  [WhseJob1501Types.WHSE_JOB1501_ADD_SELECTED_DOCUMENTS]: whseJob1501AddSelectedDocuments,
  [WhseJob1501Types.WHSE_JOB1501_REMOVE_SELECTED_DOCUMENTS]: whseJob1501RemoveSelectedDocuments,
  [WhseJob1501Types.WHSE_JOB1501_SET_WORKSPACE_VISIBLE]: whseJob1501SetWorkspaceVisible,
  [WhseJob1501Types.WHSE_JOB1501_CREATE_WHSE_JOB1501_LOADING]: whseJob1501CreateWhseJob1501Loading,
  [WhseJob1501Types.WHSE_JOB1501_CREATE_WHSE_JOB1501_SUCCESS]: whseJob1501CreateWhseJob1501Success
});
